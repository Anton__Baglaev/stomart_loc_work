<div id="requestIsAvailable" data-load="<?=SITE_TEMPLATE_PATH?>/images/picLoad.gif">
	<div id="requestIsAvailableContainer">
		<div class="requestIsAvailableHeading">Узнать о поступлении товара<a href="#" class="close closeWindow"></a></div>
		<div class="requstProductContainer">
			<div class="productColumn">
				<div class="productImageBlock">
					<a href="#" class="requestIsAvailableUrl" target="_blank"><img src="<?=SITE_TEMPLATE_PATH?>/images/picLoad.gif" alt="" class="requestIsAvailablePicture"></a>
				</div>
				<div class="productNameBlock">
					<a href="#" class="productUrl requestIsAvailableUrl" target="_blank">
						<span class="middle">Загрузка товара</span>
					</a>
				</div>
			</div>
			<div class="formColumn">
				<div class="requestIsAvailableFormHeading">Заполните данные</div>
				<form id="requestIsAvailableForm" method="GET">
					<input type="text" name="name" value="" placeholder="Имя" id="requestIsAvailableFormName">
					<input type="text" name="telephone" value="" data-required="Y" placeholder="Телефон*" id="requestIsAvailableFormTelephone">
					<input type="hidden" name="productID" value="" id="requestIsAvailableProductID">
					<input name="id" type="hidden" id="requestIsAvailableFormId" value="">
					<input name="act" type="hidden" id="requestIsAvailableFormAct" value="requestIsAvailable">
					<input name="SITE_ID" type="hidden" id="requestIsAvailableFormSiteId" value="<?=SITE_ID?>">
					<textarea name="message" placeholder="Сообщение"></textarea>
					<div class="personalInfoRequest"><input type="checkbox" name="personalInfoRequest" id="personalInfoRequest"><label for="personalInfoRequest">Я согласен на <a href="/personal-info/" class="pilink">обработку персональных данных.</a>*</label></div>
					<a href="#" id="requestIsAvailableSubmit"><img src="<?=SITE_TEMPLATE_PATH?>/images/request.png" alt="Запросить цену"> Узнать о поступлении товара</a>
				</form>
			</div>
		</div>
		<div id="requestIsAvailableResult">
			<div id="requestIsAvailableResultTitle"></div>
			<div id="requestIsAvailableResultMessage"></div>
			<a href="" id="requestIsAvailableResultClose" class="closeWindow">Закрыть окно</a>
        </div>
	</div>
</div>
