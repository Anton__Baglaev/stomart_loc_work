<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Способы оплаты Как можно оплатить заказы в СТОМАРТ. Как поменять способ оплаты Когда можно поменять способ оплаты и как это сделать Ошибка при оплате. Чеки и документы. Где взять чек и сертификат на товар. Выписки и справки");
$APPLICATION->SetPageProperty("keywords", "Банковский перевод, безналичная оплата, онлайн оплата, наличные");
$APPLICATION->SetPageProperty("title", "Как оплатить | СТОМАРТ");
$APPLICATION->SetTitle("Способы оплаты ");
?><h1 class="h1 ff-bold">Способы оплаты</h1>
<ul>
	<li><a href="#1" class="link-dashed">Наличный расчёт</a></li>
	<li><a href="#2" class="link-dashed">Банковской картой</a></li>
	<li><a href="#3" class="link-dashed">Безналичный расчет</a></li>
	<li><a href="#4" class="link-dashed">Оплата по квитанции</a></li>
</ul>
<h2 id="1" class="h2 ff-medium">
Наличный расчёт </h2>
<p>
	 Если товар доставляется курьером, то оплата осуществляется наличными курьеру в руки. При получении товара обязательно проверьте комплектацию товара, наличие гарантийного талона и чека.<br>
</p>
<h2 id="2" class="h2 ff-medium">
Банковской картой </h2>
<p>
	 Для выбора оплаты товара с помощью банковской карты на соответствующей странице необходимо нажать кнопку&nbsp;Оплата заказа банковской картой. Оплата происходит через ПАО СБЕРБАНК с использованием банковских карт следующих платёжных систем:
</p>
<div class="list-payment">
	<div class="payment-item visa">
 <img src="/local/templates/stomart/images/pngwing.com%20(1).png" alt="">
	</div>
	<div class="payment-item">
 <img src="/local/templates/stomart/images/pngwing.com.png" alt="">
	</div>
	<div class="payment-item mir">
 <img src="/local/templates/stomart/images/logo-mir.png">
	</div>
</div>
 <!--ul>
           <li><img width="150" src="/local/templates/stomart/images/logo-mir.png" height="75" align="middle">МИР;</li>
       </ul>
        <br>
       <ul>
           <li><img width="150" src="/local/templates/stomart/images/pngwing.com%20(1).png" height="150" align="middle">&nbsp;VISA International;</li>
       </ul>
        <br>
       <ul>
           <li><img width="150" src="/local/templates/stomart/images/pngwing.com.png" height="149" align="middle">&nbsp;Mastercard Worldwide ;</li>
       </ul-->
<p>
	 Для оплаты (ввода реквизитов Вашей карты) Вы будете перенаправлены на платёжный шлюз ПАО СБЕРБАНК. Соединение с платёжным шлюзом и передача информации осуществляется в защищённом режиме с использованием протокола шифрования SSL. В случае если Ваш банк поддерживает технологию безопасного проведения интернет-платежей Verified By Visa, MasterCard SecureCode, MIR Accept, J-Secure, для проведения платежа также может потребоваться ввод специального пароля.<br>
 <br>
	 Настоящий сайт поддерживает 256-битное шифрование. Конфиденциальность сообщаемой персональной информации обеспечивается ПАО СБЕРБАНК. Введённая информация не будет предоставлена третьим лицам за исключением случаев, предусмотренных законодательством РФ. Проведение платежей по банковским картам осуществляется в строгом соответствии с требованиями платёжных систем МИР, Visa Int., MasterCard Europe Sprl.
</p>
<h2 id="3" class="h2 ff-medium">
Безналичный расчет </h2>
<p>
	 Безналичный расчет (по счету) – такой расчет более всего подходит для организаций. Вы делаете заказ, выбираете способ оплаты "безналичный расчет (по счету)". Распечатываете выставленный счет, оплачиваете его после того, как статус заказа изменится на "ожидание платежа".
</p>
<h2 id="4" class="h2 ff-medium">
Оплата по квитанции </h2>
<p>
	 Оплата по квитанции через банк – для оплаты вам нужно распечатать и оплатить квитанцию банка РФ, а затем прислать нам на электронную почту <a href="mailto:zakaz@stomart.ru?subject=Вопрос по оплате">zakaz@stomart.ru</a> отсканированную копию полученного документа об оплате. Также вы можете оплатить через приложение банка введя <a href="/about/requisite/">реквизиты</a> нашей компании.
</p><? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>