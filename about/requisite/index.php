<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Реквизиты | СТОМАРТ - лидер по продаже оборудования для стоматологов");
$APPLICATION->SetPageProperty("description", "Медицинское оборудование для стоматологов. Доставка по всей России. Программа лояльности. Выставочный шоу-рум в центре Москвы");
$APPLICATION->SetTitle("Реквизиты компании СТОМАРТ");?><h1>Реквизиты</h1>
 <?$APPLICATION->IncludeComponent(
	"bitrix:menu",
	"personal",
	Array(
		"ALLOW_MULTI_SELECT" => "N",
		"CHILD_MENU_TYPE" => "",
		"COMPONENT_TEMPLATE" => "personal",
		"DELAY" => "N",
		"MAX_LEVEL" => "1",
		"MENU_CACHE_GET_VARS" => array(),
		"MENU_CACHE_TIME" => "3600000",
		"MENU_CACHE_TYPE" => "A",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"ROOT_MENU_TYPE" => "about",
		"USE_EXT" => "N"
	)
);?>
<div class="global-block-container">
<div class="table-simple-wrap">
<table class="table-simple">
<tbody>
<tr>
	<th colspan="2">
	 КАРТОЧКА ОРГАНИЗАЦИИ
	</th>
</tr>
<tr>
	<td>
	 Полное или сокращенное наименование фирмы <br>(в соответствии с Учредительными документами)
	</td>
	<td>
	 Общество с ограниченной ответственностью «СТОМАРТ» <br>(ООО «СТОМАРТ»)
	</td>
</tr>
<tr>
	<td>
	 Юридический адрес <br>(в соответствии с учредительными документами)
	</td>
	<td>
	 117420, г. Москва, ул. Наметкина, д.14, корпус 1, помещение I К39,58
	</td>
</tr>
<tr>
	<td>
	 Почтовый и фактический адрес
	</td>
	<td>
	 117420, г. Москва, ул. Наметкина, д.14, корпус 1, помещение I К39,58
	</td>
</tr>
<tr>
	<td>
	 Телефон
	</td>
	<td>
	 +7 (495) 646-0156
	</td>
</tr>
<tr>
	<td>
	 ИНН
	</td>
	<td>
	 7728353396
	</td>
</tr>
<tr>
	<td>
	 КПП
	</td>
	<td>
	 772801001
	</td>
</tr>
<tr>
	<td>
	 ОКПО
	</td>
	<td>
	 05556831
	</td>
</tr>
<tr>
	<td>
	 ОКАТО
	</td>
	<td>
	 45293590000
	</td>
</tr>
<tr>
	<td>
	 ОКТМО
	</td>
	<td>
	 45908000000
	</td>
</tr>
<tr>
	<td>
	 ОКВЭД
	</td>
	<td>
	 47.74
	</td>
</tr>
<tr>
	<td>
	 ОГРН
	</td>
	<td>
	 5167746326820
	</td>
</tr>
<tr>
	<td>
	 Рег. номер в ПФР
	</td>
	<td>
	 087-707-016494
	</td>
</tr>
<tr>
	<td>
	 Рег. Номер в ФСС
	</td>
	<td>
	 7717067810
	</td>
</tr>
<tr>
	<th colspan="2">
	 ПЛАТЕЖНЫЕ РЕКВИЗИТЫ
	</th>
</tr>
<tr>
	<td>
	 Расчетный счет
	</td>
	<td>
	 40702810702300007705
	</td>
</tr>
<tr>
	<td>
	 Корреспондентский счет
	</td>
	<td>
	 30101810200000000593
	</td>
</tr>
<tr>
	<td>
	 БИК
	</td>
	<td>
	 044525593
	</td>
</tr>
<tr>
	<td>
	 Наименование банка
	</td>
	<td>
	 АО «Альфа-Банк»
	</td>
</tr>
</tbody>
</table>
</div>
	<div class="global-information-block">
		 <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_RECURSIVE" => "Y",
		"AREA_FILE_SHOW" => "sect",
		"AREA_FILE_SUFFIX" => "information_block",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => ""
	)
);?>
	</div>
</div>
	 <p>Генеральный директор Вардапетян Арутюн Тигранович</p>
	 <p>Главный бухгалтер Скибина Светлана Александровна</p><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>