<?
	//include module
	\Bitrix\Main\Loader::includeModule("dw.deluxe");
	//get template settings
	$arTemplateSettings = DwSettings::getInstance()->getCurrentSettings();
?>
<br>
<?if(!empty($arTemplateSettings["TEMPLATE_TELEPHONE_1"])):?><div class="telephone"><a href="tel:<?=$arTemplateSettings["TEMPLATE_TELEPHONE_1"]?>"><?=$arTemplateSettings["TEMPLATE_TELEPHONE_1"]?></a></div><?endif;?>
<?if(!empty($arTemplateSettings["TEMPLATE_EMAIL_1"])):?><div class="email"><a href="mailto:<?=$arTemplateSettings["TEMPLATE_EMAIL_1"]?>"><?=$arTemplateSettings["TEMPLATE_EMAIL_1"]?></a></div><?endif;?>
<?if(!empty($arTemplateSettings["TEMPLATE_WORKING_TIME"])):?><ul class="list"><li><?=$arTemplateSettings["TEMPLATE_WORKING_TIME"]?></li></ul><?endif;?>