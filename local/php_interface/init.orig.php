<?php
//редиректна на нижний регистр
function getUrlQuery($url, $key = null)
{
    $parts = parse_url($url);
    if (!empty($parts['query'])) {
        parse_str($parts['query'], $query);
        if (is_null($key)) {
            return $query;
        } elseif (isset($query[$key])) {
            return $query[$key];
        }
    }

    return false;
}
//var_dump(getUrlQuery($_SERVER['REQUEST_URI']));
if ( $_SERVER['REQUEST_URI'] != strtolower( $_SERVER['REQUEST_URI']) && !getUrlQuery($_SERVER['REQUEST_URI']) && strpos($_SERVER['REQUEST_URI'], '/brands/')) {
    header('Location: https://'.$_SERVER['HTTP_HOST'] .
        strtolower($_SERVER['REQUEST_URI']), true, 301);
    exit();
}

AddEventHandler("search", "BeforeIndex", "BeforeIndexHandler");
function BeforeIndexHandler($arFields) {
    $arrIblock = array(20);
    $arDelFields = array("DETAIL_TEXT", "PREVIEW_TEXT") ;
    if (CModule::IncludeModule('iblock') && $arFields["MODULE_ID"] == 'iblock' && in_array($arFields["PARAM2"], $arrIblock) && intval($arFields["ITEM_ID"]) > 0){
        $dbElement = CIblockElement::GetByID($arFields["ITEM_ID"]) ;
        if ($arElement = $dbElement->Fetch()){
            foreach ($arDelFields as $value){
                if (isset ($arElement[$value]) && strlen($arElement[$value]) > 0){
                    $arFields["BODY"] = str_replace (CSearch::KillTags($arElement[$value]) , "", CSearch::KillTags($arFields["BODY"]) );
                }
            }
        }
        return $arFields;
    }
}



// Удаляем лишние пробелы перед выводом
AddEventHandler("main", "OnEndBufferContent", "ChangeMyContent");

function ChangeMyContent(&$content)
{
    $descr = htmlspecialchars(parseDescription($content));

    $content = str_replace("#OG_DESCRIPTION#", $descr, $content);
  
}

function parseDescription($html) {
    // Get the 'content' attribute value in a <meta name="description" ... />
    $matches = array();

    // Search for <meta name="description" content="Buy my stuff" />
    preg_match('/<meta.*?name=("|\')description("|\').*?content=("|\')(.*?)("|\')/i', $html, $matches);
    if (count($matches) > 4) {
        return trim($matches[4]);
    }

    // Order of attributes could be swapped around: <meta content="Buy my stuff" name="description" />
    preg_match('/<meta.*?content=("|\')(.*?)("|\').*?name=("|\')description("|\')/i', $html, $matches);
    if (count($matches) > 2) {
        return trim($matches[2]);
    }

    // No match
    return 'Стоматологическое оборудование - купить оборудование для стоматологического кабинета СТОМАРТ';
}


//-- Добавление обработчика события
AddEventHandler("sale", "OnOrderNewSendEmail", "bxModifySaleMails");
//-- Собственно обработчик события
function bxModifySaleMails($orderID, &$eventName, &$arFields) {
$arOrder = CSaleOrder::GetByID($orderID);

//-- получаем телефоны и адрес
$order_props = CSaleOrderPropsValue::GetOrderProps($orderID);
$type = "";
$phone = "";
$inn = "";
$address = "";
$kpp = "";
$company = "";

while ($arProps = $order_props->Fetch()) {
	if ($arProps["CODE"] == "PERSON_TYPES") {
		$type = $arProps["VALUE"];
	}
	if ($arProps["CODE"] == "PHONE") {
		$phone = htmlspecialchars($arProps["VALUE"]);
	}
	if ($arProps["CODE"] == "INN") {
		$inn = $arProps["VALUE"];
	}
	if ($arProps["CODE"] == "ADDRESS")
	{
	$address = $arProps["VALUE"];
	}
	if ($arProps["CODE"] == "KPP")
	{
	$kpp = $arProps["VALUE"];
	}	
	if ($arProps["CODE"] == "COMPANY")
	{
	$company = $arProps["VALUE"];
	}	
	$full_address = $address;
}

//-- добавляем новые поля в массив результатов
$arFields["PERSON_TYPES"] =  $type;
$arFields["PHONE"] =  $phone;
$arFields["INN"] =  $inn;
$arFields["KPP"] =  $kpp;
$arFields["FULL_ADDRESS"] = $full_address;
$arFields["COMPANY"] =  $company;
}