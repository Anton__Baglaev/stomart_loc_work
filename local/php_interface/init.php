<?
function update_metrika_stat_block($data){
    $wc_metrika_stat_block_id = 3;
    \Bitrix\Main\Loader::IncludeModule("highloadblock");

    $hlblock = \Bitrix\Highloadblock\HighloadBlockTable::getById($wc_metrika_stat_block_id)->fetch();
    $entity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);
    $entityDataClass = $entity->getDataClass();

    $rows = [];
    $search_upd = [];
    foreach ($data as $item) {
        $rows[] = array(
            'UF_WCSTAT_URL'         => $item["url"],
            'UF_WCSTAT_COUNT'       => $item["count"],
            //'UF_WCSTAT_UPDATED'     => time()
        );
        $search_upd[$item["url"]] = $item["count"];
    }

    $added = $entityDataClass::addMulti($rows, true);
    if ($added){
        delete_metrika_old_stat();
    }

    $arFilter = array("IBLOCK_ID"=> 20);

    $sections_res = CIBlockSection::GetList([], $arFilter, false, [], false);
    while($ar_res = $sections_res->GetNext()) {
        $section_url =  $ar_res['SECTION_PAGE_URL'];
        //echo "<br>".$section_url." - ".$search_upd[$section_url];

        if (!empty($search_upd[$section_url])) {
            $section_id = "S".$ar_res["ID"];
            wc_update_search_rank($section_id, $search_upd[$section_url]);
        }
    }

    $elements_res = CIBlockElement::GetList([], $arFilter, false, [], false);

    while($ar_res = $elements_res->GetNext()) {
        $element_url =  $ar_res['DETAIL_PAGE_URL'];
        if (!empty($search_upd[$element_url])) {
            wc_update_search_rank($ar_res["ID"], $search_upd[$element_url]);
            wc_update_iblock_rank($ar_res["ID"], $search_upd[$element_url]);
        }
    }

    exit();
}

function wc_update_iblock_rank($section_id, $rank){
    $connection = \Bitrix\Main\Application::getConnection();
    $rank = (int)$rank + 5000;
    $rank2 = (int)$rank + 50000;

    $sql = "UPDATE b_iblock_element SET SORT=".(int)$rank. ", SHOW_COUNTER=".(int)$rank2." WHERE ID='".$section_id."' AND IBLOCK_ID=20";
    $connection->queryExecute($sql);
}


function wc_update_search_rank($section_id, $rank){

    $connection = \Bitrix\Main\Application::getConnection();
    $helper = $connection->getSqlHelper();

    $sql = "UPDATE b_search_content SET CUSTOM_RANK=".(int)$rank. " WHERE ITEM_ID='".$section_id."' AND MODULE_ID='iblock'";
    $connection->queryExecute($sql);
}

function delete_metrika_old_stat(){
    $wc_metrika_stat_block_id = 3;
    \Bitrix\Main\Loader::IncludeModule("highloadblock");

    $hlblock = \Bitrix\Highloadblock\HighloadBlockTable::getById($wc_metrika_stat_block_id)->fetch();
    $entity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);
    $entityDataClass = $entity->getDataClass();


    $connection = \Bitrix\Main\Application::getConnection();
    $helper = $connection->getSqlHelper();

    $tableName = $entity->getDBTableName();
    $today = date('Y-m-d H:i:s', time()-10);
    $where = "UF_WCSTAT_UPDATED < '".$today."'" ;
    $sql = "DELETE FROM ".$helper->quote($tableName)." WHERE ".$where;

    $connection->queryExecute($sql);
    //$connection->queryExecute("ALTER TABLE ".$helper->quote($tableName)." AUTO_INCREMENT = 1");

    $entity->cleanCache();
}

function update_metrika_statistics(){

    $url = 'https://api-metrika.yandex.ru/stat/v1/data';
    $params = array(
        'ids'         => '16113817',
        'metrics'     => 'ym:pv:pageviews',
        'dimensions'     => 'ym:pv:URLPath',
        'limit'     => '300',
        'accuracy' => '1',
        //'date1'         => '90daysAgo',
        'date1'         => '2022-03-01',
        'date2'          => 'today'
        //'date2'          => '2022-09-01'
    );
    $context = stream_context_create(array(
        'http' => array(
            'method' => 'GET',
            'header' => 'Authorization: OAuth y0_AgAAAAAA7m_6AAht_wAAAADPbFd9GynpXXUsRYu4ydsYlWuJqTc-LOQ'.PHP_EOL.
                'Content-Type: application/x-yametrika+json' . PHP_EOL
        ),
    ));

    $metrika_result = file_get_contents( $url . '?' . http_build_query($params), false, $context);

    $metrika_result = json_decode($metrika_result, true);

    $data = [];
    foreach ($metrika_result["data"] as $item) {
        $url = $item["dimensions"][0]["name"];
        $count = $item["metrics"][0];

        $i = [];
        $i["url"] = $url;
        $i["count"] = $count;
        $data[] = $i;
    }

    update_metrika_stat_block($data);

}

if (!empty($_GET["wc_upd_stat"])) {
    update_metrika_statistics();
}

CModule::AddAutoloadClasses(
    '',
    array(
        'WCCFFacetStorage' => '/local/php_interface/wccffacetstorage.php',
    )
);

function file_get_contents_from_url($url, $headers = false){

    $data = file_get_contents($url);

    if ($data === false){

        if (function_exists('curl_init')){

            $curl = curl_init();

            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_HEADER, $headers);
            curl_setopt($curl, CURLOPT_TIMEOUT, 5);
            $data = curl_exec($curl);
            curl_close($curl);

        }

    }

	return $data;

}

/**
 * событие обновляющее бонусные рубли у товара при обновлении
 */

AddEventHandler("iblock", "OnBeforeIBlockElementUpdate", "OnProductUpdate");

function getBonusValueFromGoodId($id)
{
	$price_result = CPrice::GetBasePrice($id);

	$myPricesa = $price_result["PRICE"]; // тут присваиваю значения переменной
	$myPricesa = substr($myPricesa, 0, -3);// цена отображается по умолчанию например 2000.00, эта строчка удаляет с конца три символа!
	// бонусные рубли
	return round($myPricesa / 100);

}

function OnProductUpdate(&$arFields)
{
	$IBLOCK_ID = 20; // Указываем ID инфоблока для элементов которого нужно выполнять действия
	$IBLOCK_TP_ID = 21;


	if ($arFields['IBLOCK_ID'] == $IBLOCK_ID) {
		// исключим бренд allition
		$brand = current($arFields['PROPERTY_VALUES'][570]);

		if ($brand['VALUE'] != '13029' && $brand['VALUE'] != '1688') {
			$arFields['PROPERTY_VALUES']['586'] = getBonusValueFromGoodId($arFields["ID"]);
		}

        $arFields['DETAIL_TEXT'] = preg_replace('/<h2><span[^>]*>(.*)<\/span><\/h2>/', '<h2>$1</h2>', $arFields['DETAIL_TEXT']);
        $arFields['DETAIL_TEXT'] = preg_replace('/<h3><span[^>]*>(.*)<\/span><\/h3>/', '<h3>$1</h3>', $arFields['DETAIL_TEXT']);
        $arFields['DETAIL_TEXT'] = preg_replace('/font-size: (\d+)pt;/', 'font-size: $1px;', $arFields['DETAIL_TEXT']);
        $arFields['DETAIL_TEXT'] = preg_replace('/font-size: 12px;/', 'font-size: 13px;', $arFields['DETAIL_TEXT']);
	}

	if ($arFields['IBLOCK_ID'] == $IBLOCK_TP_ID) {
		$mxResult = CCatalogSku::GetProductInfo($arFields["ID"]);

		if (is_array($mxResult)) {
			$brand_find = CIBlockElement::GetProperty(20, $mxResult['ID'], array(), array("CODE" => "ATT_BRAND"));
			if ($brand = $brand_find->Fetch()) {
				if ($brand['VALUE'] != '13029' && $brand['VALUE'] != '1688') {
					$arFields['PROPERTY_VALUES']['1437'] = getBonusValueFromGoodId($arFields["ID"]);
				}
			}
		}
	}
}


function dump($myvar){
    global $USER;
    if ($USER->IsAdmin()){
        ?>
        <pre>
			<?print_r($myvar);?>
		</pre>
        <?
    }
}
//редиректна на нижний регистр
function getUrlQuery($url, $key = null)
{
    $parts = parse_url($url);
    if (!empty($parts['query'])) {
        parse_str($parts['query'], $query);
        if (is_null($key)) {
            return $query;
        } elseif (isset($query[$key])) {
            return $query[$key];
        }
    }

    return false;
}
//var_dump(strpos($_SERVER['REQUEST_URI'], '/brands/'));
if ( $_SERVER['REQUEST_URI'] != strtolower( $_SERVER['REQUEST_URI']) && !getUrlQuery($_SERVER['REQUEST_URI']) && strpos($_SERVER['REQUEST_URI'], '/brands/') == 0  && strpos($_SERVER['REQUEST_URI'], '~') == 0) {
    header('Location: https://'.$_SERVER['HTTP_HOST'] .
        strtolower($_SERVER['REQUEST_URI']), true, 301);
    exit();
}

function wc_get_search_model_variants($model) {
    $variants = array(
        preg_replace("/\s/", "", $model),
        preg_replace("/\-/", "", $model),
        preg_replace("/\s/", "-", $model),
        preg_replace("/\-/", " ", $model),
        preg_replace("/([^\-^\d^\s]+)(\d+)/", "$1-$2", $model),
    );
    $variants = array_values(array_unique($variants));

    return " ".implode(" ", $variants);

}

AddEventHandler("search", "BeforeIndex", "BeforeIndexHandler");
function BeforeIndexHandler($arFields) {
    $arrIblock = array(20);
    $arDelFields = array("DETAIL_TEXT", "PREVIEW_TEXT") ;


    if (CModule::IncludeModule('iblock') && $arFields["MODULE_ID"] == 'iblock' && in_array($arFields["PARAM2"], $arrIblock) && intval($arFields["ITEM_ID"]) > 0){
        $dbElement = CIblockElement::GetByID($arFields["ITEM_ID"]) ;

        if ($arElement = $dbElement->Fetch()){
            $db_props = CIBlockElement::GetProperty($arrIblock[0], $arFields["ITEM_ID"], array("sort" => "asc"), Array("CODE"=>"MODEL_1"));
            $model_search_extra = "";
            if($ar_props = $db_props->Fetch()) {
                $model = $ar_props["VALUE_ENUM"];
                if ($model) {
                    $model_search_extra = $model;
                }
            }
            foreach ($arDelFields as $value){
                if (isset ($arElement[$value]) && strlen($arElement[$value]) > 0){
                    $arFields["BODY"] = str_replace (CSearch::KillTags($arElement[$value]) , "", CSearch::KillTags($arFields["BODY"]) );
                }
            }

            if ($model_search_extra) {
                $arFields['TITLE'] .= wc_get_search_model_variants($model_search_extra);
            }
        }
        return $arFields;
    }
}



// Удаляем лишние пробелы перед выводом
AddEventHandler("main", "OnEndBufferContent", "ChangeMyContent");

function ChangeMyContent(&$content)
{
    $descr = htmlspecialchars(parseDescription($content));

    $content = str_replace("#OG_DESCRIPTION#", $descr, $content);

}

function parseDescription($html) {
    // Get the 'content' attribute value in a <meta name="description" ... />
    $matches = array();

    // Search for <meta name="description" content="Buy my stuff" />
    preg_match('/<meta.*?name=("|\')description("|\').*?content=("|\')(.*?)("|\')/i', $html, $matches);
    if (count($matches) > 4) {
        return trim($matches[4]);
    }

    // Order of attributes could be swapped around: <meta content="Buy my stuff" name="description" />
    preg_match('/<meta.*?content=("|\')(.*?)("|\').*?name=("|\')description("|\')/i', $html, $matches);
    if (count($matches) > 2) {
        return trim($matches[2]);
    }

    // No match
    return 'Стоматологическое оборудование - купить оборудование для стоматологического кабинета СТОМАРТ';
}


//-- Добавление обработчика события
AddEventHandler("sale", "OnOrderNewSendEmail", "bxModifySaleMails");
//-- Собственно обработчик события
function bxModifySaleMails($orderID, &$eventName, &$arFields) {
    $arOrder = CSaleOrder::GetByID($orderID);

    //-- получаем телефоны и адрес
    $order_props = CSaleOrderPropsValue::GetOrderProps($orderID);
    $type = "";
    $phone = "";
    $inn = "";
    $address = "";
    $kpp = "";
    $company = "";

    while ($arProps = $order_props->Fetch()) {
        if ($arProps["CODE"] == "PERSON_TYPES") {
            $type = $arProps["VALUE"];
        }
        if ($arProps["CODE"] == "PHONE") {
            $phone = htmlspecialchars($arProps["VALUE"]);
        }
        if ($arProps["CODE"] == "INN") {
            $inn = $arProps["VALUE"];
        }
        if ($arProps["CODE"] == "ADDRESS")
        {
        $address = $arProps["VALUE"];
        }
        if ($arProps["CODE"] == "KPP")
        {
        $kpp = $arProps["VALUE"];
        }
        if ($arProps["CODE"] == "COMPANY")
        {
        $company = $arProps["VALUE"];
        }
        $full_address = $address;
    }

    //-- добавляем новые поля в массив результатов
    $arFields["PERSON_TYPES"] =  $type;
    $arFields["PHONE"] =  $phone;
    $arFields["INN"] =  $inn;
    $arFields["KPP"] =  $kpp;
    $arFields["FULL_ADDRESS"] = $full_address;
    $arFields["COMPANY"] =  $company;

    // отправка заказа в Roistat
    roistat_send_request($arOrder);
}


function roistat_send_request($arOrder) {

    $params = [];
    $params['content'] = '';

    $products = CSaleBasket::GetList(array(), array("ORDER_ID" => $arOrder['ID']));

    while ($arItem = $products->Fetch()) {
        $params['content'] .= $arItem['QUANTITY'] . ' шт. по ' . (int) $arItem['PRICE'] . ' руб. - ' . $arItem['NAME'] . "\n";
    }

    $params['content'] .= $arOrder['USER_DESCRIPTION'] ? 'Комментарий: ' . $arOrder['USER_DESCRIPTION'] . "\n":'';

    $arDeliv = CSaleDelivery::GetByID($arOrder["DELIVERY_ID"]);
    if ($arDeliv)
    {
        $params['content'] .= "Способ доставки: {$arDeliv["NAME"]}\n";
    }

    $arPaySystem = CSalePaySystem::GetByID($arOrder["PAY_SYSTEM_ID"]);
    if ($arPaySystem)
    {
        $params['content'] .= "Способ оплаты: {$arPaySystem["NAME"]}\n";
    }

    $order_props = CSaleOrderPropsValue::GetOrderProps($arOrder['ID']);
    while ($arProps = $order_props->Fetch()) {
        if ($arProps["VALUE"])
            $params['content'] .= $arProps["NAME"] . ': ' . htmlspecialchars($arProps["VALUE"]) . "\n";

        if ($arProps["CODE"] == "PHONE") {
            $params['phone'] = htmlspecialchars($arProps["VALUE"]);
        }

        if ($arProps["CODE"] == "EMAIL") {
            $params['email'] = htmlspecialchars($arProps["VALUE"]);
        }

        if ($arProps["CODE"] == "FIO") {
            $params['name'] = htmlspecialchars($arProps["VALUE"]);
        }
    }

    $roistatData = array(
        'roistat' => isset($_COOKIE['roistat_visit']) ? $_COOKIE['roistat_visit'] : 'from_site',
        'key'     => 'ZDIyYWMyOGE2NWZkYjUxZDgyMmYyZjcyMjNmNjFjYzM6MTg4MDc1', // Ключ для интеграции с CRM, указывается в настройках интеграции с CRM.
        'title'   => 'Заказ с сайта', // Название сделки
        'comment' => $params['content'], // Комментарий к сделке
        'name'    => ($params['name']?:$params['phone']), // Имя клиента
        'email'   => ($params['email']?:''), // Email клиента
        'phone'   => ($params['phone']?:''), // Номер телефона клиента
        'order_creation_method' => 'Форма сайта', // Способ создания сделки (необязательный параметр). Укажите то значение, которое затем должно отображаться в аналитике в группировке "Способ создания заявки"
        'is_need_callback' => '0', // После создания в Roistat заявки, Roistat инициирует обратный звонок на номер клиента, если значение параметра равно 1 и в Ловце лидов включен индикатор обратного звонка.
        'callback_phone' => '', // Переопределяет номер, указанный в настройках обратного звонка.
        'sync'    => '0', //
        'is_need_check_order_in_processing' => '1', // Включение проверки заявок на дубли
        'is_need_check_order_in_processing_append' => '1', // Если создана дублирующая заявка, в нее будет добавлен комментарий об этом
        'is_skip_sending' => '0', // Не отправлять заявку в CRM.
        'fields'  => array(
            'price' => $arOrder['PRICE'],
            'responsible_user_id' => 4780924
        ),
    );

    //print_r($roistatData); exit;

    file_get_contents_from_url("https://cloud.roistat.com/api/proxy/1.0/leads/add?" . http_build_query($roistatData));
}


$eventManager = \Bitrix\Main\EventManager::getInstance();
$eventManager->addEventHandler(    'sale',    'OnOrderStatusSendEmail',    'wcStatusChange');

\Bitrix\Main\Loader::includeModule('sale');

function wcStatusChange($id, &$eventName, &$arFields, $val){
    CModule::IncludeModule("sale");
    $orderObj = \Bitrix\Sale\Order::load($id);

    if ($val == "Z") {
        $paymentCollection = $orderObj->getPaymentCollection();
        $payment = $paymentCollection[0];

        $orderUser = \Bitrix\Main\UserTable::getList(array(
            'filter' => array(
                '=ID' => $orderObj->getUserId(),
            ),
            'limit'=>1,
            'select'=>array('NAME','LAST_NAME'),

        ))->fetch();


        $r = \Bitrix\Main\Mail\Event::sendImmediate(array(
            "EVENT_NAME" => "WC_BUH_MAIL_ORDER_PAID",
            "LID" => "s1",
            'MESSAGE_ID' => 102,
            "C_FIELDS" => array(
                "EMAIL" => "buh@stomart.ru",
                'ORDER_ID' => $id,
                'ORDER_REAL_ID' => $id,
                'ORDER_SUM'=>$paymentCollection->getPaidSum(),
                "FULL_NAME" => $orderUser["NAME"]." ".$orderUser["LAST_NAME"]
            ),
        ));

        $r = \Bitrix\Main\Mail\Event::sendImmediate(array(
            "EVENT_NAME" => "WC_BUH_MAIL_ORDER_PAID",
            "LID" => "s1",
            'MESSAGE_ID' => 102,
            "C_FIELDS" => array(
                "EMAIL" => "buh01@stomart.ru",
                'ORDER_ID' => $id,
                'ORDER_REAL_ID' => $id,
                'ORDER_SUM'=>$paymentCollection->getPaidSum(),
                "FULL_NAME" => $orderUser["NAME"]." ".$orderUser["LAST_NAME"]
            ),
        ));


        /*ob_start();
        //var_dump($payment->getUserId());

        var_dump($orderUser);
        $z = ob_get_contents();

        file_put_contents($_SERVER["DOCUMENT_ROOT"]."/tmp.log", json_encode($z));*/

    }

    if (!in_array($val, ["DA", "M"])) return;
    //if ($val !== "DA") return;

    $paymentCollection = $orderObj->getPaymentCollection();
    $payment = $paymentCollection[0];
    $service = \Bitrix\Sale\PaySystem\Manager::getObjectById($payment->getPaymentSystemId());
    $context = \Bitrix\Main\Application::getInstance()->getContext();
    $r = $service->initiatePay($payment, $context->getRequest());
    $pay_url =  $r->getPaymentUrl();


    //$short_uri = "https://stomart.ru".CBXShortUri::GetShortUri('https://www.stomart.ru/personal/order/payment/?ORDER_ID='.$id);
    $short_uri = "https://stomart.ru".CBXShortUri::GetShortUri($pay_url);
    //file_put_contents($_SERVER["DOCUMENT_ROOT"]."/tmp.log", json_encode($pay_url).json_encode($short_uri));
    $short_uri_lower = strtolower($short_uri);

    $arOrder = CSaleOrder::GetByID($id);
    $res = CSaleBasket::GetList(array(), array("ORDER_ID" => $id));
    $goods = [];
    while ($arItem = $res->Fetch()) {
        $goods[] = $arItem["NAME"];
    }

    $arFields["ORDER_SUMM"] = $arOrder["PRICE"];
    $arFields["GOODS"] = implode("<br>", $goods);
    $arFields["ORDER_PAY_LINK"] = $short_uri;

    $db_props = CSaleOrderPropsValue::GetOrderProps($id);
    while ($arProps = $db_props->Fetch()) {
       $arFields['PROP_'.$arProps['CODE']] = $arProps['NAME'];
       $arFields['PROP_VALUE_'.$arProps['CODE']] = $arProps['VALUE'];
    }

    //file_put_contents($_SERVER["DOCUMENT_ROOT"]."/tmp.log", json_encode($arFields));

    $fields = array(
        'PHONE' => $arFields["PROP_VALUE_PHONE"],
        'ORDER_ID' => $arFields["ORDER_ID"],
        'ORDER_REAL_ID' => $arFields["ORDER_REAL_ID"],
	    'EMAIL' => $arFields["EMAIL"],
	    'ORDER_PAY_LINK' => $short_uri
    );

	$sms = new \Bitrix\Main\Sms\Event('SMS_ORDER_STATUS_CHANGED_DA', $fields);
	$sms->setSite("s1");
	$sms->setLanguage('ru');
	$sms->send(true);

}

AddEventHandler('main', 'OnBeforeUserRegister', 'wcCheckRegistration');

function password_generate($len)
{
	$data = '1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz!#$%^-';
	return substr(str_shuffle($data), 0, $len);
}

function wcCheckRegistration(&$arFields)
{
	if (!\Bitrix\Main\Context::getCurrent()->getRequest()->isAdminSection()) {
		$new_password = password_generate(8);
		$arFields['CONFIRM_PASSWORD'] = $new_password;
		$arFields['PASSWORD'] = $new_password;
		$arFields['LOGIN'] = $arFields["EMAIL"];
	}
}


if (isset($_SERVER['HTTP_REFERER']))
{
    /*
    *   При просмотре сайта через Яндекс.Метрику
    *   не запрещать показывать сайт во фрейме
    */
     
    $metrikaHosts = [
        'webvisor.com',
        'metrika.yandex',
        'metrika.yandex.ru',
        'metrika.yandex.ua',
        'metrika.yandex.com',
        'metrika.yandex.by',
        'metrika.yandex.kz',
        $_SERVER['HTTP_HOST'],
    ];
     
    $refHost = parse_url($_SERVER['HTTP_REFERER'], PHP_URL_HOST);
 
    if (in_array($refHost, $metrikaHosts))
    {
        define('BX_SECURITY_SKIP_FRAMECHECK', true);
    }
}

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/mcart.extramail/classes/general/include_part.php");
