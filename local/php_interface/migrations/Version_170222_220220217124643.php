<?php

namespace Sprint\Migration;


class Version_170222_220220217124643 extends Version
{
    protected $description = "Разделы инфоблока Команда";

    protected $moduleVersion = "4.0.2";

    /**
     * @throws Exceptions\HelperException
     * @return bool|void
     */
    public function up()
    {
        $helper = $this->getHelperManager();

        $iblockId = $helper->Iblock()->getIblockIdIfExists(
            'team',
            'info'
        );

        $helper->Iblock()->addSectionsFromTree(
            $iblockId,
            array (
  0 => 
  array (
    'NAME' => 'Руководство компании',
    'CODE' => '',
    'SORT' => '100',
    'ACTIVE' => 'Y',
    'XML_ID' => '',
    'DESCRIPTION' => 'Наши руководители – это международно признанные эксперты-практики с опытом работы в Силиконовой долине и ведущих российских интернет-агентствах. Огромный опыт позволяет им запускать разнокалиберные проекты и координировать работу свыше 150 удаленных сотрудников, разбросанных по всему миру – от Канады до Бали.',
    'DESCRIPTION_TYPE' => 'text',
  ),
  1 => 
  array (
    'NAME' => 'Коммерческий отдел',
    'CODE' => '',
    'SORT' => '200',
    'ACTIVE' => 'Y',
    'XML_ID' => '',
    'DESCRIPTION' => '',
    'DESCRIPTION_TYPE' => 'text',
  ),
  2 => 
  array (
    'NAME' => 'Отдел закупок',
    'CODE' => '',
    'SORT' => '300',
    'ACTIVE' => 'Y',
    'XML_ID' => '',
    'DESCRIPTION' => '',
    'DESCRIPTION_TYPE' => 'text',
  ),
  3 => 
  array (
    'NAME' => 'Отдел маркетинга',
    'CODE' => '',
    'SORT' => '400',
    'ACTIVE' => 'Y',
    'XML_ID' => '',
    'DESCRIPTION' => '',
    'DESCRIPTION_TYPE' => 'text',
  ),
  4 => 
  array (
    'NAME' => 'Отдел сервиса',
    'CODE' => '',
    'SORT' => '501',
    'ACTIVE' => 'Y',
    'XML_ID' => '',
    'DESCRIPTION' => '',
    'DESCRIPTION_TYPE' => 'text',
  ),
  5 => 
  array (
    'NAME' => 'Отдел логистики',
    'CODE' => '',
    'SORT' => '600',
    'ACTIVE' => 'Y',
    'XML_ID' => '',
    'DESCRIPTION' => '',
    'DESCRIPTION_TYPE' => 'text',
  ),
  6 => 
  array (
    'NAME' => 'Отдел бухгалтерии',
    'CODE' => '',
    'SORT' => '700',
    'ACTIVE' => 'Y',
    'XML_ID' => '',
    'DESCRIPTION' => '',
    'DESCRIPTION_TYPE' => 'text',
  ),
  7 => 
  array (
    'NAME' => 'Склад',
    'CODE' => '',
    'SORT' => '800',
    'ACTIVE' => 'Y',
    'XML_ID' => '',
    'DESCRIPTION' => '',
    'DESCRIPTION_TYPE' => 'text',
  ),
)        );
    }

    public function down()
    {
        //your code ...
    }
}
