<?php

namespace Sprint\Migration;


class Version_130822_20220813190918 extends Version
{
    protected $description = "Миграция с подсказками";

    protected $moduleVersion = "4.0.2";

    /**
     * @throws Exceptions\HelperException
     * @return bool|void
     */
    public function up()
    {
        $helper = $this->getHelperManager();
        $hlblockId = $helper->Hlblock()->saveHlblock(array (
  'NAME' => 'WcSearchSuggest',
  'TABLE_NAME' => 'wc_search_suggest',
  'LANG' => 
  array (
    'ru' => 
    array (
      'NAME' => 'WcSearchSuggest',
    ),
    'en' => 
    array (
      'NAME' => 'WcSearchSuggest',
    ),
  ),
));
        $helper->Hlblock()->saveField($hlblockId, array (
  'FIELD_NAME' => 'UF_TITLE',
  'USER_TYPE_ID' => 'string',
  'XML_ID' => 'WCS_TITLE',
  'SORT' => '100',
  'MULTIPLE' => 'N',
  'MANDATORY' => 'Y',
  'SHOW_FILTER' => 'N',
  'SHOW_IN_LIST' => 'Y',
  'EDIT_IN_LIST' => 'Y',
  'IS_SEARCHABLE' => 'N',
  'SETTINGS' => 
  array (
    'SIZE' => 20,
    'ROWS' => 1,
    'REGEXP' => '',
    'MIN_LENGTH' => 0,
    'MAX_LENGTH' => 0,
    'DEFAULT_VALUE' => '',
  ),
  'EDIT_FORM_LABEL' => 
  array (
    'br' => '',
    'en' => '',
    'fr' => '',
    'la' => '',
    'pl' => '',
    'ru' => 'Запрос',
    'ua' => '',
  ),
  'LIST_COLUMN_LABEL' => 
  array (
    'br' => '',
    'en' => '',
    'fr' => '',
    'la' => '',
    'pl' => '',
    'ru' => 'Запрос',
    'ua' => '',
  ),
  'LIST_FILTER_LABEL' => 
  array (
    'br' => '',
    'en' => '',
    'fr' => '',
    'la' => '',
    'pl' => '',
    'ru' => 'Запрос',
    'ua' => '',
  ),
  'ERROR_MESSAGE' => 
  array (
    'br' => '',
    'en' => '',
    'fr' => '',
    'la' => '',
    'pl' => '',
    'ru' => 'Запрос',
    'ua' => '',
  ),
  'HELP_MESSAGE' => 
  array (
    'br' => '',
    'en' => '',
    'fr' => '',
    'la' => '',
    'pl' => '',
    'ru' => 'Запрос',
    'ua' => '',
  ),
));
            $helper->Hlblock()->saveField($hlblockId, array (
  'FIELD_NAME' => 'UF_WCSUGGESTIONS',
  'USER_TYPE_ID' => 'string',
  'XML_ID' => 'WCSUGGESTIONS',
  'SORT' => '100',
  'MULTIPLE' => 'Y',
  'MANDATORY' => 'N',
  'SHOW_FILTER' => 'N',
  'SHOW_IN_LIST' => 'Y',
  'EDIT_IN_LIST' => 'Y',
  'IS_SEARCHABLE' => 'N',
  'SETTINGS' => 
  array (
    'SIZE' => 20,
    'ROWS' => 1,
    'REGEXP' => '',
    'MIN_LENGTH' => 0,
    'MAX_LENGTH' => 0,
    'DEFAULT_VALUE' => '',
  ),
  'EDIT_FORM_LABEL' => 
  array (
    'br' => '',
    'en' => '',
    'fr' => '',
    'la' => '',
    'pl' => '',
    'ru' => 'Подсказки',
    'ua' => '',
  ),
  'LIST_COLUMN_LABEL' => 
  array (
    'br' => '',
    'en' => '',
    'fr' => '',
    'la' => '',
    'pl' => '',
    'ru' => 'Подсказки',
    'ua' => '',
  ),
  'LIST_FILTER_LABEL' => 
  array (
    'br' => '',
    'en' => '',
    'fr' => '',
    'la' => '',
    'pl' => '',
    'ru' => 'Подсказки',
    'ua' => '',
  ),
  'ERROR_MESSAGE' => 
  array (
    'br' => '',
    'en' => '',
    'fr' => '',
    'la' => '',
    'pl' => '',
    'ru' => 'Подсказки',
    'ua' => '',
  ),
  'HELP_MESSAGE' => 
  array (
    'br' => '',
    'en' => '',
    'fr' => '',
    'la' => '',
    'pl' => '',
    'ru' => 'Подсказки',
    'ua' => '',
  ),
));
        }

    public function down()
    {
        //your code ...
    }
}
