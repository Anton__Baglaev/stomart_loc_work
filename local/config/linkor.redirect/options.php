<?php
return array (
  'redirect_www' => NULL,
  'redirect_slash' => 'Y',
  'redirect_index' => NULL,
  'redirect_multislash' => NULL,
  'redirect_manyslash' => NULL,
  'use_redirect_urls' => 'Y',
  'redirect_from_uppercase' => NULL,
  'redirect_from_404' => NULL,
);