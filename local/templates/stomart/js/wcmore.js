$(document).ready(function(){

    $(document).on('click', '.load-more-items', function(){
        var targetContainer = $('.w55 .item').last(),
            url =  $('.load-more-items').attr('data-url');

        if (url !== undefined) {
            $('#ajaxSection').addClass('processing');
            $.ajax({
                type: 'GET',
                url: url,
                dataType: 'html',
                success: function(data){
                    $('.bx-pagination.wc .bx-pagination-container').remove();

                    var elements = $(data).find('.w55 .item'),
                        pagination = $(data).find('.bx-pagination.wc .bx-pagination-container');

                    targetContainer.after(elements);
                    $('.bx-pagination.wc').append(pagination);

                },
                complete: function(){
                    $('#ajaxSection').removeClass('processing');
                }
            });
        }

    });

});
