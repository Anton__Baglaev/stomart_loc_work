$(document).ready(function () {

    $(document).on('click', '.load-more-items-blog', function () {
        var targetContainer = $('.tiles-list .tile-wrap').last(),
          url = $('.load-more-items-blog').attr('data-url');

        if (url !== undefined) {
            $('#main').addClass('processing');
            $.ajax({
                type: 'GET',
                url: url,
                dataType: 'html',
                success: function (data) {
                    $('.bx-pagination.wc .bx-pagination-container').remove();

                    var elements = $(data).find('.tiles-list .tile-wrap'),
                      pagination = $(data).find('.bx-pagination.wc .bx-pagination-container');

                    targetContainer.after(elements);
                    $('.bx-pagination.wc').append(pagination);

                },
                complete: function () {
                    $('#main').removeClass('processing');
                }
            });
        }

    });

});
