<?php
\Bitrix\Main\Loader::IncludeModule("sale");


$statusResult = \Bitrix\Sale\Internals\StatusTable::getList(array(
    'select' => array('*', "STATUS_LANG.*",),
    'order' => array('SORT' => 'ASC'),

    'filter' => array('TYPE' => 'O'),

    'runtime' => [
        'STATUS_LANG' => [
            'data_type' => \Bitrix\Sale\Internals\StatusLangTable::class,
            'reference' => [
                '=this.ID' => 'ref.STATUS_ID',
            ]
        ],
    ],

));




while ($status = $statusResult->fetch()) {
  //  print_r($status);
    if ($status['SALE_INTERNALS_STATUS_STATUS_LANG_LID'] != 'ru') {
        continue;
    }

	$arResult['PROGRESS_BAR'][] = $status;

}
/*
echo "<pre>";
print_r($arResult['PROGRESS_BAR']);
die();
*/

foreach ($arResult['BASKET'] as &$basketItem) {
	$product_article = '';
	$id_to_search = $basketItem["PRODUCT_ID"];

	$mxResult = CCatalogSku::GetProductInfo($basketItem["PRODUCT_ID"]);

	if (is_array($mxResult)) {
		$article_find = CIBlockElement::GetProperty(20, $mxResult['ID'], array(), array("CODE" => "CML2_ARTICLE"));
		if ($article_value = $article_find->Fetch()) {
			$product_article = $article_value["VALUE"];
		}
	}
	if (!$product_article) {
		$article_find = CIBlockElement::GetProperty(20, $basketItem["PRODUCT_ID"], array(), array("CODE" => "CML2_ARTICLE"));
		if ($article_value = $article_find->Fetch()) {
			$product_article = $article_value["VALUE"];
		}
	}

	$basketItem["CML2_ARTICLE"] = $product_article;
}

