$(function(){
	var openSmartFilterFlag = false;
	var changeSortParams = function(){
		window.location.href = $(this).find(".dropDownItem.selected").data("value");
	};

	$("#selectSortParams, #selectCountElements").on("change", changeSortParams);


	var openSmartFilter = function(event){

		var smartFilterOffset = 0;

		if($(".oFilter").length > 0){
			smartFilterOffset = $(".oFilter").offset().top;
		}

		// smartFilter block adaptive toggle
		if(!openSmartFilterFlag){
			$("#smartFilter").addClass("opened").css({"marginTop": (smartFilterOffset + 24) + "px", "top": "24px"});
			openSmartFilterFlag = true;
		}

		else{
			$("#smartFilter").removeClass("opened").removeAttr("style");
			openSmartFilterFlag = false;
		}

		return event.preventDefault();
	};

	var closeSmartFilter = function(event){
		if(openSmartFilterFlag){
			$("#smartFilter").removeClass("opened");
			openSmartFilterFlag = false;
		}
	};

	$(document).on("click", ".oSmartFilter", openSmartFilter);
    $(document).on("click", "#smartFilter, .oSmartFilter, .rangeSlider", function(event){
    	return event.stopImmediatePropagation();
    });
	$(document).on("click", closeSmartFilter);


	var blockMinHeight = 200;

	if ($('.brandsDescription').length && $('body').width()<768) {
		var blockHeight =  $('.brandsDescription').innerHeight();

		if (blockHeight > blockMinHeight){
			$('.brandsDescription').css('height', blockMinHeight);
			$('.brandsDescription').addClass('wcmin');
			$('.wc-show-more-text-wrapper').addClass('act').removeClass('hidden');

			$('.wc-show-more-text').click(function() {
				if( $('.brandsDescription').innerHeight() == blockMinHeight ) {
					$('.brandsDescription').removeClass('wcmin');
					$('.brandsDescription').animate({ height: blockHeight }, 500);

					$(this).text('Скрыть текст');
				} else {
					$('.brandsDescription').addClass('wcmin');
					$('.brandsDescription').animate({ height: blockMinHeight }, 500);
					$(this).text('Показать весь текст');
				}
				return false;
			});
		}

	}
});
