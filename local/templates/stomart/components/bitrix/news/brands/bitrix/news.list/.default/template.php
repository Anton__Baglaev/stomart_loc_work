<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$this->setFrameMode(true);
$alfa = array_column($arResult["ITEMS"], 'NAME');


function getFirstLetter($str)
{
    $str = trim($str);

    return strtoupper(mb_substr($str, 0, 1));
}


$alfaChars = array_map('getFirstLetter', $alfa);
$alfaChars = array_unique($alfaChars);
sort($alfaChars);

?>

    <div class="brands_list_char">
        <?
        $numerSelectorexist = false;
        foreach ($alfaChars as $char) {

            if (is_numeric($char) && !$numerSelectorexist) {
                $numerSelectorexist = true;
                ?><a href="javascript:void(0)" data-char="0-9">0-9</a><?
            } else if (is_numeric($char) && $numerSelectorexist) {
                continue;
            } else {
                ?><a href="javascript:void(0)" data-char="<?= $char ?>"><?= $char ?></a><?

            }
        }
        ?>

    </div>

<? if ($arParams["DISPLAY_TOP_PAGER"]) { ?><? echo $arResult["NAV_STRING"]; ?><? } ?>
<? if (!empty($arResult["ITEMS"])): ?>
    <div id="brandList">
    <div class="items clearfix">
    <?

    $numberSelectorExist = false;
    ?>
    <? foreach ($arResult["ITEMS"] as $key => $arElement): ?>
        <?
        $this->AddEditAction($arElement["ID"], $arElement["EDIT_LINK"], CIBlock::GetArrayByID($arElement["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($arElement["ID"], $arElement["DELETE_LINK"], CIBlock::GetArrayByID($arElement["IBLOCK_ID"], "ELEMENT_DELETE"), array());
        //$picture = CFile::ResizeImageGet($arElement["DETAIL_PICTURE"], array("width" => 150, "height" => 120), BX_RESIZE_IMAGE_PROPORTIONAL, false);
        //$picture["src"] = !empty($picture["src"]) ? $picture["src"] : SITE_TEMPLATE_PATH."/images/empty.png";


        $letterElement = strtoupper(mb_substr($arElement["NAME"], 0, 1));
        $nextLetter = strtoupper(mb_substr($arResult["ITEMS"][$key + 1]["NAME"], 0, 1));

        if (is_numeric($letterElement)) {
            $letterElement = '0-9';
        }
        if (is_numeric($nextLetter)) {
            $nextLetter = '0-9';
        }

        if ($currentLetter != $letterElement) {
            $currentLetter = $letterElement;
            if (is_numeric($letterElement) && !$numberSelectorExist) {
                $numberSelectorExist = true;
                ?><div class="row_item"><h2 class="letter" data-char="0-9">0-9</h2><?
            } else if (is_numeric($letterElement) && $numberSelectorExist) {

            } else {


                ?> <div class="row_item"><h2 class="letter"
                                             data-char="<?= $letterElement ?>"><?= $letterElement ?></h2><?

            }


        }


        ?>
        <div class="item" id="<?= $this->GetEditAreaId($arElement["ID"]); ?>" data-char="<?= $letterElement ?>">
            <div class="tabloid">
                <? if (!empty($arElement["PROPERTIES"]["OFFERS"]["VALUE"])): ?>
                    <div class="markerContainer">
                        <? foreach ($arElement["PROPERTIES"]["OFFERS"]["VALUE"] as $ifv => $marker): ?>
                            <div class="marker"
                                 style="background-color: <?= strstr($arElement["PROPERTIES"]["OFFERS"]["VALUE_XML_ID"][$ifv], "#") ? $arElement["PROPERTIES"]["OFFERS"]["VALUE_XML_ID"][$ifv] : "#424242" ?>"><?= $marker ?></div>
                        <? endforeach; ?>
                    </div>
                <? endif; ?>
                <a href="<?= $arElement["DETAIL_PAGE_URL"] ?>" class="brandName">

                    <p><?= $arElement["NAME"] ?></p>
                </a>
            </div>
        </div>
        <? if ($letterElement != $nextLetter) {

            echo '</div>';
        }


        ?>


    <? endforeach; ?>
    </div>
    </div>
<? else: ?>
    <div id="empty">
        <div class="emptyWrapper">
            <div class="pictureContainer">
                <img src="<?= SITE_TEMPLATE_PATH ?>/images/emptyFolder.png" alt="<?= GetMessage("EMPTY_HEADING") ?>"
                     class="emptyImg">
            </div>
            <div class="info">
                <h3><?= GetMessage("EMPTY_HEADING") ?></h3>
                <p><?= GetMessage("EMPTY_TEXT") ?></p>
                <a href="<?= SITE_DIR ?>" class="back"><?= GetMessage("MAIN_PAGE") ?></a>
            </div>
        </div>
        <? $APPLICATION->IncludeComponent("bitrix:menu", "emptyMenu", Array(
            "ROOT_MENU_TYPE" => "left",
            "MENU_CACHE_TYPE" => "N",
            "MENU_CACHE_TIME" => "3600",
            "MENU_CACHE_USE_GROUPS" => "Y",
            "MENU_CACHE_GET_VARS" => "",
            "MAX_LEVEL" => "1",
            "CHILD_MENU_TYPE" => "left",
            "USE_EXT" => "Y",
            "DELAY" => "N",
            "ALLOW_MULTI_SELECT" => "N",
        ),
            false
        ); ?>
    </div>
<? endif; ?>
    <br/>
<? if ($arParams["DISPLAY_BOTTOM_PAGER"]) { ?><? echo $arResult["NAV_STRING"]; ?><? } ?>