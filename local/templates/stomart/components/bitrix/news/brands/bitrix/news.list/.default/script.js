$('.brands_list_char a').click(function () {
    var char = $(this).data('char');

  
    $('.brands_list_char a').removeClass('active');
    $(this).addClass('active');

    $([document.documentElement, document.body]).animate({
        scrollTop: $('#brandList .item[data-char="' + char + '"]').offset().top - 50
    }, 500);


});