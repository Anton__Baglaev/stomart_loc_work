<div class="global-block-container">
	<div class="global-content-block">
		<div class="blog-banner banner-no-image">
			<div class="banner-elem">
				<div class="tb">
					<div class="text-wrap tc">
						<div class="tb">
							<div class="tr">
								<div class="tc">
									<?if(!empty($arResult["ACTIVE_FROM"])):?>
										<div class="date"><?=CIBlockFormatProperties::DateFormat($arParams["ACTIVE_DATE_FORMAT"], MakeTimeStamp($arResult["ACTIVE_FROM"], CSite::GetDateFormat()));?></div>
									<?endif;?>
									<?if(!empty($arResult["NAME"])):?>
										<h1 class="ff-medium"><?=$arResult["NAME"]?></h1>
									<?endif;?>
									<?if(!empty($arResult["PREVIEW_TEXT"])):?>
										<div class="descr"><?=$arResult["PREVIEW_TEXT"]?></div>
									<?endif;?>
								</div>
							</div>
							<div class="social">
								<div class="ya-share2" data-services="vkontakte,odnoklassniki,moimir,telegram"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="detail-text-wrap">
			<?=$arResult["DETAIL_TEXT"]?>

            <?if(count($arResult['PROPERTIES']["CAR_ITEMS_1"]['VALUE'])):?>

                <div id="stock_1" class="stockProduct">
                    <div class="wrap">
                        <ul class="slideBox productList">
                            <?foreach($arResult['PROPERTIES']["CAR_ITEMS_1"]['VALUE'] as $arElement):?>
                                <li>
                                    <?$APPLICATION->IncludeComponent(
                                        "dresscode:catalog.item",
                                        "shortWithAddCartButton",
                                        array(
                                            "PICTURE_HEIGHT" => '220',
                                            "PICTURE_WIDTH" =>'200',
                                            "HIDE_NOT_AVAILABLE" => $arParams["HIDE_NOT_AVAILABLE"],
                                            "PRODUCT_PRICE_CODE" => $arParams["PRODUCT_PRICE_CODE"],
                                            "LAZY_LOAD_PICTURES" => $arParams["LAZY_LOAD_PICTURES"],
                                            "CONVERT_CURRENCY" => $arParams["CONVERT_CURRENCY"],
                                            "HIDE_MEASURES" => $arParams["HIDE_MEASURES"],
                                            "CURRENCY_ID" => $arParams["CURRENCY_ID"],
                                            "IBLOCK_TYPE" => '1c_catalog',
                                            "IBLOCK_ID" => 20,
                                            "PRODUCT_ID" => $arElement,
                                            "CACHE_TIME" => 36000000, // not delete
                                            "CACHE_TYPE" => "Y", // not delete
                                        ),
                                        false,
                                        array("HIDE_ICONS" => "Y")
                                    );?>
                                </li>
                            <?endforeach;?>
                        </ul>
                    </div>
                </div>
            <?endif;?>

            <?=htmlspecialcharsBack($arResult['PROPERTIES']["CAR_TEXT_1"]['VALUE']['TEXT']);?>

            <?if(count($arResult['PROPERTIES']["CAR_ITEMS_2"]['VALUE'])):?>

                <div id="stock_2" class="stockProduct">
                    <div class="wrap">
                        <ul class="slideBox productList">
                            <?foreach($arResult['PROPERTIES']["CAR_ITEMS_2"]['VALUE'] as $arElement):?>
                                <li>
                                    <?$APPLICATION->IncludeComponent(
                                        "dresscode:catalog.item",
                                        "shortWithAddCartButton",
                                        array(
                                            "PICTURE_HEIGHT" => '220',
                                            "PICTURE_WIDTH" =>'200',
                                            "HIDE_NOT_AVAILABLE" => $arParams["HIDE_NOT_AVAILABLE"],
                                            "PRODUCT_PRICE_CODE" => $arParams["PRODUCT_PRICE_CODE"],
                                            "LAZY_LOAD_PICTURES" => $arParams["LAZY_LOAD_PICTURES"],
                                            "CONVERT_CURRENCY" => $arParams["CONVERT_CURRENCY"],
                                            "HIDE_MEASURES" => $arParams["HIDE_MEASURES"],
                                            "CURRENCY_ID" => $arParams["CURRENCY_ID"],
                                            "IBLOCK_TYPE" => '1c_catalog',
                                            "IBLOCK_ID" => 20,
                                            "PRODUCT_ID" => $arElement,
                                            "CACHE_TIME" => 36000000, // not delete
                                            "CACHE_TYPE" => "Y", // not delete
                                        ),
                                        false,
                                        array("HIDE_ICONS" => "Y")
                                    );?>
                                </li>
                            <?endforeach;?>
                        </ul>
                    </div>
                </div>
            <?endif;?>

            <?=htmlspecialcharsBack($arResult['PROPERTIES']["CAR_TEXT_2"]['VALUE']['TEXT']);?>

            <?if(count($arResult['PROPERTIES']["CAR_ITEMS_3"]['VALUE'])):?>

                <div id="stock_3" class="stockProduct">
                    <div class="wrap">
                        <ul class="slideBox productList">
                            <?foreach($arResult['PROPERTIES']["CAR_ITEMS_3"]['VALUE'] as $arElement):?>
                                <li>
                                    <?$APPLICATION->IncludeComponent(
                                        "dresscode:catalog.item",
                                        "shortWithAddCartButton",
                                        array(
                                            "PICTURE_HEIGHT" => '220',
                                            "PICTURE_WIDTH" =>'200',
                                            "HIDE_NOT_AVAILABLE" => $arParams["HIDE_NOT_AVAILABLE"],
                                            "PRODUCT_PRICE_CODE" => $arParams["PRODUCT_PRICE_CODE"],
                                            "LAZY_LOAD_PICTURES" => $arParams["LAZY_LOAD_PICTURES"],
                                            "CONVERT_CURRENCY" => $arParams["CONVERT_CURRENCY"],
                                            "HIDE_MEASURES" => $arParams["HIDE_MEASURES"],
                                            "CURRENCY_ID" => $arParams["CURRENCY_ID"],
                                            "IBLOCK_TYPE" => '1c_catalog',
                                            "IBLOCK_ID" => 20,
                                            "PRODUCT_ID" => $arElement,
                                            "CACHE_TIME" => 36000000, // not delete
                                            "CACHE_TYPE" => "Y", // not delete
                                        ),
                                        false,
                                        array("HIDE_ICONS" => "Y")
                                    );?>
                                </li>
                            <?endforeach;?>
                        </ul>
                    </div>
                </div>
            <?endif;?>

            <?=htmlspecialcharsBack($arResult['PROPERTIES']["CAR_TEXT_3"]['VALUE']['TEXT']);?>

            <?if(count($arResult['PROPERTIES']["CAR_ITEMS_4"]['VALUE'])):?>

                <div id="stock_4" class="stockProduct">
                    <div class="wrap">
                        <ul class="slideBox productList">
                            <?foreach($arResult['PROPERTIES']["CAR_ITEMS_4"]['VALUE'] as $arElement):?>
                                <li>
                                    <?$APPLICATION->IncludeComponent(
                                        "dresscode:catalog.item",
                                        "shortWithAddCartButton",
                                        array(
                                            "PICTURE_HEIGHT" => '220',
                                            "PICTURE_WIDTH" =>'200',
                                            "HIDE_NOT_AVAILABLE" => $arParams["HIDE_NOT_AVAILABLE"],
                                            "PRODUCT_PRICE_CODE" => $arParams["PRODUCT_PRICE_CODE"],
                                            "LAZY_LOAD_PICTURES" => $arParams["LAZY_LOAD_PICTURES"],
                                            "CONVERT_CURRENCY" => $arParams["CONVERT_CURRENCY"],
                                            "HIDE_MEASURES" => $arParams["HIDE_MEASURES"],
                                            "CURRENCY_ID" => $arParams["CURRENCY_ID"],
                                            "IBLOCK_TYPE" => '1c_catalog',
                                            "IBLOCK_ID" => 20,
                                            "PRODUCT_ID" => $arElement,
                                            "CACHE_TIME" => 36000000, // not delete
                                            "CACHE_TYPE" => "Y", // not delete
                                        ),
                                        false,
                                        array("HIDE_ICONS" => "Y")
                                    );?>
                                </li>
                            <?endforeach;?>
                        </ul>
                    </div>
                </div>
            <?endif;?>

            <?=htmlspecialcharsBack($arResult['PROPERTIES']["CAR_TEXT_4"]['VALUE']['TEXT']);?>

            
            <?if(!empty($arResult['PROPERTIES']["SECTION_LINK"]['VALUE'])):?>
                <?
                $url_section = $arResult['PROPERTIES']["SECTION_LINK"]['VALUE'];
                ?>
                <div class="detail-text-wrap">
                    <div class="consultation-wrap">
                        <div class="tb">
                            <div class="tc">
                                <div class="tb">
                                    <div class="tc">
                                        <div class="consultation-heading">
                                            Выбрать интересующий товар
                                        </div>
                                        <div class="text">
                                            Указанные в статье товары по кнопке "Перейти в раздел"
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tc consultation-btn-wrap">
                                <a href="<?=$url_section?>" class="btn-simple btn-medium">
                                    Перейти в раздел
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            <?endif;?>
			<div class="btn-simple-wrap">
				<a href="<?=$arResult["LIST_PAGE_URL"]?>" class="btn-simple btn-small"><?=GetMessage("NEWS_BACK")?></a>
			</div>
		</div>
	</div>
	<?global $arrFilter; $arrFilter["!ID"] = $arResult["ID"];?>
	<?$APPLICATION->IncludeComponent(
		"bitrix:news.list",
		"blogDetail",
		array_merge($arParams, array("NEWS_COUNT" => 3, "FILTER_NAME" => "arrFilter", "INCLUDE_IBLOCK_INTO_CHAIN" => "N", "ADD_SECTIONS_CHAIN" => "N", "ADD_ELEMENT_CHAIN" => "N", "SET_TITLE" => "N", "DISPLAY_TOP_PAGER" => "N", "DISPLAY_BOTTOM_PAGER" => "N")),
		$component
	);?>
</div>

<script src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js" charset="utf-8"></script>
<script src="//yastatic.net/share2/share.js" charset="utf-8"></script>
