<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>
<?if(!empty($arResult)):?>
	<?
		//get banner picture resize
		if(!empty($arResult["DISPLAY_PROPERTIES"]["BG_IMAGE"]["FILE_VALUE"])){
			$arResult["RESIZE_BANNER_PICTURE"] = CFile::ResizeImageGet($arResult["DISPLAY_PROPERTIES"]["BG_IMAGE"]["FILE_VALUE"], array("width" => 1920, "height" => 500), BX_RESIZE_IMAGE_PROPORTIONAL, false, false, false, 85);
		}
		//get detail picture resize
		if(!empty($arResult["DETAIL_PICTURE"])){
			$arResult["RESIZE_DETAIL_PICTURE"] = CFile::ResizeImageGet($arResult["DETAIL_PICTURE"], array("width" => 550, "height" => 500), BX_RESIZE_IMAGE_PROPORTIONAL, false, false, false, 85);
		}
	?>
	<?//include view?>
	<?if(!empty($arResult["RESIZE_BANNER_PICTURE"])):?>
		<?include_once("include/sale-detail-banner.php");?>
	<?elseif(!empty($arResult["DETAIL_PICTURE"])):?>
		<?include_once("include/sale-detail-image.php");?>
	<?else:?>
		<?include_once("include/sale-detail-no-image.php");?>
	<?endif;?>
<?endif;?>