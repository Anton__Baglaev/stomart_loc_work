$(function(){
    $( document ).ready(function() {
	    $('.file-id-span').each(function () {
		    const nameFile = $(this).data('file-name');
		    const form_id = $(this).data('form-id');
		    const existText = $('.openWebFormModal[data-id=' + form_id + ']').text();
		    $('.openWebFormModal[data-id=' + form_id + ']').text(existText + ' ' + nameFile);
	    });
    });
function downloadinstruction(form) {
	if (!form) {
		form = $("form[name='SIMPLE_FORM_4']");
	}
	const form_id = form.parents(".webFormDwModal").data('id');
	const mail = form.find('input[name^="form_email_"]').eq(0).val();
	const msg = form.serialize() + '&file-id=' + $('.file-id-span[data-form-id=' + form_id + ']').data('file-id') + '&form_email=' + mail;
	$.ajax({
		type: 'POST',
		url: '/ajax/get-docs.php', // Обработчик собственно
		data: msg,
		success: function (data) {

			if (data.status == 'ok') {
				console.log(data)
			} else {
				console.log(data)

			}


		},
	});
}


	var $modalForms = $(".webformModal.download-instruction");

	var sendWebFormDI = function (event, data) {

		var requiredErrorPosition = false;
		var requiredError = false;

		var $thisForm = $(this).addClass("loading");
		var $parentThis = $thisForm.parents(".webFormDwModal");
		var $thisFormFields = $thisForm.find(".webFormItemField");
		var $thisFormErrors = $thisForm.find(".webFormItemError");
		var $submitButton = $thisForm.find('input[type="submit"]').addClass("loading");
		var $webFormError = $thisForm.find(".webFormError");
		var $webFormCaptchaSid = $thisForm.find(".webFormCaptchaSid");
		var $webFormCaptchaImage = $thisForm.find(".webFormCaptchaImage");

		var formId = $parentThis.data("id");

		$thisFormFields.each(function(i, nextField){

			var $nextField = $(nextField);
			if($nextField.data("required") == "Y"){
				var $nextFieldEx = $nextField.find('input[type="text"], input[type="password"], input[type="file"], select, textarea');
				if($nextFieldEx.attr("name")){
					if(!$nextFieldEx.val() || $nextFieldEx.val().length == 0){
						$nextFieldEx.addClass("error");
						if(!requiredError){
							requiredErrorPosition = $nextFieldEx.offset().top;
							requiredError = true;
						}
					}
				}

			}
		});

		//get form data
		var formData = new FormData(this);

		//set control
		formData.append("control", Math.floor((new Date()).getTime() / 1000));

		var $personalInfo = $thisForm.find(".personalInfoField");
		if(!$personalInfo.prop("checked")){
			$personalInfo.addClass("error");
			requiredError = true;
		}

		//check errors
		if(requiredError == false){
	  		$.ajax({
				  url: webFormAjaxDirDI + "?FORM_ID=" + formId + "&SITE_ID=" + webFormSiteId,
				  data: formData,
				  cache: false,
				  contentType: false,
				  processData: false,
				  enctype: "multipart/form-data",
				  type: "POST",
				  dataType: "json",
				  success: function (response) {

					  //remove error labels
	  				$thisFormErrors.empty().removeClass("visible");
	  				$webFormError.empty().removeClass("visible");

					  if (response["SUCCESS"] != "Y") {

						  //set errors
						  $.each(response["ERROR"], function (nextId, nextValue) {
							  var $errorItemContainer = $("#WEB_FORM_ITEM_" + nextId);
							  if (nextId != 0 && $errorItemContainer) {
								  $errorItemContainer.find(".webFormItemError").html(nextValue).addClass("visible");
							  } else {
								  $webFormError.append(nextValue).addClass("visible");
							  }
						  });

						  // reload captcha
						  if (response["CAPTCHA"]) {
							  $webFormCaptchaSid.val(response["CAPTCHA"]["CODE"]);
							  $webFormCaptchaImage.attr("src", response["CAPTCHA"]["PICTURE"]);
						  }

					  } else {
						  $("#webFormMessage_" + formId).css({
							  display: "block"
						  });
						  $(".webformModal").removeClass("visible");
						  downloadinstruction($thisForm);
						  setTimeout($thisForm[0].reset, 500);


					  }

		  			//remove loader
		  			$thisForm.removeClass("loading");
		  			$submitButton.removeClass("loading");

		  		}

	  		});
	  	}else{

	  		// if(requiredErrorPosition){
	  		// 	$("html, body").animate({
	  		// 		"scrollTop": requiredErrorPosition - $(window).height() / 2
	  		// 	}, 250);
	  		// }

	  		$thisForm.removeClass("loading");
	  		$submitButton.removeClass("loading");
	  	}

		return event.preventDefault();

	}

	var removeErrorsDI = function (event) {
		$(this).removeClass("error");
	};

	var webFormExitDI = function (event) {
		$(".webFormMessage").hide();
		return event.preventDefault();
	}

	var openWebFormModalDI = function (event) {
		//   $('.button-widget-open').click();
		// return event.preventDefault();

		var $thisForm = $("#webFormDwModal_" + $(this).data("id"));
		var $thisFormCn = $thisForm.find(".webformModalContainer");

		$thisForm.addClass("visible");

		if ($thisFormCn.height() < $(window).height()) {
			$thisForm.addClass("small");
		}

		return event.preventDefault();

	}

	var closeWebFormModalDI = function (event) {
		$(this).parents(".webformModal").removeClass("visible");
		return event.preventDefault();
	};

	var checkFormsHeight = function(event){

		//get window height now
		var windowHeightNow = $(this).height();

		//get web forms
		$modalForms.each(function(i, nextForm){

			//vars
			var $nextForm = $(nextForm);
			var $nextFormCn = $nextForm.find(".webformModalContainer");

			//class change
			if ($nextFormCn.height() < windowHeightNow) {
				$nextForm.addClass("small");
			} else {
				$nextForm.removeClass("small");
			}

		});
	}

	$(document).on("focus", ".download-instruction .webFormItemField input, .webFormItemField select, .webFormItemField textarea", removeErrorsDI);
	$(document).on("click", ".download-instruction .webFormModalHeadingExit", closeWebFormModalDI);
	$(document).on("click", ".openWebFormModal", openWebFormModalDI);
	$(document).on("click", ".webFormMessageExit", webFormExitDI);
	$(document).off("submit").on("submit", ".download-instruction .webFormDwModal form", sendWebFormDI);

	if ($modalForms.length > 0) {
		$(window).on("resize", checkFormsHeight);
	}

});
