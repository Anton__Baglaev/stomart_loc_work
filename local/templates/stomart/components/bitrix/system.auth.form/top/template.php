<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?$this->setFrameMode(true);

?>

<?if($arResult["FORM_TYPE"] == "login"):?>
	<li class="top-auth-login"><a href="<?=SITE_DIR?>auth/"><?=GetMessage("LOGIN")?></a></li>
	<li class="top-auth-register"><a href="<?=SITE_DIR?>auth/?register=yes"><?=GetMessage("REGISTER")?></a></li>
<?else:?>


        <? $APPLICATION->IncludeComponent(    "bitrix:sale.personal.account_custom", "header",    Array(),    false        ) ?>

	<li class="top-auth-exit"><a href="<?=SITE_DIR?>exit/"><?=GetMessage("EXIT")?></a></li>
<?endif?>
