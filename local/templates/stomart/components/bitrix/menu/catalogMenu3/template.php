<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<? $this->setFrameMode(true);
$page = $APPLICATION->GetCurPage();
$closedPage = 'ustanovki_i_aksessuary';
$isNeedNoIndex = strpos($page,$closedPage);
//print_r(123123123123123);
?>
<? if (CModule::IncludeModule("currency")): ?>
    <? if (!empty($arResult)): ?>
        <div id="mainMenuContainer">
            <div class="wpLimiter">
                <a href="<?= SITE_DIR ?>catalog/" class="minCatalogButton" id="catalogSlideButton">
                    <img src="<?= SITE_TEMPLATE_PATH ?>/images/catalogButton.png"
                         alt=""> <?= GetMessage("CATALOG_BUTTON_LABEL") ?>
                </a>
                <? /* if (count($arResult["ITEMS"]) > 1): */ ?>
                <div id="menuCatalogSection">
                    <div class="menuSection">
                        <a href="javascript:void(0);" class="catalogButton"><img
                                    src="<?= SITE_TEMPLATE_PATH ?>/images/catalogButton.png"
                                    alt=""><?= GetMessage("CATALOG_BUTTON_LABEL") ?> <img
                                    src="<?= SITE_TEMPLATE_PATH ?>/images/sectionMenuArrow.png" alt=""
                                    class="sectionMenuArrow"></a>
                        <div class="drop">
                            <div class="limiter">
                                <ul class="menuSectionList">
                                    <li class="sectionColumn open">
                                        <div class="container flex-container">
                                        <a href="/catalog/" class="picture">
                                                        <img src="/local/templates/stomart/images/all-catecory.png" alt="Все категории" title="Все категории">
                                                    </a>
                                            <a href="/catalog/" class="menuLink" style="font-weight: bold">
                                                Все категории
                                            </a>
                                            <!-- Стрелка вправо -->
                                            <svg class="arrow-right-3" viewBox="0 0 5 9">
                                                <path d="M0.419,9.000 L0.003,8.606 L4.164,4.500 L0.003,0.394 L0.419,0.000 L4.997,4.500 L0.419,9.000 Z"/>
                                            </svg>
                                        </div>
                                        <? if (!empty($arResult["ITEMS"])): ?>
                                        <? if ($isNeedNoIndex) {echo '<noindex>'; } ?>
                                        <div class="drop-inner ">
                                            <div class="limiter">
                                                <? $arResult["ITEMS"] = array_values($arResult["ITEMS"]);
                                                $count = 0;
                                                $mainCount = 0;
                                                $lengthArResult = count($arResult["ITEMS"]);
                                                ?>

                                                <? foreach ($arResult["ITEMS"] as $x2 => $next2Element): ?>
                                                <?

                                                if ($count == 0) {
                                                    echo ' <ul class="nextColumn">';

                                                }
                                                ?>
                                    <li<? if (!empty($next2Element["ELEMENTS"])): ?> class="allow-dropdown"<? endif; ?>>
                                        <a href="<?= $next2Element["LINK"] ?>"
                                           class="menu2Link menu2LinkHeader<? if ($next2Element["SELECTED"]): ?> selected<? endif ?>">
                                            <?= $next2Element["TEXT"] ?>
                                        </a>
                                        <? if (!empty($next2Element["ELEMENTS"])): ?>
                                            <?
                                            // echo"<pre>";
                                            // print_r($next2Element);
                                            ?>
                                            <div class="x3-section">
                                                <? $elNumber = 0; ?>
                                                <? foreach (array_shift(array_values($next2Element["ELEMENTS"])) as $x3 => $next3Element): ?>

                                                    <div class="x3-item<? if ($elNumber > 3): ?> hidden<? endif ?>">
                                                        <a href="<?= $next3Element["LINK"] ?>"
                                                           class="menu2Link<? if ($next3Element["SELECTED"]): ?> selected<? endif ?>">
                                                            <?= $next3Element["TEXT"] ?>
                                                        </a>
                                                    </div>
                                                    <? $elNumber++; ?>
                                                <? endforeach; ?>
                                            </div>
                                        <? endif; ?>


                                    <? if (count($next2Element["ELEMENTS"][1]) > 4): ?>
                                        <div onmouseover="showMenuItem(this);"
                                            class="drop-more"><span>Еще</span></div>
                                    <? endif; ?>
                                    </li>
                                    <?

                                    $count++;
                                    $mainCount++;

                                    if ($count == 4 || $lengthArResult == $mainCount) {
                                        echo '</ul>';
                                        $count = 0;
                                    }
                                    ?>

                                    <? endforeach; ?>
</div></div>
                                    </li>


                                    <? /* endif; */ ?>
                                    <? foreach ($arResult["ITEMS"] as $nextElement): ?>
                                        <li class="sectionColumn">
                                            <div class="container flex-container">
                                                <? if (!empty($nextElement["DETAIL_PICTURE"])): ?>
                                                    <a href="<?= $nextElement["LINK"] ?>" class="picture">
                                                        <img src="<?= $nextElement["DETAIL_PICTURE"]["src"] ?>"
                                                             alt="<?= $nextElement["TEXT"] ?>" title="<?= $nextElement["TEXT"] ?>">
                                                    </a>
                                                <? endif; ?>

                                                <a href="<?= $nextElement["LINK"] ?>"
                                                   class="menuLink<? if ($nextElement["SELECTED"]): ?> selected<? endif ?>">
                                                    <?= $nextElement["TEXT"] ?>
                                                </a>

                                                <!-- Стрелка вправо -->
                                                <svg class="arrow-right-3" viewBox="0 0 5 9">
                                                    <path d="M0.419,9.000 L0.003,8.606 L4.164,4.500 L0.003,0.394 L0.419,0.000 L4.997,4.500 L0.419,9.000 Z"/>
                                                </svg>
                                            </div>
                                            <? if (!empty($nextElement["ELEMENTS"])): ?>
                                                <div class="drop-inner ">
                                                    <div class="limiter">
                                                        <ul class="nextColumn">
                                                            <li>
                                                                <a href="<?= $nextElement["LINK"] ?>"
                                                                   class="menu2LinkHeader"><?= $nextElement["TEXT"] ?></a>
                                                            </li>
                                                            <? foreach ($nextElement["ELEMENTS"] as $next2Column): ?>
                                                                <? if (!empty($next2Column)): ?>

                                                                    <? $next2Column = array_values($next2Column);
                                                                    $count = 0;
                                                                    ?>
                                                                    <? foreach ($next2Column as $x2 => $next2Element): ?>


                                                                        <? /*

                                            if ($count == 0) {
                                                echo ' <ul class="nextColumn">';
                                                echo ' <a href="'. $next2Element["LINK"].'" class="menu2LinkHeader">'. $next2Element["TEXT"].'</a>';

                                            }
                                           */ ?>
                                                                        <li<? if (!empty($next2Element["ELEMENTS"])): ?> class="allow-dropdown"<? endif; ?>>
                                                                            <? if (!empty($next2Element["PICTURE"]["src"])): ?>
                                                                                <a href="<?= $next2Element["LINK"] ?>"
                                                                                   class="menu2Link has-image">
                                                                                    <img src="<?= $next2Element["PICTURE"]["src"] ?>"
                                                                                         alt="<?= $next2Element["TEXT"] ?>"
                                                                                         title="<?= $next2Element["TEXT"] ?>">
                                                                                </a>
                                                                            <? endif; ?>
                                                                            <a href="<?= $next2Element["LINK"] ?>"
                                                                               class="menu2Link<? if ($next2Element["SELECTED"]): ?> selected<? endif ?> <? if (empty($next2Element["ELEMENTS"])): ?> no-bold<? endif ?>">
                                                                                <?= $next2Element["TEXT"] ?>
                                                                                <? if (!empty($next2Element["ELEMENTS"])): ?>
                                                                                    <span class="dropdown btn-simple btn-micro"></span>
                                                                                <? endif; ?>
                                                                            </a>
                                                                            <? if (!empty($next2Element["ELEMENTS"])): ?>
                                                                                <ul>
                                                                                    <? foreach ($next2Element["ELEMENTS"] as $x3 => $next3Element): ?>
                                                                                        <li>
                                                                                            <a href="<?= $next3Element["LINK"] ?>"
                                                                                               class="menu2Link<? if ($next3Element["SELECTED"]): ?> selected<? endif ?>">
                                                                                                <?= $next3Element["TEXT"] ?>
                                                                                            </a>
                                                                                        </li>
                                                                                    <? endforeach; ?>
                                                                                </ul>
                                                                            <? endif; ?>
                                                                            <? if ($isNeedNoIndex) {echo '</noindex>'; } ?>
                                                                        </li>
                                                                        <? /*
                                                    $count++;
                                                    if ($count == 20) {
                                                        echo ' </ul>';
                                                        $count = 0;
                                                    }
                                                    */ ?>
                                                                    <? endforeach; ?>

                                                                <? endif; ?>
                                                            <? endforeach; ?>
                                                        </ul>
                                                    </div>
                                                </div>
                                            <? endif; ?>
                                        </li>
                                    <? endforeach; ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

            <? endif; ?>
                <ul id="mainMenu">
                    <? foreach ($arResult["ITEMS"] as $nextElement): ?>
                        <li class="eChild<? if (!empty($nextElement["ELEMENTS"])): ?> allow-dropdown<? endif; ?>">
                            <a href="<?= $nextElement["LINK"] ?>"
                               class="menuLink<? if ($nextElement["SELECTED"]): ?> selected<? endif ?>">
                                <? if (!empty($nextElement["PICTURE"])): ?>
                                    <img src="<?= $nextElement["PICTURE"]["src"] ?>" alt="<?= $nextElement["TEXT"] ?>"
                                         title="<?= $nextElement["TEXT"] ?>">
                                <? endif; ?>
                                <span class="back"></span>
                                <span class="link-title"><?= $nextElement["TEXT"] ?></span>
                                <span class="dropdown btn-simple btn-micro"></span>
                            </a>
                            <? if (!empty($nextElement["ELEMENTS"])): ?>
                                <div class="drop"<? if (!empty($nextElement["BIG_PICTURE"])): ?> style="background: url(<?= $nextElement["BIG_PICTURE"]["src"] ?>) 50% 50% no-repeat #ffffff;"<? endif; ?>>
                                    <div class="limiter">
                                        <? foreach ($nextElement["ELEMENTS"] as $next2Column): ?>
                                            <? if (!empty($next2Column)): ?>
                                                <ul class="nextColumn">
                                                    <? foreach ($next2Column as $x2 => $next2Element): ?>
                                                        <li<? if (!empty($next2Element["ELEMENTS"])): ?> class="allow-dropdown"<? endif; ?>>
                                                            <? if (!empty($next2Element["PICTURE"]["src"])): ?>
                                                                <a href="<?= $next2Element["LINK"] ?>"
                                                                   class="menu2Link has-image">
                                                                    <img src="<?= $next2Element["PICTURE"]["src"] ?>"
                                                                         alt="<?= $next2Element["TEXT"] ?>"
                                                                         title="<?= $next2Element["TEXT"] ?>">
                                                                </a>
                                                            <? endif; ?>
                                                            <a href="<?= $next2Element["LINK"] ?>"
                                                               class="menu2Link<? if ($next2Element["SELECTED"]): ?> selected<? endif ?>">
                                                                <?= $next2Element["TEXT"] ?>
                                                                <? if (!empty($next2Element["ELEMENTS"])): ?>
                                                                    <span class="dropdown btn-simple btn-micro"></span>
                                                                <? endif; ?>
                                                            </a>
                                                            <? if (!empty($next2Element["ELEMENTS"])): ?>
                                                                <ul>
                                                                    <? foreach ($next2Element["ELEMENTS"] as $x3 => $next3Element): ?>
                                                                        <li>
                                                                            <a href="<?= $next3Element["LINK"] ?>"
                                                                               class="menu2Link<? if ($next3Element["SELECTED"]): ?> selected<? endif ?>">
                                                                                <?= $next3Element["TEXT"] ?>
                                                                            </a>
                                                                        </li>
                                                                    <? endforeach; ?>
                                                                </ul>
                                                            <? endif; ?>
                                                        </li>
                                                    <? endforeach; ?>
                                                </ul>
                                            <? endif; ?>
                                        <? endforeach; ?>
                                    </div>
                                </div>
                            <? endif; ?>
                        </li>
                    <? endforeach; ?>
                </ul>
            </div>
        </div>
    <? endif; ?>
<? endif; ?>