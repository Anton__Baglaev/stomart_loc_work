$(function(){

	//vars jquery
	var $searchQuery = $("#searchQuery");
	var $searchResult = $("#searchResult");
	var $searchOverlap = $("#searchOverlap");

	//vars
	var searchTimeoutID;

	//functions
	var searchKeyPressed = function(event){
		if(event.keyCode !== 27){
			clearTimeout(searchTimeoutID);
			if($searchQuery.val().length > 1){
				searchTimeoutID = setTimeout(function(){
					getSearchProductList($searchQuery.val(), 0, afterSearchGetProducts)
				}, 250);
			}else{
				$searchResult.empty().removeClass("visible");
				$searchOverlap.hide();
			}
		}
	};

	var pageElementCount = 5;
	if (window.innerWidth > 1920) {pageElementCount = 5}

	var getSearchProductList = function(keyword, page, cb){

		var sectionPage = page != "" ? page : 1;
		//sectionPage = 2;
		$searchQuery.addClass("loading");

		var searchProductParamsObject = jQuery.parseJSON(searchProductParams);

		if(typeof searchProductParamsObject["HIDE_NOT_AVAILABLE"] == "undefined"){
			searchProductParamsObject["HIDE_NOT_AVAILABLE"] = "N";
		}

		if(typeof searchProductParamsObject["STEMMING"] == "undefined"){
			searchProductParamsObject["STEMMING"] = "N";
		}

		var getParamsObject = {
			"IBLOCK_TYPE": searchProductParamsObject["IBLOCK_TYPE"],
			"IBLOCK_ID": searchProductParamsObject["IBLOCK_ID"],
			"CONVERT_CASE": searchProductParamsObject["CONVERT_CASE"],
			"LAZY_LOAD_PICTURES": searchProductParamsObject["LAZY_LOAD_PICTURES"],
			"STEMMING": searchProductParamsObject["STEMMING"],
			"ELEMENT_SORT_FIELD": "sort",
			"ELEMENT_SORT_ORDER": "asc",
			"PROPERTY_CODE": searchProductParamsObject["PROPERTY_CODE"],
			"PAGE_ELEMENT_COUNT": pageElementCount,
			"PRICE_CODE": searchProductParamsObject["PRICE_CODE"],
			"PAGER_TEMPLATE": "round",
			"CONVERT_CURRENCY": searchProductParamsObject["CONVERT_CURRENCY"],
			"CURRENCY_ID": searchProductParamsObject["CURRENCY_ID"],
			"HIDE_NOT_AVAILABLE": searchProductParamsObject["HIDE_NOT_AVAILABLE"],
			"FILTER_NAME": "arrFilter",
			"ADD_SECTIONS_CHAIN": "N",
			"SHOW_ALL_WO_SECTION": "Y",
			"HIDE_MEASURES": searchProductParamsObject["HIDE_MEASURES"],
			"PAGEN_1": sectionPage,
			"SEARCH_QUERY": keyword,
			"SEARCH_PROPERTIES": searchProductParamsObject["SEARCH_PROPERTIES"],
			"SITE_ID": SITE_ID
		};

		$('.cp-ls-view-all').removeClass('wide')

		var jqxhr = $.post(searchAjaxPath, getParamsObject, cb);

	};

	var afterSearchGetProducts = function(http){
		$searchQuery.removeClass("loading");
		$searchResult.html(http).addClass("visible");
		$searchOverlap.show();
		checkLazyItems();
	};

	var searchCloseWindow = function(event){
		$searchResult.empty().removeClass("visible");
		clearTimeout(searchTimeoutID);
		$searchOverlap.hide();
		return event.preventDefault();
	};

	var pageChangeProduct = function(event){

		var $this = $(this);
		var page = parseInt($this.data("page"));

		if(page > 0 || page == 0){
			getSearchProductList($searchQuery.val(), page, afterSearchGetProducts);
		}

		return event.preventDefault();

	};

	//bind
	$searchQuery.on("keyup", searchKeyPressed);
	$(document).on("click", "#searchProductsClose", searchCloseWindow);
	$(document).on("click", "#searchResult .bx-pagination a", pageChangeProduct);
	$(document).on("click", ".cm-ls-load-more", function(){
		$searchQuery.addClass("loading");
		var next_page = parseInt($(this).attr('data-page'), 10);
		var all_pages = parseInt($(this).attr('data-total-pages'), 10);
		getSearchProductList($searchQuery.val(), next_page, (v) => {
			var n = $('.search_group_goods').height();

			var goods_in_block = $(v).find('.search_group_goods .wc-item-li');
			$('.search_group_goods .wc-item-li').last().after(goods_in_block);
			$searchQuery.removeClass("loading");
			$searchOverlap.show();

			$('.wc-search-content').animate({ scrollTop: n+100 }, 250);

			checkLazyItems();

			next_page++;

			$(this).attr('data-page', next_page);
			if (all_pages < next_page) {
				$(this).hide();
				$('.cp-ls-view-all').addClass('wide')
			}

		});


	});

	$(document).on("click", ".wc_suggest", function(event){
		$searchQuery.val($(this).text());
		getSearchProductList($searchQuery.val(), 0, afterSearchGetProducts)
		event.preventDefault();
	});

});
