<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);?>
<?
	$arCache = array(
		"SITE_ID" => SITE_ID,
		"CACHE_INDEX" => "DETAIL",
		"ID" => $arResult["VARIABLES"]["ELEMENT_CODE"],
		"SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
		"SECTION_CODE_PATH" => $arResult["VARIABLES"]["SECTION_CODE_PATH"]
	);

	$obCache = new CPHPCache();
	
	if($obCache->InitCache($arParams["CACHE_TIME"], serialize($arCache), "/")){
	   $arResult["EXTRA"] = $obCache->GetVars();
	}

	elseif($obCache->StartDataCache()){
		
		if(!empty($arResult["VARIABLES"]["ELEMENT_CODE"]) && CModule::IncludeModule("iblock")){
			
			$arSelect = Array("ID", "IBLOCK_ID", "NAME");
			$arFilter = Array("ACTIVE" => "Y");
			$arFilter["=CODE"] = $arResult["VARIABLES"]["ELEMENT_CODE"];
			$arFilter["IBLOCK_ID"] = $arParams["IBLOCK_ID"];

			//show deactivated products
			if(!empty($arParams["SHOW_DEACTIVATED"]) && $arParams["SHOW_DEACTIVATED"] == "Y"){
				$arFilter["ACTIVE"] = "";
			}

			//check for 404 error
			if(!empty($arResult["VARIABLES"]["SECTION_CODE_PATH"])){
				$arSectionPath = explode("/", $arResult["VARIABLES"]["SECTION_CODE_PATH"]);
				$arFilter["=SECTION_CODE"] = $arSectionPath;
			}elseif(!empty($arResult["VARIABLES"]["SECTION_CODE"])){
				$arFilter["=SECTION_CODE"] = $arResult["VARIABLES"]["SECTION_CODE"];				
			}

			$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
			if($ob = $res->GetNextElement()){
				//save
				$arResult["EXTRA"] = $ob->getFields();
			}

			else{
				//abort cache
				$obCache->AbortDataCache();
			}

		}

		$obCache->EndDataCache($arResult["EXTRA"]);

	}

?>
<?$APPLICATION->IncludeComponent("dresscode:catalog.item", "serviceDetailCustom", Array(
	"PRODUCT_ID" => (!empty($arResult["VARIABLES"]["ELEMENT_ID"])?$arResult["VARIABLES"]["ELEMENT_ID"]:(!empty($arResult["EXTRA"]["ID"])?$arResult["EXTRA"]["ID"]:"-")),	// ID Товара
		"ADD_ELEMENT_CHAIN" => (isset($arParams["ADD_ELEMENT_CHAIN"])?$arParams["ADD_ELEMENT_CHAIN"]:""),	// Добавить элемент в цепочку навигации
		"OFFERS_TABLE_DISPLAY_PICTURE_COLUMN" => $arParams["OFFERS_TABLE_DISPLAY_PICTURE_COLUMN"],
		"SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],	// URL, ведущий на страницу с содержимым раздела
		"DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["element"],
		"OFFERS_TABLE_PAGER_COUNT" => $arParams["OFFERS_TABLE_PAGER_COUNT"],
		"SECTION_CODE_PATH" => $arResult["VARIABLES"]["SECTION_CODE_PATH"],
		"PRODUCT_DISPLAY_PROPERTIES" => $arParams["PRODUCT_PROPERTIES"],	// Свойства товаров для отображения (DISPLAY_PROPERTIES)
		"SET_CANONICAL_URL" => $arParams["DETAIL_SET_CANONICAL_URL"],
		"DISPLAY_OFFERS_TABLE" => $arParams["DISPLAY_OFFERS_TABLE"],
		"META_DESCRIPTION" => $arParams["DETAIL_META_DESCRIPTION"],
		"SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],	// Символьный код раздела
		"HIDE_AVAILABLE_TAB" => $arParams["HIDE_AVAILABLE_TAB"],
		"HIDE_NOT_AVAILABLE" => $arParams["HIDE_NOT_AVAILABLE"],	// Не отображать товары, которых нет на складах
		"REVIEW_IBLOCK_TYPE" => $arParams["REVIEW_IBLOCK_TYPE"],
		"SET_LAST_MODIFIED" => $arParams["SET_LAST_MODIFIED"],	// Устанавливать в заголовках ответа время модификации страницы
		"META_KEYWORDS" => $arParams["DETAIL_META_KEYWORDS"],
		"BROWSER_TITLE" => $arParams["DETAIL_BROWSER_TITLE"],
		"REVIEW_IBLOCK_ID" => $arParams["REVIEW_IBLOCK_ID"],
		"SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],	// ID раздела
		"SHOW_DEACTIVATED" => $arParams["SHOW_DEACTIVATED"],
		"CONVERT_CURRENCY" => $arParams["CONVERT_CURRENCY"],	// Показывать цены в одной валюте
		"DISPLAY_CHEAPER" => $arParams["DISPLAY_CHEAPER"],
		"CHEAPER_FORM_ID" => $arParams["CHEAPER_FORM_ID"],
		"PRODUCT_PRICE_CODE" => $arParams["PRICE_CODE"],	// Тип цены
		"PICTURE_HEIGHT" => $arParams["PICTURE_HEIGHT"],	// Высота изображений
		"SET_STATUS_404" => $arParams["SET_STATUS_404"],
		"PICTURE_WIDTH" => $arParams["PICTURE_WIDTH"],	// Ширина изображений
		"HIDE_MEASURES" => $arParams["HIDE_MEASURES"],	// Не отображать единицы измерения у товаров
		"CURRENCY_ID" => $arParams["CURRENCY_ID"],
		"MESSAGE_404" => $arParams["MESSAGE_404"],
		"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],	// Тип Инфоблока
		"CACHE_TIME" => $arParams["CACHE_TIME"],	// Время кеширования (сек.)
		"CACHE_TYPE" => $arParams["CACHE_TYPE"],	// Тип кеширования
		"USE_REVIEW" => $arParams["USE_REVIEW"],
		"SET_TITLE" => $arParams["SET_TITLE"],	// Устанавливать заголовок страницы
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],	// Инфоблок
		"SHOW_404" => $arParams["SHOW_404"],
		"FILE_404" => $arParams["FILE_404"],
		"SET_VIEWED_IN_COMPONENT" => "N",	// Включить сохранение информации о просмотре товара
		"DISPLAY_MORE_PICTURES" => "Y",
		"SET_META_DESCRIPTION" => "Y",	// Установить meta description
		"DISPLAY_LAST_SECTION" => "Y",
		"DISPLAY_FILES_VIDEO" => "Y",
		"ADD_SECTIONS_CHAIN" => "Y",	// Добавить разделы в цепочку навигации
		"SET_META_KEYWORDS" => "Y",	// Установить meta keywords
		"SET_BROWSER_TITLE" => "Y",	// Установить заголовок окна браузера
		"GET_MORE_PICTURES" => "Y",	// Создавать массив IMAGES (необходимо для карточки товара)
		"DISPLAY_RELATED" => "Y",
		"DISPLAY_SIMILAR" => "Y",
		"DETAIL_ELEMENT" => "Y",
		"DISPLAY_BRAND" => "Y"
	),
	false
);?>