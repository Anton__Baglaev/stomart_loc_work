<?

if(!$arResult['SECTIONS_COUNT']) {

    // запрос инфоблока с кодом news
    $sections = \Bitrix\Iblock\SectionTable::getList(array(
        'order' => array('SORT' => 'ASC'), // параметры сортировки
        'select' => ['ID', 'NAME', 'IBLOCK_ID', 'IBLOCK_SECTION_ID', 'SECTION_PAGE_URL' => 'IBLOCK.SECTION_PAGE_URL'],
        'filter' => [
            'IBLOCK_ID' => 20,
            'ACTIVE' => 'Y',
            '!ID' => $arResult['SECTION']['ID'],
            //  '<SORT' => $arResult['SECTION']['SORT'],
            'IBLOCK_SECTION_ID' => $arResult['SECTION']['IBLOCK_SECTION_ID'],

            // '>DEPTH_LEVEL' => $arParentSection['DEPTH_LEVEL']);
        ],
        //  'limit' => 1,
    ))->fetchAll();

    foreach ($sections as $section) {

        $arResult["SECTIONS"][$section['ID']] = [
            'ID' => $section['ID'],
            'NAME' => $section['NAME'],
            'SECTION_PAGE_URL' => \CIBlock::ReplaceDetailUrl($section['SECTION_PAGE_URL'], $section, true, 'S'),
        ];
    }

    /*
        // запрос инфоблока с кодом news
        $arResult["SECTIONS"][] = \Bitrix\Iblock\SectionTable::getList(array(
            'order' => array('SORT' => 'asc'), // параметры сортировки
            'filter' => [
                'IBLOCK_ID' => 20,
                'ACTIVE'=>'Y',
                '!ID' => $arResult['SECTION']['ID'],
                '>SORT' => $arResult['SECTION']['SORT'],
                'IBLOCK_SECTION_ID' => $arResult['SECTION']['IBLOCK_SECTION_ID'],

               // '>DEPTH_LEVEL' => $arParentSection['DEPTH_LEVEL']);
            ],
            'limit' => 1,
        ))->fetch();
    */

/*
    echo "<pre>";
    var_dump($arResult);
*/
}
