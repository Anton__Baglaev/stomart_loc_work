$(function(){

	//vars
	var openSmartFilterFlag = false;

	//functions
	var openSmartFilter = function(event){

		// smartFilter block adaptive toggle
		if(!openSmartFilterFlag){
            var tagSectionHeight = !$('#tagSection').length ? $('h1').offset().top + 15 : $('#tagSection').offset().top - 45 ;
			$("#smartFilter").addClass("opened").css('marginTop', ($('.oSmartFilter').offset().top - tagSectionHeight ));
			openSmartFilterFlag = true;
		}

		else{
			$("#smartFilter").removeClass("opened").removeAttr("style");
			openSmartFilterFlag = false;
		}

		return event.preventDefault();
	};

	var closeSmartFilter = function(event){
		if(openSmartFilterFlag){
			$("#smartFilter").removeClass("opened");
			openSmartFilterFlag = false;
		}
	};

	function appendToUrl(url, param){

		//check args
		if(typeof url != "undefined" && url != ""){
			//push
			url = url + url.indexOf("?") != "-1" ? "&" : "?" + param;
		}

		return url;

	}

	//binds
	$(document).on("click", ".oSmartFilter", openSmartFilter);
    $(document).on("click", "#smartFilter, .oSmartFilter, .rangeSlider", function(event){
    	return event.stopImmediatePropagation();
    });

	$(document).on("click", closeSmartFilter);

});