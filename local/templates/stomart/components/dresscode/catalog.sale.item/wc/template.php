<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?$this->setFrameMode(true);?>
<?if(!empty($arResult["ITEMS"])):?>
	<?$uniqID = CAjax::GetComponentID($this->__component->__name, $this->__component->__template->__name, false);?>
	<?foreach($arResult["ITEMS"] as $ii => $arNextElement):?>
		<?
			if(!empty($arNextElement["EDIT_LINK"])){
				$this->AddEditAction($arNextElement["ID"], $arNextElement["EDIT_LINK"], CIBlock::GetArrayByID($arNextElement["IBLOCK_ID"], "ELEMENT_EDIT"));
				$this->AddDeleteAction($arNextElement["ID"], $arNextElement["DELETE_LINK"], CIBlock::GetArrayByID($arNextElement["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage("CT_BNL_ELEMENT_DELETE_CONFIRM")));
			}
		?>
		<div class="bindAction" id="<?=$this->GetEditAreaId($arNextElement["ID"]);?>">
			<div class="tb">
				<div class="tc bindActionImage"><a href="<?=$arNextElement["DETAIL_PAGE_URL"]?>"><span class="image" title="<?=$arNextElement["NAME"]?>"></span></a></div>
				<div class="tc"><?=GetMessage("BIND_ACTION_LABEL")?><br><a href="<?=$arNextElement["DETAIL_PAGE_URL"]?>" class="theme-color"><?=$arNextElement["NAME"]?></a></div>
			</div>
		</div>
	<?endforeach;?>
<?else:?>
	<?php
		$BRANDS = [
			1688, 13029
		];

		if (!empty($arParams["BRAND_ID"])){
			if (in_array($arParams["BRAND_ID"], $BRANDS)){
				//echo "<span class='theme-color'>".GetMessage("PR_P_LABEL")."</span>";
				echo '<div class="bindAction" i>
					<div class="tb">
						<div class="tc bindActionImage post"><span class="image"></span></div>
						<div class="tc">Прямые поставки<br><span class="theme-color">ОФИЦИАЛЬНЫЙ ДИСТРИБЬЮТОР</span></div>
					</div>
				</div>';
			}
			else {
				// echo '<a href="/loalty/" target="_blank" class="theme-color upp"><img src="/local/templates/stomart/images/cheaper.png" class="icon"><span>Товар участвует: </span>'.GetMessage("LOYALTI_LABEL").'</a>';

				echo '<div class="bindAction" i>
					<div class="tb">
						<div class="tc bindActionImage loyalty"><a href="/loalty/"><span class="image"></span></a></div>
						<div class="tc">Товар участвует в:<br><a href="/loalty/" class="theme-color">ПРОГРАММА ЛОЯЛЬНОСТИ</a></div>
					</div>
				</div>';
			}
		}
		else {
				// echo '<a href="/loalty/" target="_blank" class="theme-color upp"><img src="/local/templates/stomart/images/cheaper.png" class="icon"><span>Товар участвует: </span>'.GetMessage("LOYALTI_LABEL").'</a>';
				echo '<a href="#" class="brandImage"></a>';
				echo '<div class="bindAction" i>
					<div class="tb">
						<div class="tc bindActionImage loyalty"><a href="/loalty/"><span class="image"></span></a></div>
						<div class="tc">Товар участвует в:<br><a href="/loalty/" class="theme-color">ПРОГРАММА ЛОЯЛЬНОСТИ</a></div>
					</div>
				</div>';
			}
	?>
<?endif;?>