<?php
\Bitrix\Main\Loader::IncludeModule("sale");

foreach ($arResult["ITEMS"] as &$basketItem) {
	$product_article = '';
	$id_to_search = $basketItem["PRODUCT_ID"];

	$mxResult = CCatalogSku::GetProductInfo($basketItem["PRODUCT_ID"]);
	if (is_array($mxResult)) {
		$article_find = CIBlockElement::GetProperty(20, $mxResult['ID'], array(), array("CODE" => "CML2_ARTICLE"));
		if ($article_value = $article_find->Fetch()) {
			$product_article = $article_value["VALUE"];
		}
	}
	if (!$product_article) {
		$article_find = CIBlockElement::GetProperty(20, $basketItem["PRODUCT_ID"], array(), array("CODE" => "CML2_ARTICLE"));
		if ($article_value = $article_find->Fetch()) {
			$product_article = $article_value["VALUE"];
		}
	}

	$basketItem["PROPERTIES"]["CML2_ARTICLE"]["VALUE"] = $product_article;
}

