<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?$this->setFrameMode(false);?>
<?php $WCCFS = WCCFFacetStorage::get('wccf');?>
<?if(!empty($arResult)):
    $compareCssClass = !empty($_SESSION["COMPARE_LIST"]["ITEMS"][(int)$arResult["~ID"]])?"added":"";
    $wishlistCssClass = !empty($_SESSION["WISHLIST_LIST"]["ITEMS"][(int)$arResult["~ID"]])?"added":"";?>
	<?$uniqID = CAjax::GetComponentID($this->__component->__name, $this->__component->__template->__name, false);?>
	<?
		if(!empty($arResult["PARENT_PRODUCT"]["EDIT_LINK"])){
			$this->AddEditAction($arResult["ID"], $arResult["PARENT_PRODUCT"]["EDIT_LINK"], CIBlock::GetArrayByID($arResult["PARENT_PRODUCT"]["IBLOCK_ID"], "ELEMENT_EDIT"));
			$this->AddDeleteAction($arResult["ID"], $arResult["PARENT_PRODUCT"]["DELETE_LINK"], CIBlock::GetArrayByID($arResult["PARENT_PRODUCT"]["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage("CT_BNL_ELEMENT_DELETE_CONFIRM")));
		}
		if(!empty($arResult["EDIT_LINK"])){
			$this->AddEditAction($arResult["ID"], $arResult["EDIT_LINK"], CIBlock::GetArrayByID($arResult["IBLOCK_ID"], "ELEMENT_EDIT"));
			$this->AddDeleteAction($arResult["ID"], $arResult["DELETE_LINK"], CIBlock::GetArrayByID($arResult["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage("CT_BNL_ELEMENT_DELETE_CONFIRM")));
		}
	?>
<div class="wc-item-li">
    <a class="cm-ls-product" href="<?=$arResult["DETAIL_PAGE_URL"]?>">
        <div class="wc-item-container">
            <div class="wc-image-container">
                <?if($arParams["LAZY_LOAD_PICTURES"] == "Y"):?>
                    <img  width="70" src="<?=SITE_TEMPLATE_PATH?>/images/lazy.svg" class="lazy" data-lazy="<?=$arResult["PICTURE"]["src"]?>" alt="<?if(!empty($arResult["IPROPERTY_VALUES"]["ELEMENT_PREVIEW_PICTURE_FILE_ALT"])):?><?=$arResult["IPROPERTY_VALUES"]["ELEMENT_PREVIEW_PICTURE_FILE_ALT"]?><?else:?><?=$arResult["NAME"]?><?endif;?>" title="<?if(!empty($arResult["IPROPERTY_VALUES"]["ELEMENT_PREVIEW_PICTURE_FILE_TITLE"])):?><?=$arResult["IPROPERTY_VALUES"]["ELEMENT_PREVIEW_PICTURE_FILE_TITLE"]?><?else:?><?=$arResult["NAME"]?><?endif;?>">
                <?else:?>
                    <img  width="70" src="<?=$arResult["PICTURE"]["src"]?>" alt="<?if(!empty($arResult["IPROPERTY_VALUES"]["ELEMENT_PREVIEW_PICTURE_FILE_ALT"])):?><?=$arResult["IPROPERTY_VALUES"]["ELEMENT_PREVIEW_PICTURE_FILE_ALT"]?><?else:?><?=$arResult["NAME"]?><?endif;?>" title="<?if(!empty($arResult["IPROPERTY_VALUES"]["ELEMENT_PREVIEW_PICTURE_FILE_TITLE"])):?><?=$arResult["IPROPERTY_VALUES"]["ELEMENT_PREVIEW_PICTURE_FILE_TITLE"]?><?else:?><?=$arResult["NAME"]?><?endif;?>">
                <?endif;?>
            </div>
            <div class="wc-info-container">
                <div class="wc-product-name-wrap">
                    <span class="wc-product-name product-title">
                        <?=$arResult["NAME"]?>
                    </span>
                </div>
                <div class="wc-product-price-wrap">
                    <?if(!empty($arResult["PRICE"])):?>
                        <?//price container?>
                        <?if($arResult["EXTRA_SETTINGS"]["COUNT_PRICES"] > 1):?>
                            <a class="price getPricesWindow" data-id="<?=$arResult["ID"]?>">
                                <span class="priceIcon"></span><?=CCurrencyLang::CurrencyFormat($arResult["PRICE"]["DISCOUNT_PRICE"], $arResult["EXTRA_SETTINGS"]["CURRENCY"], true)?>
                                <?if($arParams["HIDE_MEASURES"] != "Y" && !empty($arResult["EXTRA_SETTINGS"]["MEASURES"][$arResult["CATALOG_MEASURE"]]["SYMBOL_RUS"])):?>
                                    <span class="measure"> / <?=$arResult["EXTRA_SETTINGS"]["MEASURES"][$arResult["CATALOG_MEASURE"]]["SYMBOL_RUS"]?></span>
                                <?endif;?>
                                <s class="discount">
                                    <?if(!empty($arResult["PRICE"]["DISCOUNT"])):?>
                                        <?=CCurrencyLang::CurrencyFormat($arResult["PRICE"]["RESULT_PRICE"]["BASE_PRICE"], $arResult["EXTRA_SETTINGS"]["CURRENCY"], true)?>
                                    <?endif;?>
                                </s>
                            </a>
                        <?else:?>
                            <a class="price <?if($arResult["PROPERTIES"]["DISCOUNTED"]['VALUE'] == "Y"):?>discounted-price<?endif;?>"><?=CCurrencyLang::CurrencyFormat($arResult["PRICE"]["DISCOUNT_PRICE"], $arResult["EXTRA_SETTINGS"]["CURRENCY"], true)?>
                                <?if($arParams["HIDE_MEASURES"] != "Y" && !empty($arResult["EXTRA_SETTINGS"]["MEASURES"][$arResult["CATALOG_MEASURE"]]["SYMBOL_RUS"])):?>
                                    <span class="measure"> / <?=$arResult["EXTRA_SETTINGS"]["MEASURES"][$arResult["CATALOG_MEASURE"]]["SYMBOL_RUS"]?></span>
                                <?endif;?>
                                <s class="discount">
                                    <?if(!empty($arResult["PRICE"]["DISCOUNT"])):?>
                                        <?=CCurrencyLang::CurrencyFormat($arResult["PRICE"]["RESULT_PRICE"]["BASE_PRICE"], $arResult["EXTRA_SETTINGS"]["CURRENCY"], true)?>
                                    <?endif;?>
                                </s>
                            </a>
                        <?endif;?>
                    <?else:?>
                        <a class="price"><?=GetMessage("REQUEST_PRICE_LABEL")?><s class="discount"></s></a>
                    <?endif;?>
                </div>
                <div class="wc-product-code-wrap">
                    <span class="wc-product-code">
                        <?php
                        if (!empty($arResult["PROPERTIES"]["CML2_BAR_CODE"]["VALUE"])) {
                                 echo '<span class="list_bc">Код товара: '.$arResult["PROPERTIES"]["CML2_BAR_CODE"]["VALUE"].'</span>';
                        }
                        ?>
                    </span>
                </div>
            </div>
            <div class="cp-wc-search-buttons">

                <a href="#" class="addCompare label topCompare <?=$compareCssClass?>" data-id="<?=$arResult["ID"]?>" data-no-label="Y">
                    <span class="icon"></span>
                </a>

                <a href="#" class="addWishlist label topWishlist <?=$wishlistCssClass?>" data-id="<?=$arResult["~ID"]?>" data-no-label="Y">
                    <span class="icon"></span>
                </a>

                <?if(!empty($arResult["PRICE"])):?>
                    <?if($arResult["CATALOG_AVAILABLE"] != "Y"):?>
                        <?//addCart button?>
                        <?if($arResult["CATALOG_SUBSCRIBE"] == "Y"):?>
                            <a href="#" class="wc addCart subscribe cart" data-id="<?=$arResult["ID"]?>"><span class="countLink"></span></a>
                        <?else:?>
                            <a href="#" class="wc addCart disabled cart" data-id="<?=$arResult["ID"]?>"><span class="countLink"></span></a>
                        <?endif;?>
                    <?else:?>
                        <a href="#" class="wc addCart cart" data-id="<?=$arResult["ID"]?>"><span class="countLink"></span></a>
                    <?endif;?>
                <?else:?>
                    <a href="#" class="wc addCart disabled requestPrice cart" data-id="<?=$arResult["ID"]?>"><span class="countLink"></span></a>
                <?endif;?>

            </div>
        </div>
    </a>
</div>
<?endif;?>
