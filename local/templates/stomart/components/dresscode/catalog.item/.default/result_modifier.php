<?

	if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true){
		die();
	}


    if (!empty($arResult["PROPERTIES"]["ATT_BRAND"]["VALUE"]))
    {
        $db_el = CIBlockElement::GetList(
            array(),
            array(
                'ID' => $arResult["PROPERTIES"]["ATT_BRAND"]["VALUE"]
            )
        );
        if ($ar_el = $db_el->GetNext())
        {
            $arResult["PROPERTIES"]["ATT_BRAND_LINK"] = $ar_el['DETAIL_PAGE_URL'];
        }
    }

?>