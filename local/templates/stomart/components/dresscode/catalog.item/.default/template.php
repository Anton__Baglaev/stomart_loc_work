<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?$this->setFrameMode(false);?>
<?php $WCCFS = WCCFFacetStorage::get('wccf');?>
<?if(!empty($arResult)):
    $compareCssClass = !empty($_SESSION["COMPARE_LIST"]["ITEMS"][(int)$arResult["~ID"]])?"added":"";
    $wishlistCssClass = !empty($_SESSION["WISHLIST_LIST"]["ITEMS"][(int)$arResult["~ID"]])?"added":"";
    ?>
	<?$uniqID = CAjax::GetComponentID($this->__component->__name, $this->__component->__template->__name, false);?>
	<?
		if(!empty($arResult["PARENT_PRODUCT"]["EDIT_LINK"])){
			$this->AddEditAction($arResult["ID"], $arResult["PARENT_PRODUCT"]["EDIT_LINK"], CIBlock::GetArrayByID($arResult["PARENT_PRODUCT"]["IBLOCK_ID"], "ELEMENT_EDIT"));
			$this->AddDeleteAction($arResult["ID"], $arResult["PARENT_PRODUCT"]["DELETE_LINK"], CIBlock::GetArrayByID($arResult["PARENT_PRODUCT"]["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage("CT_BNL_ELEMENT_DELETE_CONFIRM")));
		}
		if(!empty($arResult["EDIT_LINK"])){
			$this->AddEditAction($arResult["ID"], $arResult["EDIT_LINK"], CIBlock::GetArrayByID($arResult["IBLOCK_ID"], "ELEMENT_EDIT"));
			$this->AddDeleteAction($arResult["ID"], $arResult["DELETE_LINK"], CIBlock::GetArrayByID($arResult["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage("CT_BNL_ELEMENT_DELETE_CONFIRM")));
		}
	?>
	<div class="item product sku" id="<?=$this->GetEditAreaId($arResult["ID"]);?>" data-product-iblock-id="<?=$arParams["IBLOCK_ID"]?>" data-from-cache="<?=$arResult["FROM_CACHE"]?>" data-convert-currency="<?=$arParams["CONVERT_CURRENCY"]?>" data-currency-id="<?=$arParams["CURRENCY_ID"]?>" data-product-id="<?=!empty($arResult["~ID"]) ? $arResult["~ID"] : $arResult["ID"]?>" data-iblock-id="<?=$arResult["SKU_INFO"]["IBLOCK_ID"]?>" data-prop-id="<?=$arResult["SKU_INFO"]["SKU_PROPERTY_ID"]?>" data-product-width="<?=$arParams["PICTURE_WIDTH"]?>" data-product-height="<?=$arParams["PICTURE_HEIGHT"]?>" data-hide-measure="<?=$arParams["HIDE_MEASURES"]?>" data-currency="<?=$arResult["EXTRA_SETTINGS"]["CURRENCY"]?>" data-hide-not-available="<?=$arParams["HIDE_NOT_AVAILABLE"]?>" data-price-code="<?=implode("||", $arParams["PRODUCT_PRICE_CODE"])?>">
		<div class="tabloid nowp">
			<a href="#" class="removeFromWishlist" data-id="<?=$arResult["~ID"]?>"></a>
			<?if(!empty($arResult["PROPERTIES"]["OFFERS"]["VALUE"])):?>
				<div class="markerContainer">
					<?foreach ($arResult["PROPERTIES"]["OFFERS"]["VALUE"] as $ifv => $marker):?>
					    <div class="marker" style="background-color: <?=strstr($arResult["PROPERTIES"]["OFFERS"]["VALUE_XML_ID"][$ifv], "#") ? $arResult["PROPERTIES"]["OFFERS"]["VALUE_XML_ID"][$ifv] : "#424242"?>"><?=$marker?></div>
					<?endforeach;?>
				</div>
			<?endif;?>
			<div class="rating">
				<i class="m" style="width:<?=(intval($arResult["PROPERTIES"]["RATING"]["VALUE"]) * 100 / 5)?>%"></i>
				<i class="h"></i>
			</div>
			<?if(!empty($arResult["EXTRA_SETTINGS"]["SHOW_TIMER"])):?>
				<div class="specialTime productSpecialTime" id="timer_<?=$arResult["EXTRA_SETTINGS"]["TIMER_UNIQ_ID"];?>_<?=$uniqID?>">
					<div class="specialTimeItem">
						<div class="specialTimeItemValue timerDayValue">0</div>
						<div class="specialTimeItemlabel"><?=GetMessage("PRODUCT_TIMER_DAY_LABEL")?></div>
					</div>
					<div class="specialTimeItem">
						<div class="specialTimeItemValue timerHourValue">0</div>
						<div class="specialTimeItemlabel"><?=GetMessage("PRODUCT_TIMER_HOUR_LABEL")?></div>
					</div>
					<div class="specialTimeItem">
						<div class="specialTimeItemValue timerMinuteValue">0</div>
						<div class="specialTimeItemlabel"><?=GetMessage("PRODUCT_TIMER_MINUTE_LABEL")?></div>
					</div>
					<div class="specialTimeItem">
						<div class="specialTimeItemValue timerSecondValue">0</div>
						<div class="specialTimeItemlabel"><?=GetMessage("PRODUCT_TIMER_SECOND_LABEL")?></div>
					</div>
				</div>
			<?endif;?>
			<?if(!empty($arResult["PROPERTIES"]["TIMER_LOOP"]["VALUE"])):?>
				<script type="text/javascript">
					$(document).ready(function(){
						$("#timer_<?=$arResult["EXTRA_SETTINGS"]["TIMER_UNIQ_ID"];?>_<?=$uniqID?>").dwTimer({
							timerLoop: "<?=$arResult["PROPERTIES"]["TIMER_LOOP"]["VALUE"]?>",
							<?if(empty($arResult["PROPERTIES"]["TIMER_START_DATE"]["VALUE"])):?>
								startDate: "<?=MakeTimeStamp($arResult["DATE_CREATE"], "DD.MM.YYYY HH:MI:SS")?>"
							<?else:?>
								startDate: "<?=MakeTimeStamp($arResult["PROPERTIES"]["TIMER_START_DATE"]["VALUE"], "DD.MM.YYYY HH:MI:SS")?>"
							<?endif;?>
						});
					});
				</script>
			<?elseif(!empty($arResult["EXTRA_SETTINGS"]["SHOW_TIMER"]) && !empty($arResult["PROPERTIES"]["TIMER_DATE"]["VALUE"])):?>
				<script type="text/javascript">
					$(document).ready(function(){
						$("#timer_<?=$arResult["EXTRA_SETTINGS"]["TIMER_UNIQ_ID"];?>_<?=$uniqID?>").dwTimer({
							endDate: "<?=MakeTimeStamp($arResult["PROPERTIES"]["TIMER_DATE"]["VALUE"], "DD.MM.YYYY HH:MI:SS")?>"
						});
					});
				</script>
			<?endif;?>
		    <div class="productTable">
		    	<div class="productColImage">
					<a href="<?=$arResult["DETAIL_PAGE_URL"]?>" class="picture">
						<?if($arParams["LAZY_LOAD_PICTURES"] == "Y"):?>
							<img src="<?=SITE_TEMPLATE_PATH?>/images/lazy.svg" class="lazy" data-lazy="<?=$arResult["PICTURE"]["src"]?>" alt="<?if(!empty($arResult["IPROPERTY_VALUES"]["ELEMENT_PREVIEW_PICTURE_FILE_ALT"])):?><?=$arResult["IPROPERTY_VALUES"]["ELEMENT_PREVIEW_PICTURE_FILE_ALT"]?><?else:?><?=$arResult["NAME"]?><?endif;?>" title="<?if(!empty($arResult["IPROPERTY_VALUES"]["ELEMENT_PREVIEW_PICTURE_FILE_TITLE"])):?><?=$arResult["IPROPERTY_VALUES"]["ELEMENT_PREVIEW_PICTURE_FILE_TITLE"]?><?else:?><?=$arResult["NAME"]?><?endif;?>">
						<?else:?>
							<img src="<?=$arResult["PICTURE"]["src"]?>" alt="<?if(!empty($arResult["IPROPERTY_VALUES"]["ELEMENT_PREVIEW_PICTURE_FILE_ALT"])):?><?=$arResult["IPROPERTY_VALUES"]["ELEMENT_PREVIEW_PICTURE_FILE_ALT"]?><?else:?><?=$arResult["NAME"]?><?endif;?>" title="<?if(!empty($arResult["IPROPERTY_VALUES"]["ELEMENT_PREVIEW_PICTURE_FILE_TITLE"])):?><?=$arResult["IPROPERTY_VALUES"]["ELEMENT_PREVIEW_PICTURE_FILE_TITLE"]?><?else:?><?=$arResult["NAME"]?><?endif;?>">
						<?endif;?>
                        <?if($arResult["PROPERTIES"]["DISCOUNTED"]['VALUE'] != "Y"):?>
						<span class="getFastView" data-id="<?=$arResult["ID"]?>"><?=GetMessage("FAST_VIEW_PRODUCT_LABEL")?></span>
                        <?endif;?>
					</a>
		    	</div>
		    	<div class="productColText">
					<a href="<?=$arResult["DETAIL_PAGE_URL"]?>" class="name"><span class="middle"><?=$arResult["NAME"]?></span></a>
                    <?if ($arResult["PROPERTIES"]["NOT_AVAILABLE_RUSSIA"]["VALUE"] !== "Y"):?>
                        <?if(!empty($arResult["PRICE"])):?>
                            <?//price container?>
                            <?if($arResult["EXTRA_SETTINGS"]["COUNT_PRICES"] > 1):?>
                                <a class="price getPricesWindow" data-id="<?=$arResult["ID"]?>">
                                    <span class="priceIcon"></span><?=CCurrencyLang::CurrencyFormat($arResult["PRICE"]["DISCOUNT_PRICE"], $arResult["EXTRA_SETTINGS"]["CURRENCY"], true)?>
                                    <?if($arParams["HIDE_MEASURES"] != "Y" && !empty($arResult["EXTRA_SETTINGS"]["MEASURES"][$arResult["CATALOG_MEASURE"]]["SYMBOL_RUS"])):?>
                                        <span class="measure"> / <?=$arResult["EXTRA_SETTINGS"]["MEASURES"][$arResult["CATALOG_MEASURE"]]["SYMBOL_RUS"]?></span>
                                    <?endif;?>
                                    <s class="discount">
                                        <?if(!empty($arResult["PRICE"]["DISCOUNT"])):?>
                                            <?=CCurrencyLang::CurrencyFormat($arResult["PRICE"]["RESULT_PRICE"]["BASE_PRICE"], $arResult["EXTRA_SETTINGS"]["CURRENCY"], true)?>
                                        <?endif;?>
                                    </s>
                                </a>
                            <?else:?>
                                <a class="price <?if($arResult["PROPERTIES"]["DISCOUNTED"]['VALUE'] == "Y"):?>discounted-price<?endif;?>"><?=CCurrencyLang::CurrencyFormat($arResult["PRICE"]["DISCOUNT_PRICE"], $arResult["EXTRA_SETTINGS"]["CURRENCY"], true)?>
                                    <?if($arParams["HIDE_MEASURES"] != "Y" && !empty($arResult["EXTRA_SETTINGS"]["MEASURES"][$arResult["CATALOG_MEASURE"]]["SYMBOL_RUS"])):?>
                                        <span class="measure"> / <?=$arResult["EXTRA_SETTINGS"]["MEASURES"][$arResult["CATALOG_MEASURE"]]["SYMBOL_RUS"]?></span>
                                    <?endif;?>
                                    <s class="discount">
                                        <?if(!empty($arResult["PRICE"]["DISCOUNT"])):?>
                                            <?=CCurrencyLang::CurrencyFormat($arResult["PRICE"]["RESULT_PRICE"]["BASE_PRICE"], $arResult["EXTRA_SETTINGS"]["CURRENCY"], true)?>
                                        <?endif;?>
                                    </s>
                                </a>
                            <?endif;?>
                        <?else:?>
                            <a class="price"><?=GetMessage("REQUEST_PRICE_LABEL")?><s class="discount"></s></a>
                        <?endif;?>
                    <?else: ?>
                        <a class="price" href="#">&nbsp;<s class="discount"></s></a>
                    <?endif;?>
                    <div class="middle property">
                        <?php
                            if (!empty($WCCFS[$arResult["PROPERTIES"]["BREND_1"]["ID"]])
                                && !empty($WCCFS[$arResult["PROPERTIES"]["BREND_1"]["ID"]][$arResult["PROPERTIES"]["BREND_1"]["VALUE"][0]])) {
                                echo '<a title="Показать только '.$arResult["PROPERTIES"]["BREND_1"]["VALUE"][0].'" href="#" class="wc_filter_link" data-filter="'.$WCCFS[$arResult["PROPERTIES"]["BREND_1"]["ID"]][$arResult["PROPERTIES"]["BREND_1"]["VALUE"][0]].'">'.$arResult["PROPERTIES"]["BREND_1"]["VALUE"][0].'</a>, ';
                            } elseif ($arResult["PROPERTIES"]["BREND_1"]["VALUE"][0]){
                                echo '<a class="wc_filter_link" >'. $arResult["PROPERTIES"]["BREND_1"]["VALUE"][0] . '<a>, ';
                            }


                            if (!empty($WCCFS[$arResult["PROPERTIES"]["STRANA_PROIZVODITEL_1"]["ID"]])
                                && !empty($WCCFS[$arResult["PROPERTIES"]["STRANA_PROIZVODITEL_1"]["ID"]][$arResult["PROPERTIES"]["STRANA_PROIZVODITEL_1"]["VALUE"]])) {
                                echo '<a title="Показать только '.$arResult["PROPERTIES"]["STRANA_PROIZVODITEL_1"]["VALUE"].'" href="#" class="wc_filter_link" data-filter="'.$WCCFS[$arResult["PROPERTIES"]["STRANA_PROIZVODITEL_1"]["ID"]][$arResult["PROPERTIES"]["STRANA_PROIZVODITEL_1"]["VALUE"]].'">'.$arResult["PROPERTIES"]["STRANA_PROIZVODITEL_1"]["VALUE"].'</a>';
                            } elseif ($arResult["PROPERTIES"]["STRANA_PROIZVODITEL_1"]["VALUE"]){
                                echo '<a class="wc_filter_link" >'. $arResult["PROPERTIES"]["STRANA_PROIZVODITEL_1"]["VALUE"] . '<a>';
                            }

                            if (!empty($arResult["PROPERTIES"]["CML2_BAR_CODE"]["VALUE"])) {
                                 echo '<br><span class="list_bc">Код товара - '.$arResult["PROPERTIES"]["CML2_BAR_CODE"]["VALUE"].'</span>';
                             }

                        ?>
                        <?/* if ($arResult["PROPERTIES"]["BREND_1"]["VALUE"]): */?><!--
                            <?/*= $arResult["PROPERTIES"]["BREND_1"]["VALUE"] */?>,
                        <?/* endif; */?>
                        <?/* if ($arResult["PROPERTIES"]["KOD"]["VALUE"]): */?>
                            <?/*= $arResult["PROPERTIES"]["KOD"]["VALUE"] */?>,
                        <?/* endif; */?>
                        <?/* if ($arResult["PROPERTIES"]["STRANA_PROIZVODITEL_1"]["VALUE"]): */?>
                            <?/*= $arResult["PROPERTIES"]["STRANA_PROIZVODITEL_1"]["VALUE"] */?>
                        --><?/* endif; */?>
                    </div>
                    <? if ($arResult["PROPERTIES"]["DISCOUNTED"]['VALUE'] == "Y"): ?>
					    <p class="discounted-product">
                            ТОВАР СНЯТ С ПРОИЗВОДСТВА
                        </p>
                    <?elseif ($arResult["PROPERTIES"]["NOT_AVAILABLE_RUSSIA"]["VALUE"] === "Y"):?>
                        <p class="discounted-product not-available-rf">
                            ТОВАР НЕ ПОСТАВЛЯЕТСЯ В РОССИЮ
                        </p>
                    <?else:?>
                        <?if ($arResult["PROPERTIES"]["NOT_AVAILABLE_RUSSIA"]["VALUE"] !== "Y"):?>
                        <div class="addCartContainer">
                            <?if(!empty($arResult["PRICE"])):?>
                                <?if($arResult["CATALOG_AVAILABLE"] != "Y"):?>
                                    <?//addCart button?>
                                    <?if($arResult["CATALOG_SUBSCRIBE"] == "Y"):?>
                                        <a href="#" class="addCart subscribe" data-id="<?=$arResult["ID"]?>"><span><img src="<?=SITE_TEMPLATE_PATH?>/images/subscribe.svg" alt="<?=GetMessage("SUBSCRIBE_LABEL")?>" class="icon"><?=GetMessage("SUBSCRIBE_LABEL")?></span></a>
                                    <?else:?>
                                        <a href="#" class="addCart disabled" data-id="<?=$arResult["ID"]?>"><span><img src="/local/templates/stomart/images/cartW.svg" alt="<?=GetMessage("ADDCART_LABEL")?>" class="icon"><?=GetMessage("ADDCART_LABEL")?></span></a>
                                    <?endif;?>
                                <?else:?>
                                    <a href="#" class="addCart" data-id="<?=$arResult["ID"]?>"><span><img src="/local/templates/stomart/images/cartW.svg" alt="<?=GetMessage("ADDCART_LABEL")?>" class="icon"><?=GetMessage("ADDCART_LABEL")?></span></a>
                                <?endif;?>
                            <?else:?>
                                <a href="#" class="addCart disabled requestPrice" data-id="<?=$arResult["ID"]?>"><span><img src="<?=SITE_TEMPLATE_PATH?>/images/request.svg" alt="" class="icon"><?=GetMessage("REQUEST_PRICE_BUTTON_LABEL")?></span></a>
                            <?endif;?>
                            <div class="quantityContainer">
                                <div class="quantityWrapper">
                                    <a href="#" class="minus"></a><input type="text" class="quantity"<?if(!empty($arResult["PRICE"]["EXTENDED_PRICES"])):?> data-extended-price='<?=\Bitrix\Main\Web\Json::encode($arResult["PRICE"]["EXTENDED_PRICES"])?>'<?endif;?> value="<?=$arResult["EXTRA_SETTINGS"]["BASKET_STEP"]?>" data-step="<?=$arResult["EXTRA_SETTINGS"]["BASKET_STEP"]?>" data-max-quantity="<?=$arResult["CATALOG_QUANTITY"]?>" data-enable-trace="<?=(($arResult["CATALOG_QUANTITY_TRACE"] == "Y" && $arResult["CATALOG_CAN_BUY_ZERO"] == "N") ? "Y" : "N")?>"><a href="#" class="plus"></a>
                                </div>
                            </div>
                        </div>
                        <? endif;?>
                    <? endif;?>
    <?if(!empty($arResult["PRICE"]) && $arResult["PROPERTIES"]["DISCOUNTED"]['VALUE'] != "Y" && $arResult["PROPERTIES"]["NOT_AVAILABLE_RUSSIA"]['VALUE'] != "Y" ):?>
                  <noindex>  <a href="#" class="addCart addCart-visible" data-id="<?=$arResult["ID"]?>"><img src="/local/templates/stomart/images/cartW.svg" alt="<?=GetMessage("ADDCART_LABEL")?>" class="icon"><?=GetMessage("ADDCART_LABEL")?></a></noindex>
    <?endif;?>
                    <a href="<?=$arResult["DETAIL_PAGE_URL"]?>" class="btn-simple add-cart"><?=GetMessage("SEE_ON_PAGE")?></a>
                </div>
		    </div>
			<div class="optional">
				<div class="row">

                       <a href="#" class="addWishlist label  <?=$wishlistCssClass?>" data-id="<?=$arResult["~ID"]?>">
                            <img src="/local/templates/stomart/images/heart.svg" alt="" class="icon">
                            <?=$wishlistCssClass?GetMessage("WISHLIST_ADDED"):GetMessage("WISHLIST_LABEL")?>
                        </a>

                    <!-- <?if($arResult["PROPERTIES"]["DISCOUNTED"]['VALUE'] != "Y"):?>
                        <?if($arResult["PROPERTIES"]["NOT_AVAILABLE_RUSSIA"]["VALUE"] !== "Y" && (!empty($arResult["PRICE"]) && $arResult["CATALOG_AVAILABLE"] === "Y")):?>
                            <a href="#" class="fastBack label" data-id="<?=$arResult["ID"]?>"><?=GetMessage("FASTBACK_LABEL")?></a>
                        <?endif;?>
                    <?else:?>
                        <a href="#" class="addWishlist label <?=$wishlistCssClass?>" data-id="<?=$arResult["~ID"]?>">
                            <img src="/local/templates/stomart/images/heart.svg" alt="" class="icon">
                            <?=$wishlistCssClass?GetMessage("WISHLIST_ADDED"):GetMessage("WISHLIST_LABEL")?>
                        </a>

                    <?endif;?> -->

                    <?php
                    ?>
					<a href="#" class="w3 addCompare label <?=$compareCssClass?>" data-id="<?=$arResult["ID"]?>">
                        <img src="/local/templates/stomart/images/scales.svg" alt="" class="icon">
                        <?=$compareCssClass?GetMessage("ADD_COMPARE_ADDED"):GetMessage("COMPARE_LABEL")?>
                    </a>
                    <?if ($arResult["PROPERTIES"]["NOT_AVAILABLE_RUSSIA"]["VALUE"] === "Y"):?>
                        <a href="#" class="addWishlist label  <?=$wishlistCssClass?>" data-id="<?=$arResult["~ID"]?>">
                            <img src="/local/templates/stomart/images/heart.svg" alt="" class="icon">
                            <?=$wishlistCssClass?GetMessage("WISHLIST_ADDED"):GetMessage("WISHLIST_LABEL")?>
                        </a>
                    <?endif;?>
				</div>
                <?if($arResult["PROPERTIES"]["DISCOUNTED"]['VALUE'] != "Y" && $arResult["PROPERTIES"]["NOT_AVAILABLE_RUSSIA"]["VALUE"] !== "Y"):?>
				<div class="row">

				        <?if($arResult["PROPERTIES"]["NOT_AVAILABLE_RUSSIA"]["VALUE"] !== "Y" && (!empty($arResult["PRICE"]) && $arResult["CATALOG_AVAILABLE"] === "Y")):?>
                            <a href="#" class="fastBack label" style="width: 100%; text-align: center;" data-id="<?=$arResult["ID"]?>"><img src="/local/templates/stomart/images/fileText.png" alt="" class="icon"><?=GetMessage("FASTBACK_LABEL")?></a>
                        <?endif;?>

                        <!-- <a href="#" class="addWishlist label  <?=$wishlistCssClass?>" data-id="<?=$arResult["~ID"]?>">
                            <img src="/local/templates/stomart/images/heart.svg" alt="" class="icon">
                            <?=$wishlistCssClass?GetMessage("WISHLIST_ADDED"):GetMessage("WISHLIST_LABEL")?>
                        </a> -->







					<?if($arResult["CATALOG_QUANTITY"] > 0):?>
						<?if(!empty($arResult["EXTRA_SETTINGS"]["STORES"]) && $arResult["EXTRA_SETTINGS"]["STORES_MAX_QUANTITY"] > 0):?>
							<a href="#" data-id="<?=$arResult["ID"]?>" class="inStock label changeAvailable getStoresWindow"><img src="<?=SITE_TEMPLATE_PATH?>/images/inStock.png" alt="<?=GetMessage("AVAILABLE")?>" class="icon"><span><?=GetMessage("AVAILABLE")?></span></a>
						<?else:?>
							<span class="inStock label changeAvailable"><img src="<?=SITE_TEMPLATE_PATH?>/images/inStock.png" alt="<?=GetMessage("AVAILABLE")?>" class="icon"><span><?=GetMessage("AVAILABLE")?></span></span>
						<?endif;?>
					<?else:?>
						<?if($arResult["CATALOG_AVAILABLE"] == "Y"):?>
							<a class="onOrder label changeAvailable"><img src="<?=SITE_TEMPLATE_PATH?>/images/onOrder.png" alt="" class="icon"><?=GetMessage("ON_ORDER")?></a>
						<?else:?>
							<a class="outOfStock label changeAvailable"><img src="<?=SITE_TEMPLATE_PATH?>/images/outOfStock.png" alt="" class="icon"><?=GetMessage("NOAVAILABLE")?></a>
						<?endif;?>
					<?endif;?>
				</div>
                <?endif;?>
			</div>
			<!--<?if(!empty($arResult["SKU_OFFERS"])):?>
				<?if(!empty($arResult["SKU_PROPERTIES"]) && $level = 1):?>
					<?foreach ($arResult["SKU_PROPERTIES"] as $propName => $arNextProp):?>
						<?if(!empty($arNextProp["VALUES"])):?>
							<?if($arNextProp["LIST_TYPE"] == "L" && $arNextProp["HIGHLOAD"] != "Y"):?>
								<?foreach ($arNextProp["VALUES"] as $xml_id => $arNextPropValue):?>
									<?if($arNextPropValue["SELECTED"] == "Y"):?>
										<?$currentSkuValue = $arNextPropValue["DISPLAY_VALUE"];?>
									<?endif;?>
								<?endforeach;?>
								<div class="skuProperty oSkuDropDownProperty" data-name="<?=$propName?>" data-level="<?=$level++?>" data-highload="<?=$arNextProp["HIGHLOAD"]?>">
									<div class="skuPropertyName"><?=preg_replace("/\[.*\]/", "", $arNextProp["NAME"])?>:</div>
									<div class="oSkuDropdown">
										<span class="oSkuCheckedItem noHideChecked"><?=$currentSkuValue?></span>
										<ul class="skuPropertyList oSkuDropdownList">
											<?foreach ($arNextProp["VALUES"] as $xml_id => $arNextPropValue):?>
												<li class="skuPropertyValue oSkuDropdownListItem<?if($arNextPropValue["DISABLED"] == "Y"):?> disabled<?elseif($arNextPropValue["SELECTED"] == "Y"):?> selected<?endif;?>" data-name="<?=$propName?>" data-value="<?=$arNextPropValue["VALUE"]?>">
													<a href="#" class="skuPropertyLink oSkuPropertyItemLink"><?=$arNextPropValue["DISPLAY_VALUE"]?></a>
												</li>
											<?endforeach;?>
										</ul>
									</div>
								</div>
							<?else:?>
								<div class="skuProperty" data-name="<?=$propName?>" data-level="<?=$level++?>" data-highload="<?=$arNextProp["HIGHLOAD"]?>">
									<div class="skuPropertyName"><?=preg_replace("/\[.*\]/", "", $arNextProp["NAME"])?></div>
									<ul class="skuPropertyList">
										<?foreach ($arNextProp["VALUES"] as $xml_id => $arNextPropValue):?>
											<li class="skuPropertyValue<?if($arNextPropValue["DISABLED"] == "Y"):?> disabled<?elseif($arNextPropValue["SELECTED"] == "Y"):?> selected<?endif;?>" data-name="<?=$propName?>" data-value="<?=$arNextPropValue["VALUE"]?>">
												<a href="#" class="skuPropertyLink">
													<?if(!empty($arNextPropValue["IMAGE"])):?>
														<img src="<?=$arNextPropValue["IMAGE"]["src"]?>" alt="">
													<?else:?>
														<?=$arNextPropValue["DISPLAY_VALUE"]?>
													<?endif;?>
												</a>
											</li>
										<?endforeach;?>
									</ul>
								</div>
							<?endif;?>
						<?endif;?>
					<?endforeach;?>
				<?endif;?>
			<?endif;?>-->
			<div class="clear"></div>
		</div>
		<a href="#" class="addCompare mobileCompare <?=$compareCssClass?>" data-id="<?=$arResult["ID"]?>"><img src="/local/templates/stomart/images/scalesG.svg" alt="" class="icon"></a>
		<a href="#" class="addWishlist mobileCompare <?=$wishlistCssClass?>" data-id="<?=$arResult["ID"]?>"><img src="/local/templates/stomart/images/heartG.svg" alt="" class="icon"></a>
	</div>
<?endif;?>
