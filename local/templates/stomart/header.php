<?
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();
?>
<?
	//include module
	\Bitrix\Main\Loader::includeModule("dw.deluxe");
	//get template settings
	$arTemplateSettings = DwSettings::getInstance()->getCurrentSettings();
	extract($arTemplateSettings);
?>
<?
	if(!empty($TEMPLATE_TOP_MENU_FIXED)){
		$_SESSION["TOP_MENU_FIXED"] = $TEMPLATE_TOP_MENU_FIXED;
	}
?>
<?
IncludeTemplateLangFile(__FILE__);
?>
<!DOCTYPE html>
<html lang="<?=LANGUAGE_ID?>">
	<head>
		<meta charset="<?=SITE_CHARSET?>">
		<meta name="format-detection" content="telephone=no">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>

<?
		global $APPLICATION;

    //if (strpos($APPLICATION->GetCurPageParam(), '?') !== false) {
        if ($APPLICATION->GetPageProperty('canonical') == '') {
            CMain::IsHTTPS() ? $s = 's' : $s = '';
            $canon_url = 'http' . $s . '://' . SITE_SERVER_NAME . $APPLICATION->GetCurPage();
            $APPLICATION->AddHeadString('<link href="' . $canon_url . '" rel="canonical" />', true);
        }
    //}

?>


        <link rel="manifest" href="<?=SITE_DIR?>site.webmanifest?v=290822">
        <link rel="icon" type="image/svg+xml" href="<?=SITE_DIR?>favicon.svg">
        <link rel="icon" type="image/png" href="<?=SITE_DIR?>favicon.png">

		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="theme-color" content="#17a594">
        <meta name="yandex-verification" content="4f4b3f9823180f76" />
		<?CJSCore::Init(array("fx", "ajax", "window", "popup", "date", "easing"));?>
		<?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/fonts/roboto/roboto.css");?>
		<?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/themes/".$TEMPLATE_THEME_NAME."/style.css");?>
		<?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/wc.css");?>
		<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/jquery-1.11.0.min.js");?>
		<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/jquery.easing.1.3.js");?>
		<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/rangeSlider.js");?>
		<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/maskedinput.js");?>
		<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/system.js");?>
		<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/topMenu.js");?>
		<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/topSearch.js");?>
		<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/dwCarousel.js");?>
		<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/dwSlider.js");?>
		<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/colorSwitcher.js");?>
		<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/dwTimer.js");?>
		<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/dwZoomer.js");?>
		<?if(DwSettings::isPagen()):?><?$APPLICATION->AddHeadString('<link rel="canonical" href="'.DwSettings::getPagenCanonical().'" />');//pagen canonical?><?endif;?>
		<?if(!DwSettings::isBot() && !empty($arTemplateSettings["TEMPLATE_METRICA_CODE"])):?><?$APPLICATION->AddHeadString($arTemplateSettings["TEMPLATE_METRICA_CODE"]);//metrica counter code?><?endif;?>
		<?/*$APPLICATION->ShowHead();*/?>
        <?
        $APPLICATION->ShowMeta("robots", false, $bXhtmlStyle);
        $APPLICATION->ShowMeta("description", false, $bXhtmlStyle);
        $APPLICATION->ShowLink("canonical", null, $bXhtmlStyle);
        $APPLICATION->ShowCSS(true, $bXhtmlStyle);
        $APPLICATION->ShowHeadStrings();
        $APPLICATION->ShowHeadScripts();

        ?>
        <meta property="og:title" content="<?$APPLICATION->ShowTitle();?>" />
        <meta property="og:description" content="Стоматологическое оборудование - купить оборудование для стоматологического кабинета СТОМАРТ" />
        <meta property="og:url" content="<?=(CMain::IsHTTPS() ? "https://" : "http://").SITE_SERVER_NAME.$APPLICATION->GetCurPage()?>" />
        <meta property="og:type" content="website" />
        <meta property="og:locale" content="ru_RU">
        <meta property="og:site_name" content="STOMART">
        <meta property="og:image" content="<?$APPLICATION->ShowProperty("og:image", (CMain::IsHTTPS() ? "https://" : "http://").SITE_SERVER_NAME.'/local/templates/stomart/images/share.png')?>" />

        <?if (INDEX_PAGE == "Y"): ?>
            <meta property="og:image:width" content="1200">
            <meta property="og:image:height" content="328">
        <?endif;?>

        <meta property="business:contact_data:street_address" content="улица Намёткина, 14 корп.1">
        <meta property="business:contact_data:locality" content="Москва">
        <meta property="business:contact_data:postal_code" content="117420">
        <meta property="business:contact_data:country_name" content="Россия">
        <meta property="place:location:latitude" content="55.661784">
        <meta property="place:location:longitude" content="37.555352">
        <meta property="business:contact_data:email" content="info@stomart.ru">
        <meta property="business:contact_data:phone_number" content="8-495-445-20-73">
        <meta name="author" content="https://web-craft.pro">

        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=G-1B69MV8BFL"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'G-1B69MV8BFL');
        </script>

        <title><?$APPLICATION->ShowTitle();?></title>
	</head>
<body class="loading <?if (INDEX_PAGE == "Y"):?>index<?endif;?><?if(!empty($TEMPLATE_PANELS_COLOR) && $TEMPLATE_PANELS_COLOR != "default"):?> panels_<?=$TEMPLATE_PANELS_COLOR?><?endif;?>">

    <?require_once($_SERVER["DOCUMENT_ROOT"]."/".SITE_TEMPLATE_PATH."/cookies_warning_template.php");?>

    <div id="panel">
		<?$APPLICATION->ShowPanel();?>
	</div>
	<div id="foundation"<?if(!empty($TEMPLATE_SLIDER_HEIGHT) && $TEMPLATE_SLIDER_HEIGHT != "default"):?> class="slider_<?=$TEMPLATE_SLIDER_HEIGHT?>"<?endif;?>>
		<?require_once($_SERVER["DOCUMENT_ROOT"]."/".SITE_TEMPLATE_PATH."/headers/".$TEMPLATE_HEADER."/template.php");?>
		<div id="main"<?if($TEMPLATE_BACKGROUND_NAME != ""):?> class="color_<?=$TEMPLATE_BACKGROUND_NAME?>"<?endif;?>>
			<?$APPLICATION->ShowViewContent("landing_page_banner_container");?>
			<?$APPLICATION->ShowViewContent("before_breadcrumb_container");?>
			<?if(!defined("INDEX_PAGE")):?><div class="limiter"><?endif;?>

						<?if(!defined("INDEX_PAGE") && !defined("ERROR_404")):?>
							<?$APPLICATION->IncludeComponent("bitrix:breadcrumb", ".default", Array(
								"START_FROM" => "0",
									"PATH" => "",
									"SITE_ID" => "-",
								),
								false
							);?>
						<?endif;?>
						<?$APPLICATION->ShowViewContent("after_breadcrumb_container");?>
						<?$APPLICATION->ShowViewContent("landing_page_top_text_container");?>
