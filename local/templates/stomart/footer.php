<? IncludeTemplateLangFile(__FILE__); ?>
<? $APPLICATION->ShowViewContent("landing_page_bottom_text_container"); ?>
<? if (INDEX_PAGE != "Y") : ?></div><? endif; ?>
</div>
<? $APPLICATION->ShowViewContent("no_main_container"); ?>
<? $APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	array(
		"AREA_FILE_SHOW" => "sect",
		"AREA_FILE_SUFFIX" => "footerTabs",
		"AREA_FILE_RECURSIVE" => "Y",
		"EDIT_TEMPLATE" => ""
	),
	false
); ?>

<div id="footer" <? if (!empty($TEMPLATE_FOOTER_VARIANT) && $TEMPLATE_FOOTER_VARIANT != "default") : ?> class="variant_<?= $TEMPLATE_FOOTER_VARIANT ?>" <? endif; ?>>
	<div class="fc">
		<div class="limiter">
			<div id="rowFooter">
				<div id="leftFooter">
					<div class="footerRow">
						<div class="column">
							<span class="heading"><? $APPLICATION->IncludeFile(SITE_DIR . "sect_footer_menu_heading.php", array(), array("MODE" => "text", "NAME" => GetMessage("SECT_FOOTER_MENU_HEADING"), "TEMPLATE" => "sect_footer_menu_heading.php")); ?></span>
							<? $APPLICATION->IncludeComponent(
								"bitrix:menu",
								"footerCatalog",
								array(
									"ROOT_MENU_TYPE" => "left",
									"MENU_CACHE_TYPE" => "A",
									"MENU_CACHE_TIME" => "36000000",
									"MENU_CACHE_USE_GROUPS" => "Y",
									"MENU_CACHE_GET_VARS" => array(),
									"MAX_LEVEL" => "1",
									"CHILD_MENU_TYPE" => "top",
									"USE_EXT" => "Y",
									"DELAY" => "N",
									"ALLOW_MULTI_SELECT" => "N",
									"COMPONENT_TEMPLATE" => "footerCatalog",
									"CACHE_SELECTED_ITEMS" => "N"
								),
								false
							); ?>
						</div>
						<div class="column">
							<span class="heading"><? $APPLICATION->IncludeFile(SITE_DIR . "sect_footer_menu_heading2.php", array(), array("MODE" => "text", "NAME" => GetMessage("SECT_FOOTER_MENU_HEADING2"), "TEMPLATE" => "sect_footer_menu_heading2.php")); ?></span>
							<? $APPLICATION->IncludeComponent(
								"bitrix:menu",
								"footerOffers",
								array(
									"ROOT_MENU_TYPE" => "left2",
									"MENU_CACHE_TYPE" => "N",
									"MENU_CACHE_TIME" => "3600",
									"MENU_CACHE_USE_GROUPS" => "Y",
									"MENU_CACHE_GET_VARS" => array(),
									"MAX_LEVEL" => "1",
									"CHILD_MENU_TYPE" => "top",
									"USE_EXT" => "N",
									"DELAY" => "N",
									"ALLOW_MULTI_SELECT" => "N",
									"COMPONENT_TEMPLATE" => "footerOffers",
									"CACHE_SELECTED_ITEMS" => "N"
								),
								false
							); ?>
							<span class="heading heading-2"><? $APPLICATION->IncludeFile(SITE_DIR . "sect_footer_menu_heading2-2.php", array(), array("MODE" => "text", "NAME" => GetMessage("SECT_FOOTER_MENU_HEADING2"), "TEMPLATE" => "sect_footer_menu_heading2-2.php")); ?></span>
							<? $APPLICATION->IncludeComponent(
								"bitrix:menu",
								"footerOffers",
								array(
									"ROOT_MENU_TYPE" => "left22",
									"MENU_CACHE_TYPE" => "N",
									"MENU_CACHE_TIME" => "3600",
									"MENU_CACHE_USE_GROUPS" => "Y",
									"MENU_CACHE_GET_VARS" => array(),
									"MAX_LEVEL" => "1",
									"CHILD_MENU_TYPE" => "top",
									"USE_EXT" => "N",
									"DELAY" => "N",
									"ALLOW_MULTI_SELECT" => "N",
									"COMPONENT_TEMPLATE" => "footerOffers",
									"CACHE_SELECTED_ITEMS" => "N"
								),
								false
							); ?>
						</div>
						<div class="column">
							<span class="heading"><? $APPLICATION->IncludeFile(SITE_DIR . "sect_footer_menu_heading3.php", array(), array("MODE" => "text", "NAME" => GetMessage("SECT_FOOTER_MENU_HEADING3"), "TEMPLATE" => "sect_footer_menu_heading3.php")); ?></span>
							<? $APPLICATION->IncludeComponent(
								"bitrix:menu",
								"footerHelp",
								array(
									"ROOT_MENU_TYPE" => "left3",
									"MENU_CACHE_TYPE" => "N",
									"MENU_CACHE_TIME" => "3600",
									"MENU_CACHE_USE_GROUPS" => "Y",
									"MENU_CACHE_GET_VARS" => "",
									"MAX_LEVEL" => "1",
									"CHILD_MENU_TYPE" => "top",
									"USE_EXT" => "N",
									"DELAY" => "N",
									"ALLOW_MULTI_SELECT" => "N",
									"CACHE_SELECTED_ITEMS" => "N"
								),
								false
							); ?>
							<span class="heading heading-2"><? $APPLICATION->IncludeFile(SITE_DIR . "sect_footer_menu_heading3-2.php", array(), array("MODE" => "text", "NAME" => GetMessage("SECT_FOOTER_MENU_HEADING3"), "TEMPLATE" => "sect_footer_menu_heading3-2.php")); ?></span>
							<? $APPLICATION->IncludeComponent(
								"bitrix:menu",
								"footerHelp",
								array(
									"ROOT_MENU_TYPE" => "left32",
									"MENU_CACHE_TYPE" => "N",
									"MENU_CACHE_TIME" => "3600",
									"MENU_CACHE_USE_GROUPS" => "Y",
									"MENU_CACHE_GET_VARS" => "",
									"MAX_LEVEL" => "1",
									"CHILD_MENU_TYPE" => "top",
									"USE_EXT" => "N",
									"DELAY" => "N",
									"ALLOW_MULTI_SELECT" => "N",
									"CACHE_SELECTED_ITEMS" => "N"
								),
								false
							); ?>
						</div>
					</div>
				</div>
				<div id="rightFooter">
					<table class="rightTable">
						<tr class="footerRow">
							<td class="leftColumn">
								<? $APPLICATION->IncludeFile(SITE_DIR . "sect_footer_left.php", array(), array("MODE" => "text", "NAME" => GetMessage("SECT_FOOTER_LEFT"), "TEMPLATE" => "sect_footer_left.php")); ?>
								<? $APPLICATION->IncludeFile(SITE_DIR . "sect_footer_left4.php", array(), array("MODE" => "text", "NAME" => GetMessage("SECT_FOOTER_LEFT4"), "TEMPLATE" => "sect_footer_left4.php")); ?>

								<? $APPLICATION->IncludeFile(SITE_DIR . "sect_footer_left3.php", array(), array("MODE" => "text", "NAME" => GetMessage("SECT_FOOTER_LEFT3"), "TEMPLATE" => "sect_footer_left3.php")); ?>
								<? $APPLICATION->IncludeFile(SITE_DIR . "sect_footer_counters.php", array(), array("MODE" => "text", "NAME" => GetMessage("SECT_FOOTER_COUNTERS"), "TEMPLATE" => "sect_footer_counters.php")); ?>
								<div class="social">
									<? $APPLICATION->IncludeComponent(
										"bitrix:main.include",
										".default",
										array(
											"AREA_FILE_SHOW" => "sect",
											"AREA_FILE_SUFFIX" => "sn",
											"AREA_FILE_RECURSIVE" => "Y",
											"EDIT_TEMPLATE" => ""
										),
										false
									); ?>
								</div>
								<? $APPLICATION->IncludeFile(SITE_DIR . "sect_footer_left2.php", array(), array("MODE" => "text", "NAME" => GetMessage("SECT_FOOTER_LEFT2"), "TEMPLATE" => "sect_footer_left2.php")); ?>
								<? $APPLICATION->IncludeFile(SITE_DIR . "sect_footer_counters_right.php", array(), array("MODE" => "text", "NAME" => GetMessage("SECT_FOOTER_COUNTERS"), "TEMPLATE" => "sect_footer_counters_right.php")); ?>
								<? if (!empty($arTemplateSettings["TEMPLATE_GOOGLE_CODE"])) : ?>
									<?= trim($arTemplateSettings["TEMPLATE_GOOGLE_CODE"]) ?>
								<? endif; ?>
								<? if (!empty($arTemplateSettings["TEMPLATE_COUNTERS_CODE"])) : ?>
									<?= trim($arTemplateSettings["TEMPLATE_COUNTERS_CODE"]) ?>
								<? endif; ?>
								<? $APPLICATION->IncludeFile(SITE_DIR . "sect_footer_ya_rating.php"); ?>

							</td>
							<? /*td class="rightColumn">
										<div class="wrap">




										</div>
									</td*/ ?>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</div>
	<?
	/*
		?>
<div id="footerBottom">
<div class="limiter">
	<div class="creator">
		<?if(!empty($TEMPLATE_FOOTER_VARIANT) && ($TEMPLATE_FOOTER_VARIANT == "6" || $TEMPLATE_FOOTER_VARIANT == "7")):?>
			<a href="https://dw24.su/"><img src="<?=SITE_TEMPLATE_PATH?>/images/dwC.png" alt="Digital Web"></a>
		<?elseif(!empty($TEMPLATE_FOOTER_VARIANT) && ($TEMPLATE_FOOTER_VARIANT == "4" || $TEMPLATE_FOOTER_VARIANT == "5" || $TEMPLATE_FOOTER_VARIANT == "6")):?>
			<a href="https://dw24.su/"><img src="<?=SITE_TEMPLATE_PATH?>/images/dwW.png" alt="Digital Web"></a>
		<?else:?>
			<a href="https://dw24.su/"><img src="<?=SITE_TEMPLATE_PATH?>/images/dw.png" alt="Digital Web"></a>
		<?endif;?>
	</div>
	<div class="social">
		<?$APPLICATION->IncludeComponent(
			"bitrix:main.include",
			".default",
			array(
				"AREA_FILE_SHOW" => "sect",
				"AREA_FILE_SUFFIX" => "sn",
				"AREA_FILE_RECURSIVE" => "Y",
				"EDIT_TEMPLATE" => ""
			),
			false
		);?>
	</div>
</div>
</div>
	*/ ?>
</div>
<div id="footerLine" <? if (!empty($TEMPLATE_FOOTER_LINE_COLOR) && $TEMPLATE_FOOTER_LINE_COLOR != "default") : ?> class="color_<?= $TEMPLATE_FOOTER_LINE_COLOR ?>" <? endif; ?>>
	<div class="limiter">
		<div class="col">
			<div class="item">
				<a href="<?= SITE_DIR ?>callback/" class="callback"><span class="icon"></span> <?= GetMessage("FOOTER_CALLBACK_LABEL") ?></a>
			</div>
			<div class="item">
				<? $APPLICATION->IncludeFile(SITE_DIR . "sect_footer_telephone.php", array(), array("MODE" => "text", "NAME" => GetMessage("SECT_FOOTER_TELEPHONE"), "TEMPLATE" => "sect_footer_telephone.php")); ?>
			</div>
			<div class="item">
				<? $APPLICATION->IncludeFile(SITE_DIR . "sect_footer_email.php", array(), array("MODE" => "text", "NAME" => GetMessage("SECT_FOOTER_EMAIL"), "TEMPLATE" => "sect_footer_email.php")); ?>
			</div>

		</div>
		<div class="col">
			<div id="flushFooterCart">
				<? $APPLICATION->IncludeComponent(
					"bitrix:sale.basket.basket.line",
					"bottomCart",
					array(
						"HIDE_ON_BASKET_PAGES" => "N",
						"PATH_TO_BASKET" => SITE_DIR . "personal/cart/",
						"PATH_TO_ORDER" => SITE_DIR . "personal/order/make/",
						"PATH_TO_PERSONAL" => SITE_DIR . "personal/",
						"PATH_TO_PROFILE" => SITE_DIR . "personal/",
						"PATH_TO_REGISTER" => SITE_DIR . "login/",
						"POSITION_FIXED" => "N",
						"SHOW_AUTHOR" => "N",
						"SHOW_EMPTY_VALUES" => "Y",
						"SHOW_NUM_PRODUCTS" => "Y",
						"SHOW_PERSONAL_LINK" => "N",
						"SHOW_PRODUCTS" => "Y",
						"SHOW_TOTAL_PRICE" => "Y",
						"COMPONENT_TEMPLATE" => "bottomCart"
					),
					false
				); ?>
			</div>
		</div>
	</div>
</div>
</div>
<div id="overlap"></div>

<? $APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	array(
		"AREA_FILE_SHOW" => "sect",
		"AREA_FILE_SUFFIX" => "cart",
		"AREA_FILE_RECURSIVE" => "Y",
		"EDIT_TEMPLATE" => ""
	),
	false
); ?>

<? $APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	array(
		"AREA_FILE_SHOW" => "sect",
		"AREA_FILE_SUFFIX" => "fastbuy",
		"AREA_FILE_RECURSIVE" => "Y",
		"EDIT_TEMPLATE" => ""
	),
	false
); ?>

<? $APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	array(
		"AREA_FILE_SHOW" => "sect",
		"AREA_FILE_SUFFIX" => "requestPrice",
		"AREA_FILE_RECURSIVE" => "Y",
		"EDIT_TEMPLATE" => ""
	),
	false
); ?>

<? $APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	array(
		"AREA_FILE_SHOW" => "sect",
		"AREA_FILE_SUFFIX" => "requestIsAvailable",
		"AREA_FILE_RECURSIVE" => "Y",
		"EDIT_TEMPLATE" => ""
	),
	false
); ?>

<? $APPLICATION->IncludeComponent(
	"dresscode:catalog.product.subscribe.online",
	".default",
	array(
		"SITE_ID" => SITE_ID
	),
	false,
	array(
		"HIDE_ICONS" => "Y"
	)
); ?>

<? $APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	array(
		"AREA_FILE_SHOW" => "sect",
		"AREA_FILE_SUFFIX" => "landing",
		"AREA_FILE_RECURSIVE" => "Y",
		"EDIT_TEMPLATE" => ""
	),
	false
); ?>
<? $APPLICATION->IncludeComponent(
	"dresscode:settings",
	".default",
	array(
		"COMPONENT_TEMPLATE" => ".default",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "360000"
	),
	false
); ?>

<div id="upButton">
	<a href="#"></a>
</div>

<script type="text/javascript">
	var ajaxPath = "<?= SITE_DIR ?>ajax.php";
	var SITE_DIR = "<?= SITE_DIR ?>";
	var SITE_ID = "<?= SITE_ID ?>";
	var TEMPLATE_PATH = "<?= SITE_TEMPLATE_PATH ?>";
</script>

<script type="text/javascript">
	var LANG = {
		BASKET_ADDED: "<?= GetMessage("BASKET_ADDED") ?>",
		WISHLIST_ADDED: "<?= GetMessage("WISHLIST_ADDED") ?>",
		WISHLIST_REMOVED: "В избранное",
		ADD_COMPARE_ADDED: "<?= GetMessage("ADD_COMPARE_ADDED") ?>",
		ADD_COMPARE_REMOVED: "Сравнение",
		ADD_CART_LOADING: "<?= GetMessage("ADD_CART_LOADING") ?>",
		ADD_BASKET_DEFAULT_LABEL: "<?= GetMessage("ADD_BASKET_DEFAULT_LABEL") ?>",
		ADDED_CART_SMALL: "<?= GetMessage("ADDED_CART_SMALL") ?>",
		CATALOG_AVAILABLE: "<?= GetMessage("CATALOG_AVAILABLE") ?>",
		GIFT_PRICE_LABEL: "<?= GetMessage("GIFT_PRICE_LABEL") ?>",
		CATALOG_ON_ORDER: "<?= GetMessage("CATALOG_ON_ORDER") ?>",
		CATALOG_NO_AVAILABLE: "<?= GetMessage("CATALOG_NO_AVAILABLE") ?>",
		FAST_VIEW_PRODUCT_LABEL: "<?= GetMessage("FAST_VIEW_PRODUCT_LABEL") ?>",
		CATALOG_ECONOMY: "<?= GetMessage("CATALOG_ECONOMY") ?>",
		WISHLIST_SENDED: "<?= GetMessage("WISHLIST_SENDED") ?>",
		REQUEST_PRICE_LABEL: "<?= GetMessage("REQUEST_PRICE_LABEL") ?>",
		REQUEST_PRICE_BUTTON_LABEL: "<?= GetMessage("REQUEST_PRICE_BUTTON_LABEL") ?>",
		ADD_SUBSCRIBE_LABEL: "<?= GetMessage("ADD_SUBSCRIBE_LABEL") ?>",
		REMOVE_SUBSCRIBE_LABEL: "<?= GetMessage("REMOVE_SUBSCRIBE_LABEL") ?>"
	};
</script>

<script type="text/javascript">
	<? if (!empty($arTemplateSettings)) : ?>
		var globalSettings = {
			<? foreach ($arTemplateSettings as $settingsIndex => $nextSettingValue) : ?>
				<? if (!DwSettings::checkSecretSettingsByIndex($settingsIndex)) : ?> "<?= $settingsIndex ?>": '<?= $nextSettingValue ?>',
				<? endif; ?>
			<? endforeach; ?>
		}
	<? endif; ?>
</script>
<div class="mango-callback" data-settings='{"type":"", "id": "MTAwMTMxMDI=","autoDial" : "0", "lang" : "ru-ru", "host":"widgets.mango-office.ru/", "errorMessage": "В данный момент наблюдаются технические проблемы и совершение звонка невозможно"}'>
</div>
<script>
	! function(t) {
		function e() {
			i = document.querySelectorAll(".button-widget-open");
			for (var e = 0; e < i.length; e++) "true" != i[e].getAttribute("init") && (options = JSON.parse(i[e].closest('.' + t).getAttribute("data-settings")), i[e].setAttribute("onclick", "alert('" + options.errorMessage + "(0000)'); return false;"))
		}

		function o(t, e, o, n, i, r) {
			var s = document.createElement(t);
			for (var a in e) s.setAttribute(a, e[a]);
			s.readyState ? s.onreadystatechange = o : (s.onload = n, s.onerror = i), r(s)
		}

		function n() {
			for (var t = 0; t < i.length; t++) {
				var e = i[t];
				if ("true" != e.getAttribute("init")) {
					options = JSON.parse(e.getAttribute("data-settings"));
					var o = new MangoWidget({
						host: window.location.protocol + '//' + options.host,
						id: options.id,
						elem: e,
						message: options.errorMessage
					});
					o.initWidget(), e.setAttribute("init", "true"), i[t].setAttribute("onclick", "")
				}
			}
		}

		host = window.location.protocol + "//widgets.mango-office.ru/";
		var i = document.getElementsByClassName(t);
		o("link", {
			rel: "stylesheet",
			type: "text/css",
			href: host + "css/widget-button.css"
		}, function() {}, function() {}, e, function(t) {
			var headTag = document.querySelector('head');
			headTag.insertBefore(t, headTag.firstChild)
		}), o("script", {
			type: "text/javascript",
			src: host + "widgets/mango-callback.js"
		}, function() {
			("complete" == this.readyState || "loaded" == this.readyState) && n()
		}, n, e, function(t) {
			document.documentElement.appendChild(t)
		})
	}("mango-callback");
</script>

<script>
	$(function() {
		$('#WEB_FORM_ITEM_TELEPHONE [name="form_text_6"]').mask("+7 999 999-99-99");
		$('#WEB_FORM_ITEM_TELEPHONE [name="form_text_2"]').mask("+7 999 999-99-99");
		$('#ORDER_PROP_14').mask("+7 999 999-99-99");
		$('#ORDER_PROP_3').mask("+7 999 999-99-99");
		$('input[name="USER_PHONE_NUMBER"]').mask("+7 999 999-99-99");
	});
</script>
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
	(function(m, e, t, r, i, k, a) {
		m[i] = m[i] || function() {
			(m[i].a = m[i].a || []).push(arguments)
		};
		m[i].l = 1 * new Date();
		k = e.createElement(t), a = e.getElementsByTagName(t)[0], k.async = 1, k.src = r, a.parentNode.insertBefore(k, a)
	})
	(window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

	ym(16113817, "init", {
		clickmap: true,
		trackLinks: true,
		accurateTrackBounce: true,
		webvisor: true,
		ecommerce: "dataLayer"
	});
</script>
<noscript>
	<div><img src="https://mc.yandex.ru/watch/16113817" style="position:absolute; left:-9999px;" alt="" />
	</div>
</noscript>
<!-- /Yandex.Metrika counter -->


<script>
	(function(w, d, s, h, id) {
		w.roistatProjectId = id;
		w.roistatHost = h;
		var p = d.location.protocol == "https:" ? "https://" : "http://";
		var u = /^.*roistat_visit=[^;]+(.*)?$/.test(d.cookie) ? "/dist/module.js" : "/api/site/1.0/" + id + "/init?referrer=" + encodeURIComponent(d.location.href);
		var js = d.createElement(s);
		js.charset = "UTF-8";
		js.async = 1;
		js.src = p + h + u;
		var js2 = d.getElementsByTagName(s)[0];
		js2.parentNode.insertBefore(js, js2);
	})(window, document, 'script', 'cloud.roistat.com', '3c63dbdcae3d0d0e018230502520bad5');
</script>
<script>
	(function(w, d, u, i, o, s, p) {
		if (d.getElementById(i)) {
			return;
		}
		w['MangoObject'] = o;
		w[o] = w[o] || function() {
			(w[o].q = w[o].q || []).push(arguments)
		};
		w[o].u = u;
		w[o].t = 1 * new Date();
		s = d.createElement('script');
		s.async = 1;
		s.id = i;
		s.src = u;
		p = d.getElementsByTagName('script')[0];
		p.parentNode.insertBefore(s, p);
	}(window, document, '//widgets.mango-office.ru/widgets/mango.js', 'mango-js', 'mgo'));
	mgo({
		calltracking: {
			id: 25488,
			elements: [{
				"numberText": "74956460156"
			}]
		}
	});
</script>
<!-- BEGIN WHATSAPP INTEGRATION WITH ROISTAT -->
<script type="bogus" class="js-whatsapp-message-container">
	Здравствуйте, Вас приветствует компания "СТОМАРТ". Ваш персональный номер: {roistat_visit}

</script>
<script>
	(function() {
		if (window.roistat !== undefined) {
			handler();
		} else {
			var pastCallback = typeof window.onRoistatAllModulesLoaded === "function" ? window.onRoistatAllModulesLoaded : null;
			window.onRoistatAllModulesLoaded = function() {
				if (pastCallback !== null) {
					pastCallback();
				}
				handler();
			};
		}

		function handler() {
			function init() {
				appendMessageToLinks();

				var delays = [1000, 5000, 15000];
				setTimeout(function func(i) {
					if (i === undefined) {
						i = 0;
					}
					appendMessageToLinks();
					i++;
					if (typeof delays[i] !== 'undefined') {
						setTimeout(func, delays[i], i);
					}
				}, delays[0]);
			}

			function replaceQueryParam(url, param, value) {
				var explodedUrl = url.split('?');
				var baseUrl = explodedUrl[0] || '';
				var query = '?' + (explodedUrl[1] || '');
				var regex = new RegExp("([?;&])" + param + "[^&;]*[;&]?");
				var queryWithoutParameter = query.replace(regex, "$1").replace(/&$/, '');
				return baseUrl + (queryWithoutParameter.length > 2 ? queryWithoutParameter + '&' : '?') + (value ? param + "=" + value : '');
			}

			function appendMessageToLinks() {
				var message = document.querySelector('.js-whatsapp-message-container').text;
				var text = message.replace(/{roistat_visit}/g, window.roistatGetCookie('roistat_visit'));
				text = encodeURI(text);
				var linkElements = document.querySelectorAll('[href*="//wa.me"], [href*="//api.whatsapp.com/send"], [href*="//web.whatsapp.com/send"], [href^="whatsapp://send"]');
				for (var elementKey in linkElements) {
					if (linkElements.hasOwnProperty(elementKey)) {
						var element = linkElements[elementKey];
						element.href = replaceQueryParam(element.href, 'text', text);
					}
				}
			}

			if (document.readyState === 'loading') {
				document.addEventListener('DOMContentLoaded', init);
			} else {
				init();
			}
		};
	})();
</script>
<!-- END WHATSAPP INTEGRATION WITH ROISTAT -->
</body>

</html>