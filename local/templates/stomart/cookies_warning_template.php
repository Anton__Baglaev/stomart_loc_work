<?php
if(!isset($_COOKIE['stomart_cookie_allowed'])) {?>
    <div class="cookies_warning warn_msg">
        <div>
            На сайте используются cookie-файлы. Узнать детали в <a href="/personal-info/">политике конфиденциальности</a>
        </div>
        <div class="cookies_btns">
            <a href="#" class="btn-simple btn-small allow_cookies" id="allow_cookies">Ок</a>
            <a href="#" class="disallow_cookies" id="disallow_cookies"></a>
        </div>
    </div>
    <?php
}
?>
<script>
    const allow_cookies = () => {
        const d = new Date();
        d.setTime(d.getTime() + (1000*24*60*60*1000)); //1000 days
        let expires = "expires="+ d.toUTCString();

        document.cookie = "stomart_cookie_allowed=" + (new Date()).getTime()  + ";"+expires+"; path=/";
        document.getElementsByClassName('cookies_warning')[0].classList.add('hidden');
    }

    document.getElementById('allow_cookies').addEventListener('click', function(e){
        allow_cookies();
        e.preventDefault();
    });
    document.getElementById('disallow_cookies').addEventListener('click', function(e){
        allow_cookies();
    });

</script>
