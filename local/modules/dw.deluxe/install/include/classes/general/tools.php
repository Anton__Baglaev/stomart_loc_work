<?
//namespace
namespace DigitalWeb;

//get langs
IncludeModuleLangFile(__FILE__);

//
class Tools{

    //static vars

    //private
    private static $instance = false;

    //constuct
    function __construct(){}

    //singleton
    public static function getInstance(){

        if (!self::$instance){
            self::$instance = new self();
        }

        return self::$instance;
    }

    //functions
    public static function getComponentHTML($name, $template = ".default", $arParams = array()){

        //globals
        global $APPLICATION;

        //vars
        $componentResult = false;

        //check args
        if(!empty($name)){

            //start buffer
            ob_start();

            //get component
            $APPLICATION->IncludeComponent($name, $template, $arParams);

            //write buffer
            $componentResult = ob_get_contents();

            //clear buffer
            ob_end_clean();

        }

        //result
        return $componentResult;

    }


    public static function convertEncoding($data){

        //array
        if(is_array($data)){
            return array_map(function($value){
                return \Bitrix\Main\Text\Encoding::convertEncoding($value, "UTF-8", LANG_CHARSET);
            }, $data);
        }

        //string
        else{
            return !defined("BX_UTF") ? \Bitrix\Main\Text\Encoding::convertEncoding($data, "UTF-8", LANG_CHARSET) : $data;
        }

    }

}