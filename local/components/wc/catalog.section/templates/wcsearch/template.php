<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>
<?if (!empty($arResult["ITEMS"])):?>



			<?foreach ($arResult["ITEMS"] as $index => $arElement):?>
				<?$APPLICATION->IncludeComponent(
					"dresscode:catalog.item",
					"wcsearch",
					array(
						"CACHE_TIME" => $arParams["CACHE_TIME"],
						"CACHE_TYPE" => $arParams["CACHE_TYPE"],
						"HIDE_MEASURES" => $arParams["HIDE_MEASURES"],
						"HIDE_NOT_AVAILABLE" => $arParams["HIDE_NOT_AVAILABLE"],
						"IBLOCK_ID" => $arParams["IBLOCK_ID"],
						"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
						"PRODUCT_ID" => $arElement["ID"],
						"PRODUCT_SKU_FILTER" => $arResult["FILTER"],
						"PICTURE_HEIGHT" => "",
						"PICTURE_WIDTH" => "",
						"PRODUCT_PRICE_CODE" => $arParams["PRICE_CODE"],
						"CONVERT_CURRENCY" => $arParams["CONVERT_CURRENCY"],
						"LAZY_LOAD_PICTURES" => $arParams["LAZY_LOAD_PICTURES"],
						"CURRENCY_ID" => $arParams["CURRENCY_ID"]
					),
					false,
					array("HIDE_ICONS" => "Y")
				);?>
			<?endforeach;?>


		<script>checkLazyItems();</script>


    <?php



    ?>
<?endif;?>
<script>
	var catalogSectionParams = <?=\Bitrix\Main\Web\Json::encode(array("arParams" => array_merge($arParams, array("PAGER_BASE_LINK_ENABLE" => "Y")),	"name" => $component->GetName(), "template" => $templateName, "filter" => $arResult["FILTER"]));?>;
</script>
