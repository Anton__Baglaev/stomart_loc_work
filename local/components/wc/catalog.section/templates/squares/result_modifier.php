<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/*Получаем последнюю часть урла*/
$uri = $APPLICATION->GetCurUri();
$path = parse_url($uri, PHP_URL_PATH);
$last_part = array_pop(explode("/", trim($path, "/")));
/*Проверяем - находимся мы на странице раздела или на "виртуальном урле" от Сотбита */
if ($last_part != $arResult['CODE']) {
    $arResult["DESCRIPTION"] = '';
    $arResult["~DESCRIPTION"] = '';
}


?>