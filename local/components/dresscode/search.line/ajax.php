<?//check empty request
if(empty($_POST["SEARCH_QUERY"])){
	die();
}?>
<?if(!empty($_POST["SITE_ID"])){
	define("SITE_ID", $_POST["SITE_ID"]);
}?>
<?define("STOP_STATISTICS", true);?>
<?define("NO_AGENT_CHECK", true);?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<?include($_SERVER["DOCUMENT_ROOT"]."/".$APPLICATION->GetCurDir()."lang/".LANGUAGE_ID."/ajax.php");?>
<?if(!CModule::IncludeModule("iblock") || !CModule::IncludeModule("search")){
	die("Include modules error, search, iblock");
}
use Bitrix\Main\Loader;

Loader::includeModule("highloadblock");
use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;

$arSuggestCache = [
   "module" => "WCSUGGEST"
];
$obCache = new CPHPCache();
if(1==2 && $obCache->InitCache(36000000, "WCSUGGEST", "/")){
    $cached_suggestions = $obCache->GetVars();
}

elseif($obCache->StartDataCache()){
    $hlblock = HL\HighloadBlockTable::getById(2)->fetch();
    $entity = HL\HighloadBlockTable::compileEntity($hlblock);
    $entity_data_class = $entity->getDataClass();

    $rsData = $entity_data_class::getList(array(
        "select" => array("*"),
        "order" => array("ID" => "ASC"),
    ));
    $cached_suggestions = [];
    while($arData = $rsData->Fetch()){
        $cached_suggestions[$arData["UF_TITLE"]] = $arData["UF_WCSUGGESTIONS"];
    }

    $obCache->EndDataCache(
        $cached_suggestions
    );
}
//globals
global $arrFilter;

//vars
$arItemsIds = array();

//convert encoding
$_POST["SEARCH_QUERY"] = !defined("BX_UTF") ? iconv("UTF-8", "windows-1251//ignore", $_POST["SEARCH_QUERY"]) : $_POST["SEARCH_QUERY"];

$not_decoded_sq = mb_strtolower($_POST["SEARCH_QUERY"]);

//convert case
if(!empty($_POST["CONVERT_CASE"]) && $_POST["CONVERT_CASE"] == "Y"){
	$arLang = CSearchLanguage::GuessLanguage($_POST["SEARCH_QUERY"]);
	if(is_array($arLang) && $arLang["from"] != $arLang["to"]){
		$_POST["SEARCH_QUERY"] = CSearchLanguage::ConvertKeyboardLayout($_POST["SEARCH_QUERY"], $arLang["from"], $arLang["to"]);
	}
}

$suggestions = [];
if (!empty($cached_suggestions)) {
    foreach ($cached_suggestions as $k=>$cached_suggestion) {
        if (strpos(mb_strtolower($_POST["SEARCH_QUERY"]), mb_strtolower($k)) !== false) {
            $suggestions = $cached_suggestion;
        }

	    if (strpos(mb_strtolower($not_decoded_sq), mb_strtolower($k)) !== false) {
		    $suggestions = $cached_suggestion;
	    }
    }
}
/*var_dump($cached_suggestions);
var_dump($suggestions);*/
//search
$obSearch = new CSearch;
$arSearchParams = array(
   "QUERY" => $_POST["SEARCH_QUERY"],
   "SITE_ID" => $_POST["SITE_ID"],
   "MODULE_ID" => "iblock",
   "PARAM2" => [intval($_POST["IBLOCK_ID"]), 1]
);
$pec = (int)$_POST["PAGE_ELEMENT_COUNT"];

$result_categories = [];
$result_brands = [];
$result_goods = [];
$arItemsIds = [];


$sq = mb_strtolower($arSearchParams["QUERY"]);
$wc_res = CIBlockElement::GetList(Array("SORT"=>"ASC"),
    array(
        "IBLOCK_ID" => $_POST["IBLOCK_ID"],
        array(
            "LOGIC" => "OR",
            array("PROPERTY_CML2_BAR_CODE" => $sq ),
            array("PROPERTY_CML2_BAR_CODE" => $not_decoded_sq ),
            array("PROPERTY_CML2_ARTICLE" => $sq),
            array("PROPERTY_CML2_ARTICLE" => $not_decoded_sq),
        )
    )
);

while($fields = $wc_res->GetNext()){
    $result_goods[$fields["ID"]] = $fields["ID"];
    $arItemsIds[$fields["ID"]] = $fields["ID"];
}


$obSearch->Search($arSearchParams, array("CUSTOM_RANK"=>"DESC"), array("STEMMING" => !empty($_POST["STEMMING"]) && $_POST["STEMMING"] == "Y"));
//$obSearch->NavStart();

while($searchItem = $obSearch->fetch()){
    //var_dump($searchItem);

    if (substr($searchItem["ITEM_ID"], 0, 1) === "S"){ //Категория
        if (strpos(mb_strtolower($searchItem["TITLE"]), $sq) !== false) {
            $result_categories[$searchItem["ITEM_ID"]] = $searchItem;
        }
    }
    elseif ($searchItem["PARAM2"] == 1){ // Бренд
        if (strpos(mb_strtolower($searchItem["TITLE"]), $sq) !== false) {
            $result_brands[$searchItem["ITEM_ID"]] = $searchItem;
        }
    }
    elseif(empty($_POST["PAGEN_1"]) || (!empty($_POST["PAGEN_1"]) && (int)$_POST["PAGEN_1"]===1 )) {
        if (is_numeric($searchItem["ITEM_ID"])) { //Товар
            if (strpos(mb_strtolower($searchItem["TITLE"]), $sq) !== false) {
                $result_goods[$searchItem["ITEM_ID"]] = $searchItem;
                $arItemsIds[$searchItem["ITEM_ID"]] = $searchItem["ITEM_ID"];
            }
        }
    }
}

$arSearchParams = array(
	"QUERY" => $not_decoded_sq,
	"SITE_ID" => $_POST["SITE_ID"],
	"MODULE_ID" => "iblock",
	"PARAM2" => [intval($_POST["IBLOCK_ID"]), 1]
);
$obSearch->Search($arSearchParams, array("CUSTOM_RANK"=>"DESC"), array("STEMMING" => !empty($_POST["STEMMING"]) && $_POST["STEMMING"] == "Y"));
while($searchItem = $obSearch->fetch()){
	//var_dump($searchItem);

	if (substr($searchItem["ITEM_ID"], 0, 1) === "S"){ //Категория
		if (strpos(mb_strtolower($searchItem["TITLE"]), $sq) !== false) {
			$result_categories[$searchItem["ITEM_ID"]] = $searchItem;
		}
	}
  elseif ($searchItem["PARAM2"] == 1){ // Бренд
		if (strpos(mb_strtolower($searchItem["TITLE"]), $not_decoded_sq) !== false) {
			$result_brands[$searchItem["ITEM_ID"]] = $searchItem;
		}
	}
  elseif(empty($_POST["PAGEN_1"]) || (!empty($_POST["PAGEN_1"]) && (int)$_POST["PAGEN_1"]===1 )) {
		if (is_numeric($searchItem["ITEM_ID"])) { //Товар
			if (strpos(mb_strtolower($searchItem["TITLE"]), $not_decoded_sq) !== false) {
				$result_goods[$searchItem["ITEM_ID"]] = $searchItem;
				$arItemsIds[$searchItem["ITEM_ID"]] = $searchItem["ITEM_ID"];
			}
		}
	}
}

$page_num = 1;
if (!empty($_POST["PAGEN_1"]) && (int)$_POST["PAGEN_1"]>1){
    $page_num = (int)$_POST["PAGEN_1"];
    //$obSearch->SetOffset(($page_num-1)*$pec - 1 );
    //$obSearch->SetLimit($pec );
    $result_goods = array();
    $arItemsIds = array();

    $arSearchParams = array(
        "QUERY" => $_POST["SEARCH_QUERY"],
        "SITE_ID" => $_POST["SITE_ID"],
        "MODULE_ID" => "iblock",
        "!ITEM_ID" => "S%",
        "PARAM2" => [intval($_POST["IBLOCK_ID"])]
    );

    $obSearch->Search($arSearchParams, array("CUSTOM_RANK"=>"DESC"), array("STEMMING" => !empty($_POST["STEMMING"]) && $_POST["STEMMING"] == "Y"));
    while($searchItem = $obSearch->fetch()){

        if (is_numeric($searchItem["ITEM_ID"])) { //Товар
            if (strpos(mb_strtolower($searchItem["TITLE"]), $sq) !== false) {
                $result_goods[$searchItem["ITEM_ID"]] = $searchItem;
                $arItemsIds[$searchItem["ITEM_ID"]] = $searchItem["ITEM_ID"];
            }
        }
    }

	$arSearchParams = array(
		"QUERY" => $not_decoded_sq,
		"SITE_ID" => $_POST["SITE_ID"],
		"MODULE_ID" => "iblock",
		"!ITEM_ID" => "S%",
		"PARAM2" => [intval($_POST["IBLOCK_ID"])]
	);

	$obSearch->Search($arSearchParams, array("CUSTOM_RANK"=>"DESC"), array("STEMMING" => !empty($_POST["STEMMING"]) && $_POST["STEMMING"] == "Y"));
	while($searchItem = $obSearch->fetch()){

		if (is_numeric($searchItem["ITEM_ID"])) { //Товар
			if (strpos(mb_strtolower($searchItem["TITLE"]), $not_decoded_sq) !== false) {
				$result_goods[$searchItem["ITEM_ID"]] = $searchItem;
				$arItemsIds[$searchItem["ITEM_ID"]] = $searchItem["ITEM_ID"];
			}
		}
	}
}

$next_page_num = $page_num+1;

$arrFilter["ID"] = $arItemsIds;
?>

<?if(!empty($result_brands) || !empty($result_categories) || !empty($result_goods) || !empty($suggestions)):?>
    <?php
    $goods_count = count($result_goods);
    $pages_count = ceil($goods_count/$pec);
    ?>
    <div class="wc-search-content">
        <?php
            if (!empty($suggestions)) {
                echo "<div class='search_group_title'>Похожие запросы</div>";
                echo "<div class='search_group_body wc_suggestions'>";
                foreach ($suggestions as $suggestion) {
                    echo "<div>";
                    echo "<a href='#' class='wc_suggest'>".$suggestion."</a>";
                    echo "</div>";
                }
                echo "</div>";
            }

            if (!empty($result_brands)) {
                echo "<div class='search_group_title'>Бренды</div>";
                echo "<div class='search_group_body'>";
                foreach ($result_brands as $id=>$result_brand) {
                    echo "<div>";
                    echo "<a href='".$result_brand["URL"]."'>".$result_brand["TITLE"]."</a>";
                    echo "</div>";
                }
                echo "</div>";
            }

            if (!empty($result_categories)) {
                echo "<div class='search_group_title'>Категории</div>";
                echo "<div class='search_group_body'>";
                foreach ($result_categories as $id=>$result_category) {
                    echo "<div>";
                    echo "<a href='".$result_category["URL"]."'>".$result_category["TITLE"]."</a>";
                    echo "</div>";
                }
                echo "</div>";
            }

            //var_dump($result_goods);
            if (!empty($result_goods)) {
                echo "<div class='search_group_title'>Товары <span class='search_group_title-total'>".$goods_count." товара(ов)</span></div>";
                echo "<div class='search_group_body search_group_goods'>";

                $APPLICATION->IncludeComponent(
                    "wc:catalog.section",
                    "wcsearch",
                    array(
                        "IBLOCK_TYPE" => $_POST["IBLOCK_TYPE"],
                        "IBLOCK_ID" => intval($_POST["IBLOCK_ID"]),
                        "ELEMENT_SORT_FIELD" => "SHOW_COUNTER",
                        "ELEMENT_SORT_ORDER" => "DESC",
                        "PROPERTY_CODE" => $_POST["PROPERTY_CODE"],
                        "PAGE_ELEMENT_COUNT" => $_POST["PAGE_ELEMENT_COUNT"],
                        "LAZY_LOAD_PICTURES" => $_POST["LAZY_LOAD_PICTURES"],
                        "PRICE_CODE" => $_POST["PRICE_CODE"],
                        "PAGER_TEMPLATE" => "round_search",
                        "CONVERT_CURRENCY" => $_POST["CONVERT_CURRENCY"],
                        "CURRENCY_ID" => $_POST["CURRENCY_ID"],
                        "FILTER_NAME" => "arrFilter",
                        "ADD_SECTIONS_CHAIN" => "N",
                        "SHOW_ALL_WO_SECTION" => "Y",
                        "AJAX_MODE" => "N",
                        "AJAX_OPTION_JUMP" => "N",
                        "CACHE_TYPE" => "Y",
                        "CACHE_FILTER" => "Y",
                        "AJAX_OPTION_HISTORY" => "N",
                        "HIDE_NOT_AVAILABLE" => $_POST["HIDE_NOT_AVAILABLE"],
                        "HIDE_MEASURES" => $_POST["HIDE_MEASURES"],
                    )
                );
                /*$k = 0;
                foreach ($result_goods as $id=>$result_good) {
                    $k++;
                    echo "<div class='wc_search_good ".($k>10?'wc_search_good_hidden':'')."'>";
                    echo "<a href='".$result_good["URL"]."'>".$result_good["TITLE"]."</a>";
                    echo "</div>";
                }*/
                echo "</div>";
            }
        ?>
    </div>
    <div class="wc-bottom-buttons clearfix">
        <a href="<?=SITE_DIR?>search/?q=<?=htmlspecialcharsbx($_POST["SEARCH_QUERY"])?>" class="cp-ls-view-all  searchAllResult"><span>Все результаты</span></a>

        <a class="cp-ls-load-more cm-ls-load-more"
           <?php if ($next_page_num>=$pages_count) { echo 'style="display:none;"'; }?>
           data-page="<?=$next_page_num?>" data-total-pages="<?=$pages_count?>">Показать еще</a>

    </div>
<?else:?>
	<div class="errorMessage"><?=GetMessage("SEARCH_ERROR_FOR_EMPTY_RESULT")?><a href="#" id="searchProductsClose"></a></div>
<?endif;?>

