<?
if (!empty($_REQUEST["siteId"])): ?>
    <? define("SITE_ID", $_REQUEST["siteId"]); ?>
<? endif; ?>
<? define("STOP_STATISTICS", true); ?>
<? define("NO_AGENT_CHECK", true); ?>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php"); ?>
<? error_reporting(0); ?>
<?php

use Bitrix\Main\Mail\Event;

$arResult = array();
if ($_REQUEST['form_email'] && filter_var($_REQUEST['form_email'], FILTER_VALIDATE_EMAIL)) {
    $email = $_REQUEST['form_email'];

    require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");
    CModule::IncludeModule('subscribe');
    global $USER;

    // запрос всех рубрик
    $rub = CRubric::GetList(
      array("LID" => "ASC", "SORT" => "ASC", "NAME" => "ASC"),
      array("ACTIVE" => "Y", "LID" => LANG)
    );
    $arRubIDS = array();
    while ($arRub = $rub->Fetch()){
        $arRubIDS[] = $arRub['ID'];
    }

    // формируем массив с полями для создания подписки
    $arFields = Array(
        "USER_ID" => ($USER->IsAuthorized() ? $USER->GetID() : false),
        "FORMAT" => "html",
        "EMAIL" => $email,
        "ACTIVE" => "N",
        "RUB_ID" => $arRubIDS,
        "SEND_CONFIRM" => 'N'
    );


    $subscr = new CSubscription;

    // создаем подписку
    $ID = $subscr->Add($arFields);
    if ($ID > 0){
        $arResult['status'] = 'ok';
    } else {
        $arResult['status'] = 'ok';
    }



    /**
     * Отправка письма
     */
    $path = CFile::GetPath($_REQUEST['file-id']);
    if($path) {
        $pathfile = $path;
        $url = ((!empty($_SERVER['HTTPS'])) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'];

        $path = $url . $path;

        $link = '<a href="' . $path . '" download>файл</a>';
        if ($_REQUEST["WEB_FORM_ID"] == 5) {
            Event::send(array(
              "EVENT_NAME" => "GET_REG_DOCS",
              "LID" => "s1",
              "C_FIELDS" => array(
                "EMAIL" => $email,
                "LINK" => $link
              ),
              "FILE" => [$_REQUEST['file-id']]
            ));
        } else {
            Event::send(array(
              "EVENT_NAME" => "GET_DOCS",
              "LID" => "s1",
              "C_FIELDS" => array(
                "EMAIL" => $email,
                "LINK" => $link
              ),
              "FILE" => [$_REQUEST['file-id']]
            ));
        }
    } else {
        $arResult['status'] = 'error';
        $arResult['msg'] = 'Ошибка, попробуйте позднее';
    }

} else $arResult['status'] = 'error';

echo json_encode($arResult);
