<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("keywords", "сотрудники");
$APPLICATION->SetPageProperty("title", "Команда | СТОМАРТ - лидер рынка по продаже оборудования для стоматологов");
$APPLICATION->SetPageProperty("description", "Медицинское оборудование для стоматологов. Доставка по всей России");
$APPLICATION->SetTitle("Команда сотрудников СТОМАРТ");
?><h1>Команда</h1>
 <?$APPLICATION->IncludeComponent(
	"bitrix:menu",
	"personal",
	Array(
		"ALLOW_MULTI_SELECT" => "N",
		"CHILD_MENU_TYPE" => "",
		"COMPONENT_TEMPLATE" => "personal",
		"DELAY" => "N",
		"MAX_LEVEL" => "1",
		"MENU_CACHE_GET_VARS" => array(),
		"MENU_CACHE_TIME" => "3600000",
		"MENU_CACHE_TYPE" => "A",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"ROOT_MENU_TYPE" => "about",
		"USE_EXT" => "N"
	)
);?>
<div class="global-block-container team">
	<div class="global-content-block">
        <!--<ul>-->
            <?php
               /* $db_list = CIBlockSection::GetList(
                        array("by"=>"sort", "order"=>"ASC"),
                        array(
                                "GLOBAL_ACTIVE"=>"Y",
                                "IBLOCK_ID" => 22
                        ),
                        false,
                        array()

                );
                while($ar_result = $db_list->GetNext()) {
                    echo '<li><a href="#s'.$ar_result["ID"].'" class="link-dashed">'.$ar_result["NAME"].'</a></li>';
                }*/
            ?>
        <!--</ul>-->
		<div class="bx_page">

            <?php
            /*$db_list = CIBlockSection::GetList(
                array("SORT"=>"ASC"),
                array(
                    "GLOBAL_ACTIVE"=>"Y",
                    "IBLOCK_ID" => 22
                ),
                false,
                array()
            );*/
            //while($ar_result = $db_list->GetNext()) {
                /*echo '<span id="s'.$ar_result["ID"].'"></span>';
                echo '<h2 class="h2 ff-medium">';
                echo $ar_result["NAME"];
                echo '</h2>';*/

                $db_list_elements = CIBlockElement::GetList(
                    array("SORT"=>"ASC"),
                    array(
                        "GLOBAL_ACTIVE"=>"Y",
                        //"IBLOCK_SECTION_ID" => $ar_result["ID"]
                        "IBLOCK_ID" => 22
                    ),
                    false,
                    array(),
                    Array(
                        'ID',
                        'NAME',
                        'DETAIL_PAGE_URL',
                        'PREVIEW_PICTURE',
                        'DETAIL_PICTURE',
                        'PROPERTY_UF_POSITION',
                        'PROPERTY_UF_EMAIL',
                        'PROPERTY_UF_PHONE',
                        'PROPERTY_UF_PHONE_ADD',
                    )
                );

                echo '<div class="team_gallery">';
                while($ar_result_element = $db_list_elements->GetNext()) {
                    $img_resize_path = CFile::ResizeImageGet(
                        $ar_result_element["PREVIEW_PICTURE"],
                        array('width'=>'250', 'height'=>'250'),
                        BX_RESIZE_IMAGE_PROPORTIONAL
                    );


                        echo '<div class="tg_item_wrapper">';
                        echo '<div class="tg_item">';
                        echo '<div class="tg_image">';

                        echo "<img src='" . $img_resize_path['src'] . "' alt=''>";
                        echo '</div>';

                        $res_phone = preg_replace("/\D/", "", $ar_result_element["PROPERTY_UF_PHONE_VALUE"]);
                        $res_phone_txt = $ar_result_element["PROPERTY_UF_PHONE_VALUE"];
                        if (!empty($ar_result_element["PROPERTY_UF_PHONE_ADD_VALUE"])){
                            $res_phone .= "w".$ar_result_element["PROPERTY_UF_PHONE_ADD_VALUE"];
                            $res_phone_txt .= " (".$ar_result_element["PROPERTY_UF_PHONE_ADD_VALUE"].")";
                        }

                        $position = $ar_result_element["PROPERTY_UF_POSITION_VALUE"];
                        $position = str_replace("|","<br>",$position);

                        echo '<div class="tg_info">';
                        echo '<div class="tg_info_top">';
                        echo '<div class="tg_info_name">' . $ar_result_element["NAME"] . '</div>';
                        echo '<div class="tg_info_position">' . $position . '</div>';
                        //echo '<div class="tg_info_email"><a href="mailto:'.$ar_result_element["PROPERTY_UF_EMAIL_VALUE"].'">' . $ar_result_element["PROPERTY_UF_EMAIL_VALUE"] . '</a></div>';
                        echo '<div class="tg_info_phone"><a href="tel:'.$res_phone.'">' . $res_phone_txt . '</a></div>';
                        echo '</div>';

                       /* echo '<div class="tg_info_bottom">';
                        echo '<div class="tg_info_fb">' . $ar_result_element["PROPERTY_UF_FB_VALUE"] . '</div>';
                        echo '<div class="tg_info_tw">' . $ar_result_element["PROPERTY_UF_TW_VALUE"] . '</div>';
                        echo '<div class="tg_info_ig">' . $ar_result_element["PROPERTY_UF_IG_VALUE"] . '</div>';
                        echo '</div>';*/

                        echo '</div>';

                        echo '</div>';
                        echo '</div>';
                    }

                echo '</div>';

            //}
            ?>

		</div>
	</div>

</div>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
